/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filur/blit_osc.h>

#include <math.h>
#include <stddef.h>
#include <assert.h>

#ifndef M_PI
  #define M_PI 3.14159265358979323846264338327950288
#endif /* M_PI */

#define BLIT_USE_COS_APPROXIMATION 1

#ifdef BLIT_USE_COS_APPROXIMATION
#include <filur/sine_approximation.h>
#endif /* BLIT_USE_COS_APPROXIMATION */

static const double blit_osc_band_limit_factor = 0.48; /* fraction of sample rate */

static const double blit_osc_two_pi = 2.0 * M_PI;
static const double blit_osc_inv_two_pi = 1.0 / (2.0 * M_PI);

static const double blit_osc_integrator_a = 0.999773268f;
#if 0 /* doesn't compile with MSVC */
static const double blit_osc_integrator_k = 3300.0 * (1.0 - blit_osc_integrator_a);
#else
static const double blit_osc_integrator_k = 3300.0 * (1.0 - 0.999773268f);
#endif
static const double blit_osc_min_fi = 0.05 * M_PI;
static const double blit_osc_max_fi = 0.45 * M_PI;

#ifdef BLIT_USE_COS_APPROXIMATION
#define BLIT_COS(x) cos_20(x)
#define BLIT_SINE(x) sine_11(x)
#else
#define BLIT_COS(x) cos(x)
#define BLIT_SINE(x) sin(x)
#endif /* BLIT_USE_COS_APPROXIMATION */

void blit_init(double sample_rate, blit_t* blit) {
  assert(NULL != blit);

  blit->alpha = 0;
  blit->delta_alpha = 0;
  blit->m = 0;
  blit->band_limit_factor = 0;
  blit->attenuation = 0;
  blit->integrator = 0;
  blit->sample_rate = sample_rate;
  blit->sample_time = 1.0 / blit->sample_rate;
  blit->band_limit_frequency = blit->sample_rate * blit_osc_band_limit_factor;
}

void blit_clear(blit_t* blit) {
  assert(NULL != blit);

  blit->alpha = 0;
  blit->integrator = 0;
}

void blit_set_frequency(double frequency, blit_t* blit) {
  assert(NULL != blit);

  blit->delta_alpha = blit_osc_two_pi * blit->sample_time * frequency;

  blit->m = (2.0 * (int)(((int)(blit->band_limit_frequency / frequency) - 1.0) * 0.5) + 1.0);
  blit->attenuation = 0.5 / blit->m;
  blit->band_limit_factor = 2.0 * blit->m + 1.0;
}

double blit_process_sine(blit_t* blit) {
  assert(NULL != blit);

  double out;

  out = 0.75f * BLIT_SINE(blit->alpha);

  blit->alpha += blit->delta_alpha;
  if (blit->alpha > M_PI) {
    blit->alpha -= blit_osc_two_pi;
  }

  return out;
}

double blit_process_saw(blit_t* blit) {
  assert(NULL != blit);

  double alpha = 0;
  double upper_alpha = 0;
  double out;

  alpha = 0.5 * blit->alpha;
  upper_alpha = (alpha + M_PI) * blit->band_limit_factor;
  /* limit upper_alpha to the range -PI,PI */
  double alpha_offset = upper_alpha * blit_osc_inv_two_pi;
  alpha_offset = blit_osc_two_pi * (int) (alpha_offset);
  upper_alpha = upper_alpha - alpha_offset - M_PI;

  out = (blit->attenuation * (1.0 + BLIT_COS(upper_alpha) / BLIT_COS(alpha)));

  blit->alpha += blit->delta_alpha;
  if (blit->alpha > M_PI) {
    blit->alpha -= blit_osc_two_pi;
  }

  blit->integrator = out + blit_osc_integrator_a * blit->integrator;
  out = blit_osc_integrator_k * blit->integrator;

  return out;
}

double blit_process_pulse(const double pwm, blit_t* blit) {
  assert(NULL != blit);

  double alpha1 = 0;
  double alpha2 = 0;
  double upper_alpha1 = 0;
  double upper_alpha2 = 0;
  double out;

  double fi = blit_osc_min_fi + (blit_osc_max_fi - blit_osc_min_fi) * (0.5 * pwm);

  if (fi < blit_osc_min_fi) {
    fi = blit_osc_min_fi;
  } else if (fi > blit_osc_max_fi) {
    fi = blit_osc_max_fi;
  }

  alpha1 = 0.5 * blit->alpha + fi;
  alpha2 = 0.5 * blit->alpha - fi;

  upper_alpha1 = (alpha1 + M_PI) * blit->band_limit_factor;
  /* limit upper_alpha1 to the range -PI,PI */
  double alpha_offset = upper_alpha1 * blit_osc_inv_two_pi;
  alpha_offset = blit_osc_two_pi * (int) (alpha_offset);
  upper_alpha1 = upper_alpha1 - alpha_offset - M_PI;

  upper_alpha2 = (alpha2 + M_PI) * blit->band_limit_factor;
  /* limit upper_alpha2 to the range -PI,PI */
  alpha_offset = upper_alpha2 * blit_osc_inv_two_pi;
  alpha_offset = blit_osc_two_pi * (int) (alpha_offset);
  upper_alpha2 = upper_alpha2 - alpha_offset - M_PI;

  out = blit->attenuation * ((BLIT_COS(upper_alpha1) / BLIT_COS(alpha1)) - (BLIT_COS(upper_alpha2) / BLIT_COS(alpha2)));

  blit->alpha += blit->delta_alpha;
  if (blit->alpha > M_PI) {
    blit->alpha -= blit_osc_two_pi;
  }

  blit->integrator = out + blit_osc_integrator_a * blit->integrator;
  out = blit_osc_integrator_k * blit->integrator;

  return out;
}
