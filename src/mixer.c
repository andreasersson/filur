/*
 * Copyright 2018-2024 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filur/mixer.h>

#include <filur/utils.h>

#include <assert.h>
#include <math.h>
#include <stddef.h>

#ifndef M_PI
  #define M_PI 3.14159265358979323846264338327950288
#endif /* M_PI */

static const double mixer_zipper_noise_threshold = 0.0001;
static const double mixer_zipper_noise_factor = 0.0001;

static const double mixer_min_volume_db = -20.0;
static const double mixer_max_volume_db = 6.0;

#define MIXER_VOLUME_TABLE_LENGTH 256

static double mixer_volume_table[MIXER_VOLUME_TABLE_LENGTH];

static void mixer_volume_table_init(void);
static double mixer_volume(double volume);

void mixer_init_parameters(mixer_parameters_t* parameters) {
  assert(NULL != parameters);

  mixer_set_volume(0.8, parameters);
  mixer_set_pan(0.5, parameters);
}

void mixer_set_volume(double volume, mixer_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->volume = filur_limit(volume, 0.0, 1.0);
}

void mixer_set_pan(double pan, mixer_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->pan = filur_limit(pan, 0.0, 1.0);
}

void mixer_init(mixer_t* mixer) {
  assert(NULL != mixer);

  mixer_volume_table_init();
  mixer->volume = 0.0;
  mixer->pan = 0.0;
  znf_value_init(mixer_zipper_noise_threshold, mixer_zipper_noise_factor, 1.0, &mixer->gain);
  znf_value_init(mixer_zipper_noise_threshold, mixer_zipper_noise_factor, 0.7, &mixer->left_gain);
  znf_value_init(mixer_zipper_noise_threshold, mixer_zipper_noise_factor, 0.7, &mixer->right_gain);
}

void mixer_process(const double* in,
                   double* out_left,
                   double* out_right,
                   int number_of_frames,
                   const mixer_parameters_t* parameters,
                   mixer_t* mixer) {
  assert(NULL != in);
  assert(NULL != out_left);
  assert(NULL != out_right);
  assert(NULL != parameters);
  assert(NULL != mixer);

  mixer->volume = parameters->volume;
  mixer->pan = parameters->pan;
  znf_value_set(mixer_volume(parameters->volume), &mixer->gain);
  double alpha = parameters->pan * M_PI * 0.5;
  znf_value_set(cos(alpha), &mixer->left_gain);
  znf_value_set(sin(alpha), &mixer->right_gain);

  if (znf_value_equal(&mixer->gain) &&
      znf_value_equal(&mixer->left_gain) &&
      znf_value_equal(&mixer->right_gain)) {
    double left_gain = mixer->left_gain.value * mixer->gain.value;
    double right_gain = mixer->right_gain.value * mixer->gain.value;

    for (int n = 0; n < number_of_frames; ++n) {
      out_left[n] += left_gain * in[n];
      out_right[n] += right_gain * in[n];
    }
  } else {
    for (int n = 0; n < number_of_frames; ++n) {
      out_left[n] += mixer->left_gain.value * mixer->gain.value * in[n];
      out_right[n] += mixer->right_gain.value * mixer->gain.value * in[n];

      if (!mixer->gain.equal) {
        znf_value_update(&mixer->gain);
      }

      if (!mixer->left_gain.equal) {
        znf_value_update(&mixer->left_gain);
      }

      if (!mixer->right_gain.equal) {
        znf_value_update(&mixer->right_gain);
      }
    }
  }
}

void mixer_process_stereo(const double* in_left,
                          const double* in_right,
                          double* out_left,
                          double* out_right,
                          int number_of_frames,
                          const mixer_parameters_t* parameters,
                          mixer_t* mixer) {
  assert(NULL != in_left);
  assert(NULL != in_right);
  assert(NULL != out_left);
  assert(NULL != out_right);
  assert(NULL != parameters);
  assert(NULL != mixer);

  mixer->volume = parameters->volume;
  mixer->pan = parameters->pan;
  znf_value_set(mixer_volume(parameters->volume), &mixer->gain);
  double alpha = parameters->pan * M_PI * 0.5;
  znf_value_set(cos(alpha), &mixer->left_gain);
  znf_value_set(sin(alpha), &mixer->right_gain);

  if (znf_value_equal(&mixer->gain) &&
      znf_value_equal(&mixer->left_gain) &&
      znf_value_equal(&mixer->right_gain)) {
    double left_gain = mixer->left_gain.value * mixer->gain.value;
    double right_gain = mixer->right_gain.value * mixer->gain.value;

    for (int n = 0; n < number_of_frames; ++n) {
      out_left[n] += left_gain * in_left[n];
      out_right[n] += right_gain * in_right[n];
    }
  } else {
    for (int n = 0; n < number_of_frames; ++n) {
      out_left[n] += mixer->left_gain.value * mixer->gain.value * in_left[n];
      out_right[n] += mixer->right_gain.value * mixer->gain.value * in_right[n];

      if (!mixer->gain.equal) {
        znf_value_update(&mixer->gain);
      }

      if (!mixer->left_gain.equal) {
        znf_value_update(&mixer->left_gain);
      }

      if (!mixer->right_gain.equal) {
        znf_value_update(&mixer->right_gain);
      }
    }
  }
}

void mixer_update_parameters(const mixer_parameters_t* parameters, mixer_t* mixer) {
  assert(NULL != parameters);
  assert(NULL != mixer);

  if (mixer->volume != parameters->volume) {
    mixer->volume = parameters->volume;
    znf_value_set(mixer_volume(parameters->volume), &mixer->gain);
    znf_value_reset(&mixer->gain);
  }

  if (mixer->pan != parameters->pan) {
    mixer->pan = parameters->pan;
    double alpha = parameters->pan * M_PI * 0.5;
    znf_value_set(cos(alpha), &mixer->left_gain);
    znf_value_reset(&mixer->left_gain);
    znf_value_set(sin(alpha), &mixer->right_gain);
    znf_value_reset(&mixer->right_gain);
  }
}

void mixer_reset(mixer_t* mixer) {
  assert(NULL != mixer);

  znf_value_reset(&mixer->gain);
  znf_value_reset(&mixer->left_gain);
  znf_value_reset(&mixer->right_gain);
}

void mixer_volume_table_init(void) {
  for (int n = 0; n < MIXER_VOLUME_TABLE_LENGTH; ++n) {
    mixer_volume_table[n] = pow(10.0, (mixer_min_volume_db + (mixer_max_volume_db - mixer_min_volume_db) * n / MIXER_VOLUME_TABLE_LENGTH) / 20.0);
  }

  mixer_volume_table[0] = 0.0;
}

double mixer_volume(double volume) {
  int v = (int)(volume * (MIXER_VOLUME_TABLE_LENGTH - 1));
  if (v < 0) {
    v = 0;
  } else if (v >= MIXER_VOLUME_TABLE_LENGTH) {
    v = MIXER_VOLUME_TABLE_LENGTH - 1;
  }

  return mixer_volume_table[v];
}
