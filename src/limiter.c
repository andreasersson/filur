/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filur/limiter.h>

#include <filur/utils.h>

#include <assert.h>
#include <stddef.h>

static const double limiter_c1 = 0.823474077876408;
static const double limiter_c3 = -0.063766035060039;
static const double limiter_c5 = 0.003338348984948;
static const double limiter_c7 = -0.000086532799221;
static const double limiter_c9 = 0.000000855465657;
#if 0 /* doesn't compile with MSVC: error C2099: initializer is not a constant */
static const double limiter_sum = limiter_c1 + limiter_c3 + limiter_c5
    + limiter_c7 + limiter_c9;
#else
static const double limiter_sum = 0.823474077876408 + -0.063766035060039
    + 0.003338348984948 + -0.000086532799221 + 0.000000855465657;
#endif
static const double limiter_min_gain = 0.01;
static const double limiter_max_gain = 100.0;
static const double limiter_zipper_noise_threshold = 0.001;
static const double limiter_zipper_noise_factor = 0.001;

static const double limiter_cliplevel = 5.9;

static inline double limit(double x) {
  if (x > limiter_cliplevel) {
    x = limiter_cliplevel;
  } else if (x < -limiter_cliplevel) {
    x = -limiter_cliplevel;
  }

  double x2 = x * x;
  return (x * (limiter_c1 + x2 * (limiter_c3 + x2 * (limiter_c5 + x2 * (limiter_c7 + x2 * limiter_c9)))));
}

void limiter_init(limiter_t* limiter) {
  assert(NULL != limiter);

  znf_value_init(limiter_zipper_noise_threshold,
                 limiter_zipper_noise_factor,
                 1,
                 &limiter->gain);

  limiter_set_gain(1.0, limiter);
  limiter_reset(limiter);
}

void limiter_process(const double* in, double* out, int numberOfFrames, limiter_t* limiter) {
  assert(NULL != limiter);
  assert(NULL != in);
  assert(NULL != out);

  double out_gain = 1.0f / (limiter->gain.value * limiter_sum);
  for (int n = 0; n < numberOfFrames; ++n) {
    double x = limiter->gain.value * in[n];
    out[n] = out_gain * limit(x);

    if (!limiter->gain.equal) {
      znf_value_update(&limiter->gain);
      out_gain = 1.0f / (limiter->gain.value * limiter_sum);
    }
  }
}

void limiter_set_gain(double gain, limiter_t* limiter) {
  assert(NULL != limiter);

  double local_gain = filur_limit(gain, limiter_min_gain, limiter_max_gain);

  znf_value_set(local_gain, &limiter->gain);
}

void limiter_reset(limiter_t* limiter) {
  assert(NULL != limiter);

  znf_value_reset(&limiter->gain);
}
