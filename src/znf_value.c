/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filur/znf_value.h>

#include <stddef.h>
#include <math.h>
#include <assert.h>

static inline void znf_value_update_delta(znf_value_t* value) {
  if (value->target > value->value) {
    value->delta = value->step;
  } else {
    value->delta = -value->step;
  }
}

void znf_value_init(double threshold, double step, double value, znf_value_t* znf_value) {
  assert(NULL != znf_value);

  znf_value->target = value;
  znf_value->value = value;
  znf_value->threshold = threshold;
  znf_value->step = step;
  znf_value->equal = true;
  znf_value->delta = 0.0;
}

void znf_value_set(double value, znf_value_t* znf_value) {
  assert(NULL != znf_value);

  znf_value->target = value;
  znf_value_update_delta(znf_value);
  znf_value_equal(znf_value);
}

void znf_value_reset(znf_value_t *znf_value) {
  assert(NULL != znf_value);

  znf_value->value = znf_value->target;
  znf_value->equal = true;
}

void znf_value_update(znf_value_t *znf_value) {
  assert(NULL != znf_value);

  if (fabs(znf_value->delta) < fabs(znf_value->target - znf_value->value)) {
    znf_value->value += znf_value->delta;
  } else {
    znf_value->value = znf_value->target;
    znf_value->equal = true;
  }
}

bool znf_value_equal(znf_value_t* znf_value) {
  assert(NULL != znf_value);

  if (fabs(znf_value->target - znf_value->value) < znf_value->threshold) {
    znf_value->value = znf_value->target;
    znf_value->equal = true;
  } else {
    znf_value->equal = false;
  }

  return znf_value->equal;
}

void znf_log_value_init(double threshold, double step, double value, znf_log_value_t* znf_value) {
  assert(NULL != znf_value);

  znf_value->threshold = pow(10.0, threshold / 20.0);
  znf_value->step = pow(10.0, step / 20.0);
  znf_value->value = value;
  znf_log_value_set(value, znf_value);
  znf_value->equal = true;
}

void znf_log_value_set(double value, znf_log_value_t* znf_value) {
  assert(NULL != znf_value);

  if (znf_value->target != value) {
    znf_value->target = value;

    znf_value->threshold_max = znf_value->target * znf_value->threshold;
    znf_value->threshold_min = znf_value->target / znf_value->threshold;

    if (znf_value->target > znf_value->value) {
      znf_value->multiplier = znf_value->step;
    } else {
      znf_value->multiplier = 1.0 / znf_value->step;
    }

    znf_log_value_equal(znf_value);
  }
}

void znf_log_value_reset(znf_log_value_t *znf_value) {
  assert(NULL != znf_value);

  znf_value->value = znf_value->target;
  znf_value->equal = true;
}

void znf_log_value_update(znf_log_value_t *znf_value) {
  assert(NULL != znf_value);

  double local_value = znf_value->value * znf_value->multiplier;

  if ((local_value < znf_value->threshold_min) || (local_value > znf_value->threshold_max)) {
    znf_value->value = local_value;
  } else {
    znf_value->value = znf_value->target;
    znf_value->equal = true;
  }
}

bool znf_log_value_equal(znf_log_value_t* znf_value) {
  assert(NULL != znf_value);

  if ((znf_value->value >= znf_value->threshold_min) &&
      (znf_value->value <= znf_value->threshold_max)) {
    znf_value->value = znf_value->target;
    znf_value->equal = true;
  } else {
    znf_value->equal = false;
  }

  return znf_value->equal;
}
