/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filur/lfo.h>

#include <filur/sine_approximation.h>
#include <filur/utils.h>

#include <assert.h>
#include <math.h>
#include <stddef.h>

#ifndef M_PI
  #define M_PI 3.14159265358979323846264338327950288
#endif /* M_PI */

static const double lfo_min_frequency = 1e-3;
static const double lfo_max_frequency = 20.0;

static void lfo_process_alpha(double* out,
                              int number_of_frames,
                              lfo_state_t* state,
                              const lfo_parameters_t* parameters);

void lfo_init_parameters(double sample_rate, lfo_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->sample_time = 1.0 / sample_rate;
  parameters->delta_alpha = 0;

  lfo_set_frequency(0.01, parameters);
}

void lfo_init_state(lfo_state_t* state) {
  assert(NULL != state);

  state->alpha = 0;
}

void lfo_set_phase(double phase, lfo_state_t* state) {
  assert(NULL != state);

  state->alpha = filur_limit(phase, -1.0, 1.0);
}

void lfo_process_sine(double* out,
                      int number_of_frames,
                      lfo_state_t* state,
                      const lfo_parameters_t* parameters) {
  assert(NULL != out);
  assert(NULL != state);
  assert(NULL != parameters);

  lfo_process_alpha(out, number_of_frames, state, parameters);

  for (int n = 0; n < number_of_frames; ++n) {
    out[n] = sine_9(out[n] * M_PI);
  }
}

void lfo_process_triangle(double* out,
                          int number_of_frames,
                          lfo_state_t* state,
                          const lfo_parameters_t* parameters) {
  assert(NULL != out);
  assert(NULL != state);
  assert(NULL != parameters);

  lfo_process_alpha(out, number_of_frames, state, parameters);

  for (int n = 0; n < number_of_frames; ++n) {
    out[n] = 2.0 * (fabs(out[n]) - 0.5);
  }
}

void lfo_process_triangle_normalized(double* out,
                                     int number_of_frames,
                                     lfo_state_t* state,
                                     const lfo_parameters_t* parameters) {
  assert(NULL != out);
  assert(NULL != state);
  assert(NULL != parameters);

  lfo_process_alpha(out, number_of_frames, state, parameters);

  for (int n = 0; n < number_of_frames; ++n) {
    out[n] = fabs(out[n]);
  }
}

void lfo_process_triangle_sqr(double* out,
                              int number_of_frames,
                              lfo_state_t* state,
                              const lfo_parameters_t* parameters) {
  assert(NULL != out);
  assert(NULL != state);
  assert(NULL != parameters);

  lfo_process_alpha(out, number_of_frames, state, parameters);

  for (int n = 0; n < number_of_frames; ++n) {
    out[n] = out[n] * out[n];
  }
}

void lfo_process_saw(double* out,
                     int number_of_frames,
                     lfo_state_t* state,
                     const lfo_parameters_t* parameters) {
  assert(NULL != out);
  assert(NULL != state);
  assert(NULL != parameters);

  lfo_process_alpha(out, number_of_frames, state, parameters);
}

void lfo_process_square(double* out,
                        int number_of_frames,
                        lfo_state_t* state,
                        const lfo_parameters_t* parameters) {
  assert(NULL != out);
  assert(NULL != state);
  assert(NULL != parameters);

  lfo_process_alpha(out, number_of_frames, state, parameters);

  for (int n = 0; n < number_of_frames; ++n) {
    if (out[n] >= 0) {
      out[n] = 1.0;
    } else {
      out[n] = -1.0;
    }
  }
}

void lfo_process_rnd(double* out,
                     int number_of_frames,
                     lfo_state_t* state,
                     const lfo_parameters_t* parameters) {
  assert(NULL != out);
  assert(NULL != state);
  assert(NULL != parameters);

  lfo_process_alpha(out, number_of_frames, state, parameters);

  /* TODO: implement random lfo */
  for (int n = 0; n < number_of_frames; ++n) {
    out[n] = 0;
  }
}

void lfo_set_frequency(double frequency, lfo_parameters_t* parameters) {
  assert(NULL != parameters);
  assert(NULL != parameters);

  double local_frequency = frequency;
  if (local_frequency < lfo_min_frequency) {
    local_frequency = lfo_min_frequency;
  } else if (local_frequency > lfo_max_frequency) {
    local_frequency = lfo_max_frequency;
  }

  parameters->delta_alpha = 2 * parameters->sample_time * local_frequency;
}

double lfo_tempo_to_frequency(double tempo, double rate) {
  double frequency = tempo / 60 * rate;

  return frequency;
}

static void lfo_process_alpha(double* out,
                              int number_of_frames,
                              lfo_state_t* state,
                              const lfo_parameters_t* parameters) {
  assert(NULL != out);
  assert(NULL != state);
  assert(NULL != parameters);

  for (int n = 0; n < number_of_frames; ++n) {
    out[n] = state->alpha;

    state->alpha += parameters->delta_alpha;
    if (state->alpha > 1.0) {
      state->alpha -= 2.0;
    }
  }
}
