/*
 * Copyright 2018-2024 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filur/polyfilur/polyfilur.h>

#include <assert.h>
#include <math.h>
#include <stddef.h>
#include <string.h>

static const double volume_zipper_noise_threshold = 0.05; /* dB */
static const double volume_zipper_noise_step = 0.005; /* dB */

static void polyfilur_sub_process(double* out,
                                  int number_of_frames,
                                  const pf_parameters_t* parameters,
                                  polyfilur_t* polyfilur);
static pf_voice_t* polyfilur_get_voice(polyfilur_t* polyfilur);
static void process_volume(const double* in,
                           double* left,
                           double* right,
                           int number_of_frames,
                           znf_log_value_t* volume);
static void process_lfo(double* out,
                        int number_of_frames,
                        lfo_state_t* lfo_state,
                        const pf_lfo_parameters_t* parameters);

void polyfilur_init(double sample_rate, polyfilur_t* polyfilur) {
  assert(NULL != polyfilur);

  int_list_init(&polyfilur->note_list);

  polyfilur->voice_mode = PF_VOICE_MODE_POLY;
  znf_log_value_init(volume_zipper_noise_threshold,
                     volume_zipper_noise_step,
                     1.0,
                     &polyfilur->volume);

  lfo_init_state(&polyfilur->osc_lfo);
  lfo_init_state(&polyfilur->filter_lfo);

  noise_init(&polyfilur->noise);
  bilin_lp_init(sample_rate, &polyfilur->noise_lp);
  bilin_hp2_init(sample_rate, &polyfilur->noise_hp);

  bilin_hp2_set_cutoff(2000, &polyfilur->noise_hp);
  bilin_hp2_set_q(0.707f, &polyfilur->noise_hp);
  bilin_lp_set_cutoff(6000, &polyfilur->noise_lp);

  for (size_t voice = 0; voice < POLYFILUR_NUM_VOICES; ++voice) {
    pf_voice_init(sample_rate, &polyfilur->voices[voice]);
  }
}

void polyfilur_note_on(int pitch,
                       double velocity,
                       const pf_parameters_t* parameters,
                       polyfilur_t* polyfilur) {
  assert(NULL != parameters);
  assert(NULL != polyfilur);

  int num_active_voices = 0;
  if ((PF_LFO_RESTART_TRUE == parameters->filter_lfo.restart)
       || (PF_LFO_RESTART_TRUE == parameters->osc_lfo.restart)) {
    for (int index = 0;
        (index < POLYFILUR_NUM_VOICES) && (0 == num_active_voices); ++index) {
      if (PF_VOICE_STATE_FREE != polyfilur->voices[index].state) {
        ++num_active_voices;
      }
    }
  }

  if (PF_LFO_RESTART_TRUE == parameters->filter_lfo.restart) {
    if (0 == num_active_voices) {
      lfo_set_phase(parameters->filter_lfo.phase, &polyfilur->filter_lfo);
    }
  }

  if (PF_LFO_RESTART_TRUE == parameters->osc_lfo.restart) {
    if (0 == num_active_voices) {
      lfo_set_phase(parameters->osc_lfo.phase, &polyfilur->osc_lfo);
    }
  }

  if (PF_VOICE_MODE_POLY == parameters->voice_mode) {
    pf_voice_t* voice = polyfilur_get_voice(polyfilur);
    pf_voice_note_on(pitch, velocity, parameters, voice);
  } else {
    pf_voice_t* voice = &polyfilur->voices[0];
    int_list_push(pitch, &polyfilur->note_list);

    if (1 == int_list_size(&polyfilur->note_list)) {
      pf_voice_note_on(pitch, velocity, parameters, voice);
    } else {
      pf_voice_slide_note(voice->pitch, pitch, parameters, voice);
    }
  }
}

void polyfilur_note_off(int pitch,
                        const pf_parameters_t* parameters,
                        polyfilur_t* polyfilur) {
  assert(NULL != parameters);
  assert(NULL != polyfilur);

  if (PF_VOICE_MODE_POLY == parameters->voice_mode) {
    for (size_t index = 0; index < POLYFILUR_NUM_VOICES; ++index) {
      pf_voice_t* voice = &polyfilur->voices[index];
      if ((voice->pitch == pitch) && (PF_VOICE_STATE_ACTIVE == voice->state)) {
        pf_voice_note_off(parameters, voice);
      }
    }
  } else {
    pf_voice_t* voice = &polyfilur->voices[0];
    int_list_remove(pitch, &polyfilur->note_list);

    if (0 == int_list_size(&polyfilur->note_list)) {
      pf_voice_note_off(parameters, voice);
    } else if (voice->pitch != int_list_top(&polyfilur->note_list)) {
      int target_pitch = int_list_top(&polyfilur->note_list);
      pf_voice_slide_note(voice->pitch, target_pitch, parameters, voice);
    }
  }
}

void polyfilur_all_notes_off(const pf_parameters_t* parameters,
                             polyfilur_t* polyfilur) {
  assert(NULL != parameters);
  assert(NULL != polyfilur);

  for (size_t index = 0; index < POLYFILUR_NUM_VOICES; ++index) {
    if (PF_VOICE_STATE_ACTIVE == polyfilur->voices[index].state) {
      pf_voice_note_off(parameters, &polyfilur->voices[index]);
    }
  }
  int_list_clear(&polyfilur->note_list);
}

void polyfilur_process(double* left,
                       double* right,
                       int number_of_frames,
                       const pf_parameters_t* parameters,
                       polyfilur_t* polyfilur) {
  assert(NULL != parameters);
  assert(NULL != polyfilur);

  double* out = left;

  if (polyfilur->voice_mode != parameters->voice_mode) {
    polyfilur->voice_mode = parameters->voice_mode;
    polyfilur_all_notes_off(parameters, polyfilur);
  }

  int num_frames_to_process = number_of_frames;
  double* sub_block_out_buffer = out;
  while (num_frames_to_process > 0) {
    int num_sub_frames = num_frames_to_process;
    if (num_sub_frames > POLYFILUR_MAX_BUFFER_SIZE) {
      num_sub_frames = POLYFILUR_MAX_BUFFER_SIZE;
    }

    polyfilur_sub_process(sub_block_out_buffer, num_sub_frames, parameters, polyfilur);

    sub_block_out_buffer += num_sub_frames;
    num_frames_to_process -= num_sub_frames;
  }

  znf_log_value_set(parameters->volume, &polyfilur->volume);
  process_volume(out, left, right, number_of_frames, &polyfilur->volume);
}

void polyfilur_update_parameters(const pf_parameters_t* parameters, polyfilur_t* polyfilur) {
  assert(NULL != parameters);
  assert(NULL != polyfilur);

  if (polyfilur->voice_mode != parameters->voice_mode) {
    polyfilur->voice_mode = parameters->voice_mode;
    polyfilur_all_notes_off(parameters, polyfilur);
  }

  for (size_t index = 0; index < POLYFILUR_NUM_VOICES; ++index) {
    pf_voice_update_parameters(parameters, &polyfilur->voices[index]);
  }

  znf_log_value_set(parameters->volume, &polyfilur->volume);
  znf_log_value_reset(&polyfilur->volume);
}

size_t polyfilur_num_active_voices(const polyfilur_t* polyfilur) {
  assert(NULL != polyfilur);

  size_t active_voices = 0;
  for (size_t index = 0; index < POLYFILUR_NUM_VOICES; ++index) {
    if (PF_VOICE_STATE_FREE != polyfilur->voices[index].state) {
      ++active_voices;
    }
  }

  return active_voices;
}

static void polyfilur_sub_process(double* out,
                                  int number_of_frames,
                                  const pf_parameters_t* parameters,
                                  polyfilur_t* polyfilur) {
  assert(number_of_frames <= POLYFILUR_MAX_BUFFER_SIZE);

  double osc_lfo_buffer[POLYFILUR_MAX_BUFFER_SIZE];
  double filter_lfo_buffer[POLYFILUR_MAX_BUFFER_SIZE];
  double noise_buffer[POLYFILUR_MAX_BUFFER_SIZE];

  if (polyfilur->voice_mode != parameters->voice_mode) {
    polyfilur->voice_mode = parameters->voice_mode;
    polyfilur_all_notes_off(parameters, polyfilur);
  }

  process_lfo(osc_lfo_buffer, number_of_frames, &polyfilur->osc_lfo, &parameters->osc_lfo);
  process_lfo(filter_lfo_buffer, number_of_frames, &polyfilur->filter_lfo, &parameters->filter_lfo);

  if (PF_MIXER_CH_OFF != parameters->mixer.ch3.enabled) {
    noise_process(noise_buffer, number_of_frames, &polyfilur->noise);
    bilin_hp2_process(noise_buffer, noise_buffer, number_of_frames, &polyfilur->noise_hp);
    bilin_lp_process(noise_buffer, noise_buffer, number_of_frames, &polyfilur->noise_lp);
  }

  for (size_t index = 0; index < POLYFILUR_NUM_VOICES; ++index) {
    pf_voice_t* voice = &polyfilur->voices[index];
    if (PF_VOICE_STATE_FREE != voice->state) {
      pf_voice_process(noise_buffer,
                       osc_lfo_buffer,
                       filter_lfo_buffer,
                       out,
                       number_of_frames,
                       parameters, voice);
    } else {
      pf_voice_update_parameters(parameters, voice);
    }
  }
}

static pf_voice_t* polyfilur_get_voice(polyfilur_t* polyfilur) {
  assert(NULL != polyfilur);

  size_t voice_index = POLYFILUR_NUM_VOICES;

  /* find free voice */
  for (size_t index = 0;
      ((index < POLYFILUR_NUM_VOICES) && (POLYFILUR_NUM_VOICES == voice_index));
      ++index) {
    if (PF_VOICE_STATE_FREE == polyfilur->voices[index].state) {
      voice_index = index;
    }
  }

  if (POLYFILUR_NUM_VOICES == voice_index) {
    /* if no free voice, find oldest in release */
    uint64_t id = 0xffffffffffffffff;
    for (size_t index = 0;
        ((index < POLYFILUR_NUM_VOICES) && (POLYFILUR_NUM_VOICES == voice_index));
        ++index) {
      if ((PF_VOICE_STATE_RELEASE == polyfilur->voices[index].state)
          && (id > polyfilur->voices[index].voice_id)) {
        id = polyfilur->voices[index].voice_id;
        voice_index = index;
      }
    }
  }

  if (POLYFILUR_NUM_VOICES == voice_index) {
    /* if no free voice, find oldest */
    uint64_t id = 0xffffffffffffffff;
    for (size_t index = 0; index < POLYFILUR_NUM_VOICES; ++index) {
      if (id > polyfilur->voices[index].voice_id) {
        id = polyfilur->voices[index].voice_id;
        voice_index = index;
      }
    }
  }

  if (POLYFILUR_NUM_VOICES == voice_index) {
    assert(0);
    voice_index = 0;
  }

  return &polyfilur->voices[voice_index];
}

static void process_volume(const double* in,
                           double* left,
                           double* right,
                           int number_of_frames,
                           znf_log_value_t* volume) {
  if (znf_log_value_equal(volume)) {
    for (int n = 0; n < number_of_frames; ++n) {
      double out = volume->value * in[n];
      left[n] = out;
      right[n] = out;
    }
  } else {
    for (int n = 0; n < number_of_frames; ++n) {
      double out = volume->value * in[n];
      left[n] = out;
      right[n] = out;

      if (!volume->equal) {
        znf_log_value_update(volume);
      }
    }
  }
}

static void process_lfo(double* out,
                        int number_of_frames,
                        lfo_state_t* lfo_state,
                        const pf_lfo_parameters_t* parameters) {
  switch (parameters->waveform) {
    case PF_LFO_WAVEFORM_SINE:
      lfo_process_sine(out, number_of_frames, lfo_state, &parameters->lfo);
      break;

    case PF_LFO_WAVEFORM_TRIANGLE:
      lfo_process_triangle(out, number_of_frames, lfo_state, &parameters->lfo);
      break;

    case PF_LFO_WAVEFORM_SAW:
      lfo_process_saw(out, number_of_frames, lfo_state, &parameters->lfo);
      break;

    default:
      assert(false);
      break;
  }
}
