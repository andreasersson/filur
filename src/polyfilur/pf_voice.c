/*
 * Copyright 2018-2024 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filur/polyfilur/pf_voice.h>

#include <assert.h>
#include <math.h>
#include <stddef.h>
#include <string.h>

static double pf_voice_pitch_to_frequency_table[128 * 100];
static int pf_voice_pitch_to_frequency_initialized = 0;

static const int pf_voice_cent_per_note = 100;
static const double pf_voice_velocity_pos_curve = 3.0;
static const double pf_voice_velocity_neg_curve = 2.0;

static const double pf_mixer_zipper_noise_threshold = 0.05; /* dB */
static const double pf_mixer_zipper_noise_step = 0.005; /* dB */

static uint64_t pf_voice_id = 0;

static void pf_voice_pitch_to_frequency_init(void);
static double pf_voice_pitch_to_frequency(int pitch);
static double velocity_curve(double velocity, double velocity_curve);

void pf_filter_init(double sample_rate, pf_filter_t* filter) {
  assert(NULL != filter);

  biquad_state_init(&filter->biquad_state0);
  biquad_state_init(&filter->biquad_state1);
  biquad_coeffs_init(sample_rate, &filter->biquad_coeffs);

  znf_value_init(1, 0.5, 1.0, &filter->cutoff);
  znf_value_init(0.01, 0.005, 1.0, &filter->resonance);

  filter->type = -1;
  filter->current_cutoff = -1.0;
  filter->velocity = 0.0;

}

void pf_filter_set_velocity(double velocity,
                            const pf_filter_parameters_t* parameters,
                            pf_filter_t* filter) {
  assert(NULL != parameters);
  assert(NULL != filter);

  filter->velocity = parameters->velocity_depth * velocity;
}

void pf_filter_update_parameters(const pf_filter_parameters_t* parameters,
                                 pf_filter_t* filter) {
  assert(NULL != parameters);
  assert(NULL != filter);

  if (parameters->cutoff != filter->cutoff.value) {
    znf_value_set(parameters->cutoff, &filter->cutoff);
    znf_value_reset(&filter->cutoff);
  }

  if (parameters->resonance != filter->resonance.value) {
    znf_value_set(parameters->resonance, &filter->resonance);
    znf_value_reset(&filter->resonance);
  }
}

void pf_filter_process_modulation(double* out,
                                  const int* master_pitch,
                                  const double* env,
                                  const double* lfo,
                                  int number_of_frames,
                                  double mod_wheel,
                                  const pf_filter_parameters_t* parameters,
                                  pf_filter_t* filter) {
  assert(NULL != parameters);
  assert(NULL != parameters);

  double cutoff_scale = 1.0;
  if (PF_KBF_ENABLED == parameters->keyboard_follow_enabled) {
    cutoff_scale = parameters->keyboard_follow_factor;
  }

  znf_value_set(parameters->cutoff, &filter->cutoff);
  for (int n = 0; n < number_of_frames; ++n) {
    if (!filter->cutoff.equal) {
      znf_value_update(&filter->cutoff);
    }
    out[n] = env[n] * parameters->env_depth + lfo[n] * parameters->lfo_depth
        + filter->cutoff.value * cutoff_scale + filter->velocity
        + parameters->mod_wheel_depth * mod_wheel;
  }

  if (PF_KBF_ENABLED == parameters->keyboard_follow_enabled) {
    for (int n = 0; n < number_of_frames; ++n) {
      out[n] += master_pitch[n];
    }
  }
}

void pf_filter_process(const double* in,
                       const double* mod,
                       double* out,
                       int number_of_frames,
                       const pf_filter_parameters_t* parameters,
                       pf_filter_t* filter) {
  assert(NULL != parameters);
  assert(NULL != filter);

  bool update_biquad = false;

  if (filter->type != parameters->type) {
    update_biquad = true;
    filter->type = parameters->type;
  }

  znf_value_set(parameters->resonance, &filter->resonance);

  for (int n = 0; n < number_of_frames; ++n) {
    if (!filter->resonance.equal) {
      znf_value_update(&filter->resonance);
      update_biquad = true;
    }

    double new_cutoff = mod[n];
    if (fabs(new_cutoff - filter->current_cutoff) > 1) {
      update_biquad = true;
      filter->current_cutoff = new_cutoff;
    }

    if (update_biquad) {
      update_biquad = false;
      double frequency = pf_voice_pitch_to_frequency((int)(filter->current_cutoff));
      if (frequency > 20e3) {
        frequency = 20e3;
      }

      switch (parameters->type) {
        case PF_FILTER_TYPE_LOWPASS:
          biquad_coeffs_low_pass(frequency, filter->resonance.value, &filter->biquad_coeffs);
          break;

        case PF_FILTER_TYPE_HIGHPASS:
          biquad_coeffs_high_pass(frequency, filter->resonance.value, 1.0f, &filter->biquad_coeffs);
          break;

        case PF_FILTER_TYPE_BANDPASS:
          biquad_coeffs_band_pass(frequency, filter->resonance.value, &filter->biquad_coeffs);
          break;

        default:
          assert(0);
          break;
      }
    }

    double v = biquad_process_sample(in[n], &filter->biquad_state0, &filter->biquad_coeffs);
    v = biquad_process_sample(v, &filter->biquad_state1, &filter->biquad_coeffs);
    out[n] = v;
  }
}

void pf_osc_init(double sample_rate, pf_osc_t* osc) {
  assert(NULL != osc);

  blit_init(sample_rate, &osc->blit);
  osc->pitch = 0;
}

void pf_osc_restart(pf_osc_t* osc) {
  assert(NULL != osc);

  blit_clear(&osc->blit);
}

void pf_osc_process(double *out,
                    const int *master_pitch,
                    const double *lfo,
                    int number_of_frames,
                    const pf_osc_parameters_t* parameters,
                    pf_osc_t* osc) {
  assert(NULL != parameters);
  assert(NULL != osc);
  assert(number_of_frames <= POLYFILUR_MAX_BUFFER_SIZE);

  double pwm[POLYFILUR_MAX_BUFFER_SIZE];
  int pitch[POLYFILUR_MAX_BUFFER_SIZE];

  for (int n = 0; n < number_of_frames; ++n) {
    pwm[n] = 2 * parameters->pulse_width + lfo[n] * parameters->pwm_depth;
    pitch[n] = master_pitch[n] + parameters->coarse + parameters->fine_tune
        + (int)(lfo[n] * parameters->vibrato_depth);
  }

  switch (parameters->waveform) {
    case PF_OSC_WAVEFORM_SINE:
      for (int n = 0; n < number_of_frames; ++n) {
        if (osc->pitch != pitch[n]) {
          osc->pitch = pitch[n];
          blit_set_frequency(pf_voice_pitch_to_frequency(pitch[n]), &osc->blit);
        }

        out[n] = blit_process_sine(&osc->blit);
      }
      break;

    case PF_OSC_WAVEFORM_SAW:
      for (int n = 0; n < number_of_frames; ++n) {
        if (osc->pitch != pitch[n]) {
          osc->pitch = pitch[n];
          blit_set_frequency(pf_voice_pitch_to_frequency(pitch[n]), &osc->blit);
        }

        out[n] = blit_process_saw(&osc->blit);
      }
      break;

    case PF_OSC_WAVEFORM_PULSE:
      for (int n = 0; n < number_of_frames; ++n) {
        if (osc->pitch != pitch[n]) {
          osc->pitch = pitch[n];
          blit_set_frequency(pf_voice_pitch_to_frequency(pitch[n]), &osc->blit);
        }

        out[n] = blit_process_pulse(pwm[n], &osc->blit);
      }
      break;

    default:
      assert(false);
      break;
  };

}

void pf_mixer_channle_init(pf_mixer_channel_t* mixer_ch) {
  assert(NULL != mixer_ch);

  znf_log_value_init(pf_mixer_zipper_noise_threshold,
                     pf_mixer_zipper_noise_step,
                     1.0,
                     &mixer_ch->volume);
}

void pf_mixer_channle_update_parameters(const pf_mixer_ch_parameters_t* parameters,
                                        pf_mixer_channel_t* mixer_ch) {
  assert(NULL != parameters);
  assert(NULL != mixer_ch);

  if (parameters->volume != mixer_ch->volume.value) {
    znf_log_value_set(parameters->volume, &mixer_ch->volume);
    znf_log_value_reset(&mixer_ch->volume);
  }
}

void pf_mixer_channle_process(const double* in,
                              double* master,
                              double* bypass,
                              int number_of_frames,
                              const pf_mixer_ch_parameters_t* parameters,
                              pf_mixer_channel_t* mixer_ch) {
  assert(NULL != parameters);
  assert(NULL != mixer_ch);

  if (PF_MIXER_CH_OFF != parameters->enabled) {
    double* out = NULL;
    if (PF_MIXER_CH_ON == parameters->enabled) {
      out = master;
    } else {
      out = bypass;
    }

    znf_log_value_set(parameters->volume, &mixer_ch->volume);
    if (znf_log_value_equal(&mixer_ch->volume)) {
      double gain = mixer_ch->volume.value;
      for (int n = 0; n < number_of_frames; ++n) {
        out[n] += gain * in[n];
      }
    } else {
      for (int n = 0; n < number_of_frames; ++n) {
        out[n] += mixer_ch->volume.value * in[n];

        if (!mixer_ch->volume.equal) {
          znf_log_value_update(&mixer_ch->volume);
        }
      }
    }
  }
}

void pf_mixer_init(pf_mixer_t* mixer) {
  assert(NULL != mixer);

  pf_mixer_channle_init(&mixer->ch1);
  pf_mixer_channle_init(&mixer->ch2);
  pf_mixer_channle_init(&mixer->ch3);
}

void pf_mixer_update_parameters(const pf_mixer_parameters_t* parameters,
                                pf_mixer_t* mixer) {
  assert(NULL != parameters);
  assert(NULL != mixer);

  pf_mixer_channle_update_parameters(&parameters->ch1, &mixer->ch1);
  pf_mixer_channle_update_parameters(&parameters->ch2, &mixer->ch2);
  pf_mixer_channle_update_parameters(&parameters->ch3, &mixer->ch3);
}

void pf_mixer_process(const double* ch1,
                      const double* ch2,
                      const double* ch3,
                      double* master,
                      double* bypass,
                      int number_of_frames,
                      const pf_mixer_parameters_t* parameters,
                      pf_mixer_t* mixer) {
  assert(NULL != parameters);
  assert(NULL != mixer);

  pf_mixer_channle_process(ch1, master, bypass, number_of_frames, &parameters->ch1, &mixer->ch1);
  pf_mixer_channle_process(ch2, master, bypass, number_of_frames, &parameters->ch2, &mixer->ch2);
  pf_mixer_channle_process(ch3, master, bypass, number_of_frames, &parameters->ch3, &mixer->ch3);
}

void pf_voice_init(double sample_rate, pf_voice_t* voice) {
  assert(NULL != voice);

  pf_voice_pitch_to_frequency_init();

  voice->voice_id = 0;
  voice->state = PF_VOICE_STATE_FREE;
  voice->silent = PF_VOICE_SILENT_TRUE;
  voice->pitch = -1;
  voice->velocity_gain = 0.0;

  pf_osc_init(sample_rate, &voice->osc1);
  pf_osc_init(sample_rate, &voice->osc2);
  adsr_init_state(&voice->adsr1);
  adsr_init_state(&voice->adsr2);
  pf_filter_init(sample_rate, &voice->filter);
  portamento_init(sample_rate, &voice->portamento);
  pf_mixer_init(&voice->mixer);
}

void pf_voice_note_on(int pitch,
                      double velocity,
                      const pf_parameters_t* parameters,
                      pf_voice_t* voice) {
  assert(NULL != parameters);
  assert(NULL != voice);

  int pitch_in_cent = pitch * pf_voice_cent_per_note;
  voice->pitch = pitch;
  voice->voice_id = pf_voice_id++;

  double local_velocity = velocity;
  if ((parameters->velocity_depth > 0.0) || (parameters->filter.velocity_depth != 0.0)) {
    local_velocity = velocity_curve(velocity, parameters->velocity_curve);
  }
  voice->velocity_gain = (1.0f - parameters->velocity_depth)
      + parameters->velocity_depth * local_velocity;
  pf_filter_set_velocity(local_velocity, &parameters->filter, &voice->filter);
  portamento_set_pitch(pitch_in_cent, &voice->portamento);

  adsr_gate_on(&voice->adsr1);
  adsr_gate_on(&voice->adsr2);

  voice->state = PF_VOICE_STATE_ACTIVE;
  voice->silent = PF_VOICE_SILENT_FALSE;
}

void pf_voice_slide_note(int start_pitch,
                         int target_pitch,
                         const pf_parameters_t* parameters,
                         pf_voice_t* voice) {
  assert(NULL != parameters);
  assert(NULL != voice);

  int start_pitch_in_cent = start_pitch * pf_voice_cent_per_note;
  int target_pitch_in_cent = target_pitch * pf_voice_cent_per_note;
  voice->pitch = target_pitch;
  voice->voice_id = pf_voice_id++;

  if (PF_PORTAMENTO_ENABLED == parameters->portamento.enabled) {
    portamento_set_pitch(start_pitch_in_cent, &voice->portamento);
    portamento_set_target_pitch(target_pitch_in_cent, &voice->portamento);
  } else {
    portamento_set_pitch(target_pitch_in_cent, &voice->portamento);
  }

  if ((PF_VOICE_MODE_MONOLEGATO == parameters->voice_mode)) {
    adsr_gate_on(&voice->adsr1);
    adsr_gate_on(&voice->adsr2);
  }

  voice->state = PF_VOICE_STATE_ACTIVE;
  voice->silent = PF_VOICE_SILENT_FALSE;
}

void pf_voice_note_off(const pf_parameters_t* parameters, pf_voice_t* voice) {
  (void) parameters;

  assert(NULL != parameters);
  assert(NULL != voice);

  voice->state = PF_VOICE_STATE_RELEASE;
  voice->pitch = -1;

  adsr_gate_off(&voice->adsr1);
  adsr_gate_off(&voice->adsr2);
}

void pf_voice_process(const double* noise,
                      const double* osc_lfo,
                      const double* filter_lfo,
                      double* out,
                      int number_of_frames,
                      const pf_parameters_t* parameters,
                      pf_voice_t* voice) {
  assert(NULL != parameters);
  assert(NULL != voice);
  assert(number_of_frames <= POLYFILUR_MAX_BUFFER_SIZE);

  double voice_buffer[POLYFILUR_MAX_BUFFER_SIZE];
  double bypass_buffer[POLYFILUR_MAX_BUFFER_SIZE];
  double osc1_buffer[POLYFILUR_MAX_BUFFER_SIZE];
  double osc2_buffer[POLYFILUR_MAX_BUFFER_SIZE];
  double env1_buffer[POLYFILUR_MAX_BUFFER_SIZE];
  double env2_buffer[POLYFILUR_MAX_BUFFER_SIZE];
  double filter_mod_buffer[POLYFILUR_MAX_BUFFER_SIZE];
  int master_pitch_buffer[POLYFILUR_MAX_BUFFER_SIZE];

  memset(voice_buffer, 0, number_of_frames * sizeof(double));
  memset(bypass_buffer, 0, number_of_frames * sizeof(double));

  adsr_process(env1_buffer, number_of_frames, &voice->adsr1, &parameters->adsr1);
  adsr_process(env2_buffer, number_of_frames, &voice->adsr2, &parameters->adsr2);

  if (parameters->portamento.time != voice->portamento.time) {
    portamento_set_time(parameters->portamento.time, &voice->portamento);
  }

  if ((PF_MIXER_CH_OFF != parameters->mixer.ch1.enabled) ||
      (PF_MIXER_CH_OFF != parameters->mixer.ch2.enabled) ||
      (PF_MIXER_CH_OFF != parameters->mixer.ch3.enabled)) {
    for (int n = 0; n < number_of_frames; ++n) {
      portamento_process(&voice->portamento);

      master_pitch_buffer[n] = voice->portamento.current_pitch
          + (int)(parameters->pitch_bend);
    }
  }

  if (PF_MIXER_CH_OFF != parameters->mixer.ch1.enabled) {
    pf_osc_process(osc1_buffer,
                   master_pitch_buffer,
                   osc_lfo, number_of_frames,
                   &parameters->osc1,
                   &voice->osc1);
  }

  if (PF_MIXER_CH_OFF != parameters->mixer.ch2.enabled) {
    pf_osc_process(osc2_buffer,
                   master_pitch_buffer,
                   osc_lfo, number_of_frames,
                   &parameters->osc2,
                   &voice->osc2);
  }

  pf_mixer_process(osc1_buffer,
                   osc2_buffer,
                   noise,
                   voice_buffer,
                   bypass_buffer,
                   number_of_frames,
                   &parameters->mixer,
                   &voice->mixer);

  pf_filter_process_modulation(filter_mod_buffer,
                               master_pitch_buffer,
                               env2_buffer,
                               filter_lfo,
                               number_of_frames,
                               parameters->mod_wheel,
                               &parameters->filter,
                               &voice->filter);
  pf_filter_process(voice_buffer,
                    filter_mod_buffer,
                    voice_buffer,
                    number_of_frames,
                    &parameters->filter,
                    &voice->filter);

  for (int n = 0; n < number_of_frames; ++n) {
    out[n] += voice->velocity_gain * (voice_buffer[n] + bypass_buffer[n]) * env1_buffer[n];
  }

  if (ADSR_SILENT_TRUE == voice->adsr1.silent) {
    voice->state = PF_VOICE_STATE_FREE;
    voice->silent = PF_VOICE_SILENT_TRUE;
    pf_osc_restart(&voice->osc1);
    pf_osc_restart(&voice->osc2);
  }
}

void pf_voice_update_parameters(const pf_parameters_t* parameters, pf_voice_t* voice) {
  assert(NULL != parameters);
  assert(NULL != voice);

  pf_filter_update_parameters(&parameters->filter, &voice->filter);
  pf_mixer_update_parameters(&parameters->mixer, &voice->mixer);
}

static void pf_voice_pitch_to_frequency_init(void) {
  if (0 == pf_voice_pitch_to_frequency_initialized) {
    double base_frequency = 8.175798915643700;
    double k = pow(2.0, 1.0 / 1200.0);
    double frequency = base_frequency;
    size_t size = sizeof(pf_voice_pitch_to_frequency_table) / sizeof(double);
    for (size_t n = 0; n < size; ++n) {
      pf_voice_pitch_to_frequency_table[n] = frequency;
      frequency *= k;
    }

    pf_voice_pitch_to_frequency_initialized = 1;
  }
}

static double pf_voice_pitch_to_frequency(int pitch) {
  assert(0 != pf_voice_pitch_to_frequency_initialized);

  int size = sizeof(pf_voice_pitch_to_frequency_table) / sizeof(double);

  int index = pitch;
  if (index < 0) {
    index = 0;
  } else if (index >= size) {
    index = size - 1;
  }

  return pf_voice_pitch_to_frequency_table[index];
}

static double velocity_curve(double velocity, double velocity_curve) {
  double curve = 1.0;
  if (velocity_curve >= 0.0) {
    curve = 1.0 + pf_voice_velocity_pos_curve * velocity_curve;
  } else {
    curve = 1.0 / (1.0 + pf_voice_velocity_neg_curve * fabs(velocity_curve));
  }
  double local_velocity = pow(velocity, curve);

  return local_velocity;
}
