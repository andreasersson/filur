/*
 * Copyright 2018-2024 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filur/polyfilur/pf_parameters.h>
#include <filur/tempo_sync.h>

#include <assert.h>
#include <math.h>
#include <stddef.h>

static inline double from_db(double db_value);

void pf_osc_parameters_init(pf_osc_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->coarse = 0;
  parameters->fine_tune = 0;
  parameters->pulse_width = 0.0;
  parameters->pwm_depth = 0.0;
  parameters->vibrato_depth = 0.0;
  parameters->waveform = PF_OSC_WAVEFORM_PULSE;
}

void pf_osc_parameters_set_waveform(pf_osc_waveform_t waveform, pf_osc_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->waveform = waveform;
}

void pf_osc_parameters_set_coarse(int coarse, pf_osc_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->coarse = coarse;
}

void pf_osc_parameters_set_fine_tune(int fine_tune, pf_osc_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->fine_tune = fine_tune;
}

void pf_osc_parameters_set_pulse_width(double pulse_width, pf_osc_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->pulse_width = pulse_width;
}

void pf_osc_parameters_set_pwm_depth(double depth,pf_osc_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->pwm_depth = depth;
}

void pf_osc_parameters_set_vibrato_depth(double depth,pf_osc_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->vibrato_depth = depth;
}

void pf_filter_parameters_init(double keyboard_follow_factor, pf_filter_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->type = PF_FILTER_TYPE_LOWPASS;
  parameters->keyboard_follow_enabled = PF_KBF_DISABLED;
  parameters->cutoff = 8800.0;
  parameters->resonance = 0.7;
  parameters->env_depth = 0.5;
  parameters->lfo_depth = 0.0;
  parameters->velocity_depth = 0.0;
  parameters->mod_wheel_depth = 0.0;
  parameters->keyboard_follow_factor = keyboard_follow_factor;
}

void pf_filter_parameters_set_type(pf_filter_type_t type, pf_filter_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->type = type;
}

void pf_filter_parameters_set_kbf_enabled(pf_kbf_enabled_t enabled,
                                          pf_filter_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->keyboard_follow_enabled = enabled;
}

void pf_filter_parameters_set_cutoff(double cutoff, pf_filter_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->cutoff = cutoff;
}

void pf_filter_parameters_set_resonance(double resonance, pf_filter_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->resonance = resonance;
}

void pf_filter_parameters_set_env_depth(double depth, pf_filter_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->env_depth = depth;
}

void pf_filter_parameters_set_lfo_depth(double depth, pf_filter_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->lfo_depth = depth;
}

void pf_filter_parameters_set_velocity_depth(double depth, pf_filter_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->velocity_depth = depth;
}

void pf_filter_parameters_set_mod_wheel_depth(double depth, pf_filter_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->mod_wheel_depth = depth;
}

void pf_lfo_parameters_init(double sample_rate, pf_lfo_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->waveform = PF_LFO_WAVEFORM_SINE;
  parameters->speed = 0.1;
  parameters->phase = 0.0;
  parameters->tempo_sync = TEMPO_SYNC_FREE_RUNNING;
  parameters->restart = PF_LFO_RESTART_FALSE;
  lfo_init_parameters(sample_rate, &parameters->lfo);
}

void pf_lfo_parameters_set_waveform(pf_lfo_waveform_t waveform,
                                    pf_lfo_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->waveform = waveform;
}

void pf_lfo_parameters_set_speed(double speed, pf_lfo_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->speed = speed;
}

void pf_lfo_parameters_set_phase(double phase, pf_lfo_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->phase = phase;
}

void pf_lfo_parameters_set_tempo_sync(tempo_sync_t tempo_sync, pf_lfo_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->tempo_sync = tempo_sync;
}

void pf_lfo_parameters_set_restart(pf_lfo_restart_t restart,
                                   pf_lfo_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->restart = restart;
}

void pf_portamento_parameters_init(pf_portamento_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->enabled = PF_PORTAMENTO_DISABLED;
  parameters->time = 100e-3;
}

void pf_mixer_ch_parameters_init(pf_mixer_ch_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->enabled = PF_MIXER_CH_OFF;
  parameters->volume = 0.85;
}

void pf_mixer_ch_parameters_set_volume(double volume,
                                       pf_mixer_ch_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->volume = from_db(volume);
}

void pf_mixer_ch_parameters_set_enabled(pf_mixer_ch_state_t state,
                                        pf_mixer_ch_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->enabled = state;
}

void pf_mixer_parameters_init(pf_mixer_parameters_t* parameters) {
  assert(NULL != parameters);

  pf_mixer_ch_parameters_init(&parameters->ch1);
  pf_mixer_ch_parameters_init(&parameters->ch2);
  pf_mixer_ch_parameters_init(&parameters->ch3);
}

void pf_parameters_init(double sample_rate, double keyboard_follow_factor, pf_parameters_t* parameters) {
  assert(NULL != parameters);

  pf_osc_parameters_init(&parameters->osc1);
  pf_osc_parameters_init(&parameters->osc2);
  pf_filter_parameters_init(keyboard_follow_factor, &parameters->filter);
  adsr_init_parameters(sample_rate, &parameters->adsr1);
  adsr_init_parameters(sample_rate, &parameters->adsr2);

  pf_lfo_parameters_init(sample_rate, &parameters->osc_lfo);
  pf_lfo_parameters_init(sample_rate, &parameters->filter_lfo);

  pf_portamento_parameters_init(&parameters->portamento);
  pf_mixer_parameters_init(&parameters->mixer);

  parameters->volume = from_db(-6.0);
  parameters->velocity_depth = 0.0;
  parameters->velocity_curve = 0.0;
  parameters->voice_mode = PF_VOICE_MODE_POLY;
  parameters->pitch_bend = 0.0;
  parameters->pitch_bend_range = 2.0;
  parameters->mod_wheel = 0.0;
  parameters->tempo = 120.0;
}

void pf_parameters_set_tempo(double tempo, pf_parameters_t* parameters) {
  assert(NULL != parameters);

  if (parameters->tempo != tempo) {
    parameters->tempo = tempo;
    pf_parameters_update_lfo_speed(parameters->tempo, &parameters->osc_lfo);
    pf_parameters_update_lfo_speed(parameters->tempo, &parameters->filter_lfo);
  }
}

void pf_parameters_set_volume(double volume, pf_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->volume = from_db(volume);
}

void pf_parameters_set_velocity_depth(double depth, pf_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->velocity_depth = depth;
}

void pf_parameters_set_velocity_curve(double curve, pf_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->velocity_curve = curve;
}

void pf_parameters_set_voice_mode(pf_voice_mode_t voice_mode,
                                  pf_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->voice_mode = voice_mode;
}

void pf_parameters_set_pitch_bend(double bend, pf_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->pitch_bend = 2.0 * (bend - 0.5) * parameters->pitch_bend_range * 100.0;
}

void pf_parameters_set_pitch_bend_range(double range, pf_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->pitch_bend_range = range;
}

void pf_parameters_set_mod_wheel(double mod_wheel, pf_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->mod_wheel = mod_wheel;
}

void pf_parameters_update_lfo_speed(double tempo, pf_lfo_parameters_t* parameters) {
  assert(NULL != parameters);

  double local_frequency = parameters->speed;
  if (parameters->tempo_sync > 0) {
    local_frequency = lfo_tempo_to_frequency(tempo, tempo_sync_to_rate(parameters->tempo_sync));
  }

  lfo_set_frequency(local_frequency, &parameters->lfo);
}

static inline double from_db(double db_value) {
  static const double c = 1.0 / 20.0;
  return pow(10.0, c * db_value);
}
