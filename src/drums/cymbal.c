/*
 * Copyright 2018-2024 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filur/drums/cymbal.h>

#include <filur/utils.h>

#include <assert.h>
#include <stdbool.h>
#include <math.h>
#include <stddef.h>
#include <string.h>

static const double cymbal_min_cutoff = 200.0;
static const double cymbal_max_cutoff = 12e3;
static const double cymbal_min_reso = 1.0;
static const double cymbal_max_reso = 10.0;
static const double cymbal_min_gain = 0.125;
static const double cymbal_max_gain = 1.0;
static const double cymbal_min_decay = 1e-3;
static const double cymbal_swoosh_cutoff = 1000.0;

static const double cymbal_silent_threshold = 1e-4;
static const double cymbal_default_pedal_threshold = 1.0;
static const double cymbal_min_decay_threshold = 0.01;
static const double cymbal_max_decay_threshold = 0.95;

static const double cymbal_choke_time = 1e-3;
static const double cymbal_decay_60dB = -6.90775527898214;

static void cymbal_update_filter(double offset, const cymbal_parameters_t* parameters, cymbal_t* cymbal);

void cymbal_init_parameters(cymbal_parameters_t* parameters) {
  assert(NULL != parameters);

  cymbal_set_gain(1.0, parameters);
  cymbal_set_frequency(200.0, parameters);
  cymbal_set_cutoff(0.6, parameters);
  cymbal_set_resonance(0.2, parameters);
  cymbal_set_early_decay(0.1, parameters);
  cymbal_set_late_decay(0.4, parameters);
  cymbal_set_decay_threshold(cymbal_default_pedal_threshold, parameters);
  cymbal_set_noise(METAL_NOISE_TYPE_4, parameters);
  cymbal_set_swoosh(1.0, parameters);
}

void cymbal_set_gain(double gain, cymbal_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->gain = filur_limit(gain, 0.0, 1.0);
}

void cymbal_set_frequency(double frequency, cymbal_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->frequency = filur_limit(frequency, 40.0, 3000.0);
}

void cymbal_set_early_decay(double decay, cymbal_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->early_decay = filur_max(decay, cymbal_min_decay);
}

void cymbal_set_late_decay(double decay, cymbal_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->late_decay = filur_max(decay, cymbal_min_decay);
}

void cymbal_set_cutoff(double cutoff, cymbal_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->cutoff = filur_limit(cutoff * cutoff, 0.0, 1.0);
}

void cymbal_set_resonance(double resonance, cymbal_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->resonance = filur_limit(resonance, 0.0, 1.0);
}

void cymbal_set_decay_threshold(double threshold,
                                cymbal_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->decay_threshold = filur_limit(threshold, 0.0, 1.0);
}

void cymbal_set_noise(metal_noise_type_t noise, cymbal_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->noise = noise;
}

void cymbal_set_swoosh(double swoosh, cymbal_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->swoosh = filur_limit(swoosh, 0.0, 1.0);
}

void cymbal_init(double sample_rate, cymbal_t* cymbal) {
  assert(NULL != cymbal);

  cymbal->sample_rate = sample_rate;
  cymbal->sample_time = 1.0 / sample_rate;
  cymbal->silent = CYMBAL_SILENT_TRUE;
  cymbal->denormal = 1e-10;
  cymbal->envelope = 0;
  cymbal->envelope_early_a = 0.0;
  cymbal->envelope_late_a = 0.0;
  cymbal->envelope_choked_a = exp(cymbal_decay_60dB / (cymbal->sample_rate * cymbal_choke_time));
  cymbal->decay_threshold = 0.0;
  cymbal->filter_envelope = 0.0;

  cymbal->state = CYMBAL_ENV_STATE_CHOKED;
  cymbal->cutoff_gain = 1.0;
  cymbal->cutoff = 0.0;
  cymbal->resonance = 0.0;

  metal_noise_init(sample_rate, &cymbal->noise);
  biquad_state_init(&cymbal->biquad_state);
  biquad_coeffs_init(sample_rate, &cymbal->biquad_coeffs);
}

void cymbal_process(double* out,
                    int number_of_frames,
                    const cymbal_parameters_t* parameters,
                    cymbal_t* cymbal) {
  assert(NULL != parameters);
  assert(NULL != cymbal);

  if (cymbal->envelope < cymbal_silent_threshold) {
    cymbal->silent = CYMBAL_SILENT_TRUE;
    cymbal->envelope = 0.0;
  }

  if (CYMBAL_SILENT_TRUE == cymbal->silent) {
    memset(out, 0, sizeof(double) * number_of_frames);

    return;
  }

  metal_noise_process(out, number_of_frames, &cymbal->noise);

  bool update_biquad = false;

  if (cymbal->resonance != parameters->resonance) {
    update_biquad = true;
    cymbal->resonance = parameters->resonance;
  }

  for (int n = 0; n < number_of_frames; ++n) {
    cymbal->filter_envelope = cymbal->filter_envelope * cymbal->envelope_late_a;
    double env_cutoff = cymbal->filter_envelope;
    if (env_cutoff > 1.0) env_cutoff = 1.0;

    double cutoff = parameters->cutoff + env_cutoff;
    if (cymbal->cutoff != cutoff) {
      update_biquad = true;
      cymbal->cutoff = cutoff;
    }

    if (update_biquad) {
      update_biquad = false;
      cymbal_update_filter(env_cutoff, parameters, cymbal);
    }

    double sample = biquad_process_sample(out[n], &cymbal->biquad_state, &cymbal->biquad_coeffs);
    out[n] = sample;
  }

  cymbal->denormal = -cymbal->denormal;
  double envelope_a = 0;

  for (int n = 0; n < number_of_frames; ++n) {
    switch (cymbal->state) {
      case CYMBAL_ENV_STATE_EARLY:
        if (cymbal->envelope >= cymbal->decay_threshold) {
          envelope_a = cymbal->envelope_early_a;
        } else {
          cymbal->state = CYMBAL_ENV_STATE_LATE;
          envelope_a = cymbal->envelope_late_a;
        }
        break;

      case CYMBAL_ENV_STATE_LATE:
        envelope_a = cymbal->envelope_late_a;
        break;

      case CYMBAL_ENV_STATE_CHOKED:
        envelope_a = cymbal->envelope_choked_a;
        break;

      default:
        assert(0);
        break;
    }

    cymbal->envelope = cymbal->envelope * envelope_a + cymbal->denormal;
    out[n] = cymbal->cutoff_gain * out[n] * cymbal->envelope;
  }
}

void cymbal_note_on(const cymbal_parameters_t* parameters, cymbal_t *cymbal) {
  assert(NULL != parameters);
  assert(NULL != cymbal);

  metal_noise_set_type(parameters->noise, &cymbal->noise);
  metal_noise_set_frequency(parameters->frequency, &cymbal->noise);

  cymbal->filter_envelope = 1.0;
  cymbal->envelope = filur_min(parameters->gain + cymbal->envelope, 2.0);
  double decay_threshold = cymbal_min_decay_threshold
      + (cymbal_max_decay_threshold - cymbal_min_decay_threshold)
          * parameters->decay_threshold;
  cymbal->decay_threshold = cymbal->envelope * decay_threshold;

  cymbal->envelope_early_a = exp(cymbal_decay_60dB / (cymbal->sample_rate * parameters->early_decay));
  cymbal->envelope_late_a = exp(cymbal_decay_60dB / (cymbal->sample_rate * parameters->late_decay));

  if (CYMBAL_SILENT_TRUE == cymbal->silent) {
    biquad_state_reset(&cymbal->biquad_state);
  }

  cymbal->state = CYMBAL_ENV_STATE_EARLY;
  cymbal->silent = CYMBAL_SILENT_FALSE;
}

void cymbal_choke(const cymbal_parameters_t* parameters, cymbal_t *cymbal) {
  (void) parameters;
  assert(NULL != parameters);
  assert(NULL != cymbal);

  if ((CYMBAL_SILENT_FALSE == cymbal->silent)) {
    cymbal->state = CYMBAL_ENV_STATE_CHOKED;
  }
}

static void cymbal_update_filter(double offset, const cymbal_parameters_t* parameters, cymbal_t* cymbal) {
  assert(NULL != parameters);
  assert(NULL != cymbal);

  cymbal->cutoff_gain = cymbal_min_gain + (cymbal_max_gain - cymbal_min_gain) * parameters->cutoff;
  double cutoff = cymbal_min_cutoff + (cymbal_max_cutoff - cymbal_min_cutoff) * parameters->cutoff;

  if (offset < 0.0) offset = 0.0;
  if (offset > 1.0) offset = 1.0;
  double cutoff_offset = parameters->swoosh * cymbal_swoosh_cutoff * (1.0 - offset);

  cutoff += cutoff_offset;
  double resonance = cymbal_min_reso + (cymbal_max_reso - cymbal_min_reso) * parameters->resonance;
  biquad_coeffs_high_pass(cutoff, resonance, 1.0, &cymbal->biquad_coeffs);
}
