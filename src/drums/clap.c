/*
 * Copyright 2018-2024 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filur/drums/clap.h>

#include <filur/utils.h>

#include <assert.h>
#include <math.h>
#include <stddef.h>
#include <string.h>

static const double clap_early_decay_time = 50e-3;

static const double clap_early_delay_time = 15e-3;
static const double clap_min_early_delay_time = 1e-3;

static const double clap_early_attenuation = 0.5;
static const int clap_number_of_early_delays = 4;
static const double clap_silent_threshold = 0.0001;
static const double clap_min_decay = 1e-3;

static const double clap_hp_cutoff = 1000.0;
static const double clap_hp_q = 0.7071;
static const double clap_bp_min_cutoff = 200.0;
static const double clap_bp_max_cutoff = 2000.0;
static const double clap_bp_min_q = 0.5;
static const double clap_bp_max_q = 4.0;

static const double clap_decay_60dB = -6.90775527898214;

void clap_init_parameters(clap_parameters_t* parameters) {
  assert(NULL != parameters);

  clap_set_gain(1.0, parameters);
  clap_set_decay(0.12, parameters);
  clap_set_early_decay(clap_early_decay_time, parameters);
  clap_set_early_delay(clap_early_delay_time, parameters);
  clap_set_cutoff(0.6, parameters);
  clap_set_resonance(0.5, parameters);
}

void clap_set_gain(double gain, clap_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->gain = filur_limit(gain, 0.0, 1.0);
}

void clap_set_decay(double decay, clap_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->decay = filur_max(decay, clap_min_decay);
}

void clap_set_early_decay(double decay, clap_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->early_decay = filur_max(decay, clap_min_decay);
}

void clap_set_early_delay(double delay, clap_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->early_delay = filur_max(delay, clap_min_early_delay_time);
}

void clap_set_cutoff(double cutoff, clap_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->cutoff = filur_limit(cutoff, 0.0, 1.0);
}

void clap_set_resonance(double resonance, clap_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->resonance = filur_limit(resonance, 0.0, 1.0);
}

void clap_init(double sample_rate, clap_t* clap) {
  assert(NULL != clap);

  clap->sample_rate = sample_rate;
  clap->silent = CLAP_SILENT_TRUE;

  clap->early_env = 0;
  clap->release_env = 0;
  clap->early_a = 0;
  clap->release_a = 0;
  clap->denormal = 1e-10;

  clap->delay = 0;
  clap->early_delay_in_samples = 0;
  clap->early_delay_counter = 0;
  clap->level = 0;

  noise_init(&clap->_noise);
  bilin_hp2_init(sample_rate, &clap->hp);
  biquad_state_init(&clap->biquad_state);
  biquad_coeffs_init(sample_rate, &clap->biquad_coeffs);

  bilin_hp2_set_cutoff(clap_hp_cutoff, &clap->hp);
  bilin_hp2_set_q(clap_hp_q, &clap->hp);
}

void clap_process(double* out,
                  int number_of_frames,
                  const clap_parameters_t* parameters,
                  clap_t* clap) {
  (void) parameters;

  assert(NULL != parameters);
  assert(NULL != clap);

  if ((clap->early_delay_counter > clap_number_of_early_delays) &&
      ((clap->release_env + clap->early_env) < clap_silent_threshold)) {
    clap->silent = CLAP_SILENT_TRUE;
    clap->early_env = 0.0;
    clap->release_env = 0.0;
  }

  if (CLAP_SILENT_TRUE == clap->silent) {
    memset(out, 0, sizeof(double) * number_of_frames);
    return;
  }

  noise_process(out, number_of_frames, &clap->_noise);

  clap->denormal = -clap->denormal;

  for (int n = 0; n < number_of_frames; ++n) {
    if (clap->delay <= 0) {
      if (clap->early_delay_counter < clap_number_of_early_delays) {
        clap->early_env += clap->level;
      } else if (clap->early_delay_counter == clap_number_of_early_delays) {
        clap->release_env += clap->level;
      }

      ++clap->early_delay_counter;
      clap->delay = clap->early_delay_in_samples;
      clap->level *= clap_early_attenuation;
    } else {
      --clap->delay;
    }

    clap->early_env = clap->early_env * clap->early_a + clap->denormal;
    clap->release_env = clap->release_env * clap->release_a + clap->denormal;
    out[n] = 2.0 * out[n] * (clap->early_env + clap->release_env);
  }

  bilin_hp2_process(out, out, number_of_frames, &clap->hp);
  biquad_process(out, out, number_of_frames, &clap->biquad_state, &clap->biquad_coeffs);
}

void clap_choke(const clap_parameters_t* parameters, clap_t* clap) {
  assert(NULL != parameters);
  assert(NULL != clap);

  (void)parameters;

  static const double choke_decay = 1.0e-3;
  clap->release_a = exp(clap_decay_60dB / (clap->sample_rate * choke_decay));
  clap->early_a = clap->release_a;
}

void clap_note_on(const clap_parameters_t* parameters, clap_t* clap) {
  assert(NULL != parameters);
  assert(NULL != clap);

  clap->silent = CLAP_SILENT_FALSE;
  clap->early_delay_counter = 0;
  clap->delay = 0;

  clap->level = filur_min(parameters->gain, 1.0);

  double cutoff = clap_bp_min_cutoff + (clap_bp_max_cutoff - clap_bp_min_cutoff) * parameters->cutoff;
  double q = clap_bp_min_q + (clap_bp_max_q - clap_bp_min_q) * parameters->resonance;
  biquad_coeffs_band_pass(cutoff, q, &clap->biquad_coeffs);

  clap->early_delay_in_samples = (int) (clap->sample_rate * parameters->early_delay);

  clap->early_a = exp(clap_decay_60dB / (clap->sample_rate * parameters->early_decay));
  clap->early_a = clap->early_a * clap->early_a;
  clap->release_a = exp(clap_decay_60dB / (clap->sample_rate * parameters->decay));
}
