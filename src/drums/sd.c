/*
 * Copyright 2018-2024 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filur/drums/sd.h>

#include <filur/sine_approximation.h>
#include <filur/utils.h>

#include <assert.h>
#include <math.h>
#include <stddef.h>
#include <string.h>

#ifndef M_PI
  #define M_PI 3.14159265358979323846264338327950288
#endif /* M_PI */

#define FILUR_SD_MAX_BUFFER_SIZE 1024

static const double sd_envelope_late_decay_threshold = 0.01;
static const double sd_envelope_silent_threshold = 0.0001;

static const double sd_high_tone_frequency_multiplier = 1.9766744;

static const double sd_decay_60dB = -6.90775527898214;

static const double low_pass_cutoff = 8000.0;
static const double high_pass_cutoff = 3600.0;
static const double high_pass_q = 0.707;
static const double max_low_gain = 0.45;
static const double max_high_gain = 0.3;

static void sd_sub_process(double* out, int number_of_frames, sd_t* sd);
static void sd_tone_recalc(sd_tone_t* tone);

void sd_envelope_init(double sample_rate, sd_envelope_t* envelope) {
  assert(NULL != envelope);

  envelope->state = SD_ENVELOPE_STATE_SILENT;
  envelope->sample_rate = sample_rate;
  envelope->short_attack_time = 1.0 / (envelope->sample_rate * 3e-3);
  envelope->long_attack_time = 1.0 / (envelope->sample_rate * 1e-4) - envelope->short_attack_time;
  envelope->attack = envelope->short_attack_time;
  envelope->snappy = 0;
  envelope->denormal = 1e-10;
  envelope->y = 0.0;
}

void sd_envelope_process(double* out, int number_of_frames,
                         sd_envelope_t* envelope) {
  assert(NULL != envelope);

  if ((envelope->state != SD_ENVELOPE_STATE_ATTACK) &&
      (envelope->y < sd_envelope_silent_threshold)) {
    envelope->silent = SD_SILENT_TRUE;
    envelope->y = 0.0;
  }

  if (SD_SILENT_TRUE == envelope->silent) {
    memset(out, 0, sizeof(double) * number_of_frames);

    return;
  }

  envelope->denormal = -envelope->denormal;

  for (int n = 0; n < number_of_frames; ++n) {
    switch (envelope->state) {
      case SD_ENVELOPE_STATE_ATTACK:
        envelope->y += envelope->attack;
        if (envelope->y > envelope->gate) {
          envelope->y = envelope->gate;
          envelope->state = SD_ENVELOPE_STATE_DECAY;
        }
        break;

      case SD_ENVELOPE_STATE_DECAY:
        envelope->y = envelope->y * envelope->early_decay + envelope->denormal;
        if (envelope->y <= sd_envelope_late_decay_threshold) {
          envelope->state = SD_ENVELOPE_STATE_LATE_DECAY;
        }
        break;

      case SD_ENVELOPE_STATE_LATE_DECAY:
        envelope->y = envelope->y * envelope->late_decy + envelope->denormal;
        break;

      case SD_ENVELOPE_STATE_SILENT:
        envelope->y = 0.0;
        break;
    };

    out[n] = envelope->y;
  }
}

void sd_envelope_start(double accent, sd_envelope_t* envelope) {
  assert(NULL != envelope);

  envelope->attack = envelope->short_attack_time + envelope->snappy * envelope->long_attack_time;
  envelope->gate = envelope->snappy * accent;

  envelope->y = 0.0;
  envelope->state = SD_ENVELOPE_STATE_ATTACK;
  envelope->silent = SD_SILENT_FALSE;

  if (envelope->gate < sd_envelope_silent_threshold) {
    envelope->state = SD_ENVELOPE_STATE_SILENT;
    envelope->silent = SD_SILENT_FALSE;
  }
}

void sd_envelope_set_decay(double decay, sd_envelope_t* envelope) {
  assert(NULL != envelope);

  envelope->decay = decay;
  double a = log(sd_envelope_late_decay_threshold);
  double b = log(sd_envelope_silent_threshold / sd_envelope_late_decay_threshold);
  double ts = 2.0 / (envelope->decay * envelope->sample_rate);
  envelope->early_decay = exp(a * ts);
  envelope->late_decy = exp(b * ts);
}

void sd_envelope_set_snappy(double snappy, sd_envelope_t* envelope) {
  assert(NULL != envelope);

  envelope->snappy = snappy;
  if (envelope->snappy < 0) {
    envelope->snappy = 0;
  } else if (envelope->snappy > 1.0) {
    envelope->snappy = 1.0;
  }
}

void sd_tone_init(double sample_rate, sd_tone_t* tone) {
  assert(NULL != tone);

  tone->sample_rate = sample_rate;
  tone->sample_time = 1.0 / tone->sample_rate;
  tone->x = 0;
  tone->dx = 0;

  sd_tone_set_frequency(200, tone);
}

void sd_tone_process(double* out, int number_of_frames, sd_tone_t* tone) {
  assert(NULL != tone);

  for (int n = 0; n < number_of_frames; ++n) {
    tone->x += tone->dx;

    if (tone->x > M_PI) {
      tone->x -= 2 * M_PI;
    }

    out[n] = sine_11(tone->x);
  }
}

void sd_tone_set_frequency(double frequency, sd_tone_t* tone) {
  assert(NULL != tone);

  tone->frequency = frequency;
  sd_tone_recalc(tone);
}

void sd_init(double sample_rate, sd_t* sd) {
  assert(NULL != sd);

  sd->silent = SD_SILENT_TRUE;

  sd->env = 0;
  sd->envd = 0;
  sd->enva = 1.0 / (3e-3 * sample_rate);
  sd->env_state = 0;

  sd->low_gain = 0;
  sd->high_gain = 0;

  sd_tone_init(sample_rate, &sd->low_tone);
  sd_tone_init(sample_rate, &sd->high_tone);
  sd_envelope_init(sample_rate, &sd->envelope);
  noise_init(&sd->noise);
  bilin_lp_init(sample_rate, &sd->lp);
  bilin_hp2_init(sample_rate, &sd->hp);

  bilin_hp2_set_cutoff(high_pass_cutoff, &sd->hp);
  bilin_hp2_set_q(high_pass_q, &sd->hp);

  bilin_lp_set_cutoff(low_pass_cutoff, &sd->lp);

  sd_set_gain(1.0, sd);
  sd_set_tone(0.5, sd);
  sd_set_snappy(0.3, sd);
  sd_set_decay(0.1, sd);
  sd_set_frequency(240.0, sd);
}

void sd_process(double* out, int number_of_frames, sd_t* sd) {
  assert(NULL != sd);

  if ((SD_SILENT_TRUE == sd->envelope.silent) &&
      (sd->env < sd_envelope_silent_threshold)) {
    sd->silent = SD_SILENT_TRUE;
    sd->env = 0.0;
  }

  if (SD_SILENT_TRUE == sd->silent) {
    memset(out, 0, sizeof(double) * number_of_frames);

    return;
  }

  int num_frames_to_process = number_of_frames;
  double* sub_block_out_buffer = out;
  while (num_frames_to_process > 0) {
    int num_sub_frames = num_frames_to_process;
    if (num_sub_frames > FILUR_SD_MAX_BUFFER_SIZE) {
      num_sub_frames = FILUR_SD_MAX_BUFFER_SIZE;
    }

    sd_sub_process(sub_block_out_buffer, num_sub_frames, sd);

    sub_block_out_buffer += num_sub_frames;
    num_frames_to_process -= num_sub_frames;
  }
}

void sd_choke(sd_t* sd) {
  assert(NULL != sd);

  static const double choke_decay = 20.0e-3;
  sd_envelope_set_decay(choke_decay, &sd->envelope);
  sd->envd = exp(sd_decay_60dB * sd->low_tone.sample_time / choke_decay);
  sd->env_state = 1;
}

void sd_note_on(sd_t* sd) {
  assert(NULL != sd);

  sd->silent = SD_SILENT_FALSE;

  sd_envelope_set_decay(sd->decay, &sd->envelope);
  sd_envelope_set_snappy(sd->snappy, &sd->envelope);
  sd_envelope_start(sd->gain, &sd->envelope);

  sd->envd = exp(sd_decay_60dB * sd->low_tone.sample_time / sd->decay);
  sd_tone_set_frequency(sd->frequency, &sd->low_tone);
  sd_tone_set_frequency(sd->frequency * sd_high_tone_frequency_multiplier, &sd->high_tone);

  sd->low_gain = max_low_gain * filur_limit(1.0 - sd->tone, 0.0, 1.0);
  sd->high_gain = max_high_gain * filur_limit(sd->tone, 0.0, 1.0);

  sd->env = 0;
  sd->env_state = 0;
}

void sd_set_gain(double gain, sd_t* sd) {
  assert(NULL != sd);

  sd->gain = filur_limit(gain, 0.0, 1.0);
}

void sd_set_tone(double tone, sd_t* sd) {
  assert(NULL != sd);

  sd->tone = filur_limit(tone, 0.0, 1.0);
}

void sd_set_snappy(double snappy, sd_t* sd) {
  assert(NULL != sd);

  sd->snappy = filur_limit(snappy, 0.0, 1.0);
}

void sd_set_decay(double decay, sd_t* sd) {
  assert(NULL != sd);

  sd->decay = decay;
  if (sd->decay < 0.0) {
    sd->decay = 0.0;
  }
}

void sd_set_frequency(double frequency, sd_t* sd) {
  assert(NULL != sd);

  sd->frequency = frequency;
  if (sd->frequency < 0.0) {
    sd->frequency = 0.0;
  }
}

static void sd_sub_process(double* out, int number_of_frames, sd_t* sd) {
  assert(number_of_frames <= FILUR_SD_MAX_BUFFER_SIZE);

  double low_tone[FILUR_SD_MAX_BUFFER_SIZE];
  double high_tone[FILUR_SD_MAX_BUFFER_SIZE];
  double env[FILUR_SD_MAX_BUFFER_SIZE];
  double noise[FILUR_SD_MAX_BUFFER_SIZE];

  sd_tone_process(low_tone, number_of_frames, &sd->low_tone);
  sd_tone_process(high_tone, number_of_frames, &sd->high_tone);

  double low_tone_gain = sd->low_gain * sd->gain;
  double high_tone_gain = sd->high_gain * sd->gain;

  for (int n = 0; n < number_of_frames; ++n) {
    if (sd->env_state == 0) {
      sd->env += sd->enva;
      if (sd->env >= 1.0) {
        sd->env_state = 1;
        sd->env = 1.0;
      }
    } else {
      sd->env = sd->env * sd->envd;
    }

    low_tone[n] = low_tone_gain * low_tone[n] * sd->env;
    high_tone[n] = high_tone_gain * high_tone[n] * sd->env;
  }

  noise_process(noise, number_of_frames, &sd->noise);
  sd_envelope_process(env, number_of_frames, &sd->envelope);

  for (int n = 0; n < number_of_frames; ++n) {
    noise[n] = filur_limit(noise[n] * env[n], -1.0, 1.0);
  }

  bilin_hp2_process(noise, noise, number_of_frames, &sd->hp);
  bilin_lp_process(noise, noise, number_of_frames, &sd->lp);

  for (int n = 0; n < number_of_frames; ++n) {
    out[n] = low_tone[n] + high_tone[n] + noise[n];
  }
}

void sd_tone_recalc(sd_tone_t* tone) {
  assert(NULL != tone);

  tone->dx = 2 * M_PI * tone->frequency * tone->sample_time;
}
