/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filur/drums/hihat.h>

#include <filur/utils.h>

#include <assert.h>
#include <math.h>
#include <stddef.h>
#include <string.h>

static const double hihat_min_cutoff = 2000.0;
static const double hihat_max_cutoff = 12e3;
static const double hihat_min_reso = 1.0;
static const double hihat_max_reso = 10.0;
static const double hihat_min_gain = 0.125;
static const double hihat_max_gain = 1.0;
static const double hihat_min_decay = 1e-3;

static const double hihat_silent_threshold = 1e-4;
static const double hihat_default_pedal_threshold = 1.0;
static const double hihat_min_pedal_threshold = 0.5;
static const double hihat_max_pedal_threshold = 0.95;

static const double hihat_choke_time = 1e-3;
static const double hihat_decay_60dB = -6.90775527898214;

static void hihat_on(hihat_envelope_state_t state,
                     const hihat_parameters_t* parameters,
                     hihat_t* hihat);
static void hihat_update_filter(const hihat_parameters_t* parameters,
                                hihat_t* hihat);

void hihat_init_parameters(hihat_parameters_t* parameters) {
  assert(NULL != parameters);

  hihat_set_gain(1.0, parameters);
  hihat_set_cutoff(0.6, parameters);
  hihat_set_resonance(0.2, parameters);
  hihat_set_closed_decay(0.1, parameters);
  hihat_set_pedal_decay(0.4, parameters);
  hihat_set_open_decay(2.0, parameters);
  hihat_set_pedal_threshold(hihat_default_pedal_threshold, parameters);
  hihat_set_noise(METAL_NOISE_TYPE_1, parameters);
  hihat_set_frequency(100, parameters);
}

void hihat_set_gain(double gain, hihat_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->gain = filur_limit(gain, 0.0, 1.0);
}

void hihat_set_closed_decay(double decay, hihat_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->closed_decay = filur_max(decay, hihat_min_decay);
}

void hihat_set_pedal_decay(double decay, hihat_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->pedal_decay = filur_max(decay, hihat_min_decay);
}

void hihat_set_open_decay(double decay, hihat_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->open_decay = filur_max(decay, hihat_min_decay);
}

void hihat_set_cutoff(double cutoff, hihat_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->cutoff = filur_limit(cutoff * cutoff, 0.0, 1.0);
}

void hihat_set_resonance(double resonance, hihat_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->resonance = filur_limit(resonance, 0.0, 1.0);
}

void hihat_set_pedal_threshold(double threshold, hihat_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->pedal_threshold = filur_limit(threshold, 0.0, 1.0);
}

void hihat_set_noise(metal_noise_type_t noise, hihat_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->noise = noise;
}

void hihat_set_frequency(double frequency, hihat_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->frequency = filur_limit(frequency, 40.0, 3000.0);
}

void hihat_init(double sample_rate, hihat_t* hihat) {
  assert(NULL != hihat);

  hihat->sample_rate = sample_rate;
  hihat->sample_time = 1.0 / sample_rate;
  hihat->silent = HIHAT_SILENT_TRUE;
  hihat->denormal = 1e-10;
  hihat->envelope = 0;
  hihat->envelope_closed_a = 0.0;
  hihat->envelope_pedal_a = 0.0;
  hihat->envelope_open_a = 0.0;
  hihat->envelope_choked_a = exp(hihat_decay_60dB / (hihat->sample_rate * hihat_choke_time));
  hihat->pedal_threshold = 0.0;

  hihat->state = HIHAT_ENV_STATE_CLOSED;
  hihat->cutoff_gain = 1.0;
  hihat->cutoff = 0.0;
  hihat->resonance = 0.0;

  metal_noise_init(sample_rate, &hihat->noise);
  biquad_state_init(&hihat->biquad_state);
  biquad_coeffs_init(sample_rate, &hihat->biquad_coeffs);
}

void hihat_process(double* out,
                   int number_of_frames,
                   const hihat_parameters_t* parameters,
                   hihat_t* hihat) {
  assert(NULL != parameters);
  assert(NULL != hihat);

  if (hihat->envelope < hihat_silent_threshold) {
    hihat->silent = HIHAT_SILENT_TRUE;
    hihat->envelope = 0.0;
  }

  if (HIHAT_SILENT_TRUE == hihat->silent) {
    memset(out, 0, sizeof(double) * number_of_frames);

    return;
  }

  metal_noise_process(out, number_of_frames, &hihat->noise);

  if ((hihat->cutoff != parameters->cutoff) ||
      (hihat->resonance != parameters->resonance)) {
    hihat_update_filter(parameters, hihat);
    hihat->cutoff = parameters->cutoff;
    hihat->resonance = parameters->resonance;
  }

  biquad_process(out, out, number_of_frames, &hihat->biquad_state, &hihat->biquad_coeffs);

  hihat->denormal = -hihat->denormal;
  double envelope_a = 0.0;

  for (int n = 0; n < number_of_frames; ++n) {
    switch (hihat->state) {
      case HIHAT_ENV_STATE_CLOSED:
        envelope_a = hihat->envelope_closed_a;
        break;

      case HIHAT_ENV_STATE_PEDAL:
        if (hihat->envelope >= hihat->pedal_threshold) {
          envelope_a = hihat->envelope_pedal_a;
        } else {
          envelope_a = hihat->envelope_choked_a;
        }
        break;

      case HIHAT_ENV_STATE_OPEN:
        envelope_a = hihat->envelope_open_a;
        break;

      case HIHAT_ENV_STATE_CHOKED:
        envelope_a = hihat->envelope_choked_a;
        break;

      default:
        assert(0);
        break;
    }

    hihat->envelope = hihat->envelope * envelope_a + hihat->denormal;
    out[n] = hihat->cutoff_gain * out[n] * hihat->envelope;
  }
}

void hihat_closed_on(const hihat_parameters_t* parameters, hihat_t *hihat) {
  assert(NULL != parameters);
  assert(NULL != hihat);

  hihat_on(HIHAT_ENV_STATE_CLOSED, parameters, hihat);
}

void hihat_pedal_on(const hihat_parameters_t* parameters, hihat_t *hihat) {
  assert(NULL != hihat);

  hihat_on(HIHAT_ENV_STATE_PEDAL, parameters, hihat);
}

void hihat_open_on(const hihat_parameters_t* parameters, hihat_t *hihat) {
  assert(NULL != hihat);

  hihat_on(HIHAT_ENV_STATE_OPEN, parameters, hihat);
}

void hihat_choke(const hihat_parameters_t* parameters, hihat_t *hihat) {
  (void) parameters;

  assert(NULL != parameters);
  assert(NULL != hihat);

  if ((HIHAT_SILENT_FALSE == hihat->silent)) {
    hihat->state = HIHAT_ENV_STATE_CHOKED;
  }
}

static void hihat_on(hihat_envelope_state_t state,
                     const hihat_parameters_t* parameters,
                     hihat_t* hihat) {
  assert(NULL != parameters);
  assert(NULL != hihat);

  hihat->state = state;

  hihat->envelope = filur_min(parameters->gain + hihat->envelope, 2.0);
  double pedal_threshold = hihat_max_pedal_threshold - (hihat_max_pedal_threshold - hihat_min_pedal_threshold) * (parameters->pedal_threshold);
  hihat->pedal_threshold = pedal_threshold * hihat->envelope;

  metal_noise_set_type(parameters->noise, &hihat->noise);
  metal_noise_set_frequency(parameters->frequency, &hihat->noise);

  switch (hihat->state) {
    case HIHAT_ENV_STATE_CLOSED:
      hihat->envelope_closed_a = exp(hihat_decay_60dB / (hihat->sample_rate * parameters->closed_decay));
      break;

    case HIHAT_ENV_STATE_PEDAL:
      hihat->envelope_pedal_a = exp(hihat_decay_60dB / (hihat->sample_rate * parameters->pedal_decay));
      break;

    case HIHAT_ENV_STATE_OPEN:
      hihat->envelope_open_a = exp(hihat_decay_60dB / (hihat->sample_rate * parameters->open_decay));
      break;

    default:
      break;
  }

  if (HIHAT_SILENT_TRUE == hihat->silent) {
    biquad_state_reset(&hihat->biquad_state);
  }

  hihat->silent = HIHAT_SILENT_FALSE;
}

static void hihat_update_filter(const hihat_parameters_t* parameters,
                                hihat_t* hihat) {
  assert(NULL != hihat);

  hihat->cutoff_gain = hihat_min_gain + (hihat_max_gain - hihat_min_gain) * parameters->cutoff;
  double cutoff = hihat_min_cutoff + (hihat_max_cutoff - hihat_min_cutoff) * parameters->cutoff;
  double resonance = hihat_min_reso + (hihat_max_reso - hihat_min_reso) * parameters->resonance;
  biquad_coeffs_high_pass(cutoff, resonance, 1.0, &hihat->biquad_coeffs);
}
