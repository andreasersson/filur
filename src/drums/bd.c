/*
 * Copyright 2018-2024 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filur/drums/bd.h>
#include <filur/sine_approximation.h>
#include <filur/utils.h>

#include <assert.h>
#include <math.h>
#include <stddef.h>
#include <string.h>

#ifndef M_PI
  #define M_PI 3.14159265358979323846264338327950288
#endif /* M_PI */

#define FILUR_BD_MAX_BUFFER_SIZE 1024

static const double bd_tone_sweep_frequency_offset = 225.0;

static const double bd_envelope_silent_threshold = 0.001;
static const double bd_envelope_attack_time = 6.4e-3;
static const double bd_envelope_threshold_c1 = 15.0;
static const double bd_envelope_threshold_c2 = 225.0;

static const double bd_high_tone_frequency_multiplier = 1.9766744;
static const double bd_min_decay = 1e-3;

static void bd_sub_process(double* out,
                           int number_of_frames,
                           const bd_parameters_t* parameters,
                           bd_t* bd);

static void bd_on(double frequency,
                  double high_frequency,
                  double sweep_decay,
                  double sweep_threshold,
                  const bd_parameters_t* parameters,
                  bd_t* bd);

void bd_tone_init(double sample_rate, bd_tone_t* tone) {
  assert(NULL != tone);

  tone->sample_time = 1.0f / sample_rate;
  tone->x = 0;
  tone->dx = 0;

  bd_tone_set_frequency(200, 400, tone);
}

void bd_tone_process(const double* envelope,
                     double* out,
                     int number_of_frames,
                     bd_tone_t* tone) {
  assert(NULL != tone);

  double two_pi_ts = 2 * M_PI * tone->sample_time;
  double sweep_end = two_pi_ts * tone->frequency;
  double sweep_start = (tone->high_frequency - tone->frequency) * two_pi_ts;
  for (int n = 0; n < number_of_frames; ++n) {
    tone->dx = sweep_end + sweep_start * envelope[n];
    tone->x += tone->dx;
    if (tone->x > M_PI) {
      tone->x -= 2 * M_PI;
    }
    out[n] = sine_11(tone->x);
  }
}

void bd_tone_set_frequency(double frequency, double high_frequency, bd_tone_t* tone) {
  assert(NULL != tone);

  tone->frequency = frequency;
  tone->high_frequency = high_frequency;
}

void bd_envelope_init(double sample_rate, bd_envelope_t* envelope) {
  assert(NULL != envelope);

  envelope->sample_rate = sample_rate;
  envelope->y = 0.0;
  envelope->attack = 0.0;
  envelope->decay = 0.0;
  envelope->denormal = 1e-10;
  envelope->threshold = 0.0;
  envelope->silent = BD_SILENT_TRUE;
}

void bd_envelope_set_decay(double decay, bd_envelope_t* envelope) {
  assert(NULL != envelope);

  envelope->decay = exp(-6.90775527898214 / (decay * envelope->sample_rate));
}

void bd_envelope_set_threshold(double threshold, bd_envelope_t* envelope) {
  assert(NULL != envelope);

  envelope->threshold = threshold;
  envelope->attack = (1.0 - envelope->threshold) / (bd_envelope_attack_time * envelope->sample_rate);
}

void bd_envelope_start(bd_envelope_t* envelope) {
  assert(NULL != envelope);

  envelope->y = 1.0;
  envelope->silent = BD_SILENT_FALSE;
}

void bd_envelope_process(double* out, int number_of_frames, bd_envelope_t* envelope) {
  assert(NULL != envelope);

  if (BD_SILENT_TRUE == envelope->silent) {
    memset(out, 0, sizeof(double) * number_of_frames);
    return;
  }

  envelope->denormal = -envelope->denormal;

  for (int n = 0; n < number_of_frames; ++n) {
    if (envelope->y > envelope->threshold) {
      envelope->y -= envelope->attack;
    } else {
      envelope->y = envelope->y * envelope->decay + envelope->denormal;
    }

    out[n] = envelope->y;
  }

  if (envelope->y < bd_envelope_silent_threshold) {
    envelope->silent = BD_SILENT_TRUE;
  }
}

void bd_init_parameters(bd_parameters_t* parameters) {
  assert(NULL != parameters);

  bd_set_tone(0.5, parameters);
  bd_set_decay(0.05, parameters);
  bd_set_sweep_decay(500e-3, parameters);
  bd_set_frequency(50.0, parameters);
  bd_set_sweep(1.0, parameters);
  bd_set_sweep_threshold(0.01, parameters);
}

void bd_init(double sample_rate, bd_t* bd) {
  assert(NULL != bd);

  bd_tone_init(sample_rate, &bd->low_tone);
  bd_tone_init(sample_rate, &bd->high_tone);

  ar_envelope_init(sample_rate, &bd->low_decay_envelope);
  ar_envelope_init(sample_rate, &bd->high_decay_envelope);

  bd_envelope_init(sample_rate, &bd->envelope);

  bd->denormal = 1e-10f;
  bd->tone_gain = 1;
  bd->tga = 0.998576255f;
  bd->tgk = 1 - bd->tga;

  bd->silent = BD_SILENT_TRUE;
}

void bd_process(double* out, int number_of_frames,
                const bd_parameters_t* parameters,
                bd_t* bd) {
  assert(NULL != parameters);
  assert(NULL != bd);

  if (BD_SILENT_TRUE == bd->silent) {
    memset(out, 0, sizeof(double) * number_of_frames);
    return;
  }

  int num_frames_to_process = number_of_frames;
  double* sub_block_out_buffer = out;
  while (num_frames_to_process > 0) {
    int num_sub_frames = num_frames_to_process;
    if (num_sub_frames > FILUR_BD_MAX_BUFFER_SIZE) {
      num_sub_frames = FILUR_BD_MAX_BUFFER_SIZE;
    }

    bd_sub_process(sub_block_out_buffer, num_sub_frames, parameters, bd);

    sub_block_out_buffer += num_sub_frames;
    num_frames_to_process -= num_sub_frames;
  }

  bd->silent = bd->low_decay_envelope.silent && bd->high_decay_envelope.silent;
}

void bd_choke(const bd_parameters_t* parameters, bd_t* bd) {
  assert(NULL != parameters);
  assert(NULL != bd);

  (void)parameters;

  static const double choke_decay = 20.0e-3;
  ar_envelope_set_decay(choke_decay, &bd->low_decay_envelope);
  ar_envelope_set_decay(choke_decay, &bd->high_decay_envelope);
  bd_envelope_set_decay(choke_decay, &bd->envelope);
}

void bd_note_on(const bd_parameters_t* parameters, bd_t* bd) {
  assert(NULL != parameters);
  assert(NULL != bd);

  double thump_decay = parameters->decay;
  if (thump_decay < bd_min_decay) {
    thump_decay = bd_min_decay;
  }

  ar_envelope_set_decay(parameters->decay, &bd->low_decay_envelope);
  ar_envelope_set_decay(thump_decay, &bd->high_decay_envelope);
  ar_envelope_start(&bd->low_decay_envelope);
  ar_envelope_start(&bd->high_decay_envelope);

  bd_envelope_set_decay(parameters->sweep_decay, &bd->envelope);
  bd_envelope_set_threshold(parameters->sweep_threshold, &bd->envelope);
  bd_envelope_start(&bd->envelope);

  double frequency = parameters->frequency;
  double high_frequency = parameters->frequency + bd_tone_sweep_frequency_offset * parameters->sweep;
  bd_tone_set_frequency(frequency, high_frequency, &bd->low_tone);
  bd_tone_set_frequency(frequency * bd_high_tone_frequency_multiplier,
                        high_frequency * bd_high_tone_frequency_multiplier,
                        &bd->high_tone);

  bd->silent = BD_SILENT_FALSE;
}

void bd_note_on_classic(const bd_parameters_t* parameters, bd_t* bd) {
  assert(NULL != parameters);
  assert(NULL != bd);

  double threshold = bd_envelope_threshold_c1 / (bd_envelope_threshold_c2 + parameters->frequency);
  double sweep_decay = parameters->decay;
  double frequency = parameters->frequency;
  double high_frequency = parameters->frequency + bd_tone_sweep_frequency_offset;
  bd_on(frequency, high_frequency, sweep_decay, threshold, parameters, bd);
}

void bd_note_on_kick(const bd_parameters_t* parameters, bd_t* bd) {
  assert(NULL != parameters);
  assert(NULL != bd);

  double frequency = parameters->frequency;
  double high_frequency = parameters->frequency + bd_tone_sweep_frequency_offset * parameters->sweep;
  bd_on(frequency,
        high_frequency,
        parameters->sweep_decay,
        parameters->sweep_threshold,
        parameters,
        bd);
}

void bd_note_on_tom(const bd_parameters_t* parameters, bd_t* bd) {
  assert(NULL != parameters);
  assert(NULL != bd);

  double frequency = parameters->frequency;
  double high_frequency = parameters->frequency + bd_tone_sweep_frequency_offset;
  bd_on(frequency,
        high_frequency,
        parameters->sweep_decay,
        parameters->sweep_threshold,
        parameters,
        bd);
}

void bd_set_decay(double decay, bd_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->decay = filur_max(bd_min_decay, decay);
}

void bd_set_sweep_decay(double sweep_decay, bd_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->sweep_decay = filur_max(bd_min_decay, sweep_decay);
}

void bd_set_frequency(double frequency, bd_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->frequency = filur_max(0.0, frequency);
}

void bd_set_tone(double tone, bd_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->tone = filur_limit(tone, 0.0, 1.0);
}

void bd_set_gain(double gain, bd_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->gain = filur_limit(gain, 0.0, 1.0);
}

void bd_set_sweep(double sweep, bd_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->sweep = filur_max(0.0, sweep);
}

void bd_set_sweep_threshold(double sweep_threshold, bd_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->sweep_threshold = filur_limit(sweep_threshold, 0.0, 1.0);
}

static void bd_sub_process(double* out,
                           int number_of_frames,
                           const bd_parameters_t* parameters,
                           bd_t* bd) {
  assert(number_of_frames <= FILUR_BD_MAX_BUFFER_SIZE);

  double env[FILUR_BD_MAX_BUFFER_SIZE];
  double low_env[FILUR_BD_MAX_BUFFER_SIZE];
  double high_env[FILUR_BD_MAX_BUFFER_SIZE];
  double highTone[FILUR_BD_MAX_BUFFER_SIZE];
  double lowTone[FILUR_BD_MAX_BUFFER_SIZE];

  bd_envelope_process(env, number_of_frames, &bd->envelope);

  bd_tone_process(env, lowTone, number_of_frames, &bd->low_tone);
  bd_tone_process(env, highTone, number_of_frames, &bd->high_tone);

  ar_envelope_process(low_env, number_of_frames, &bd->low_decay_envelope);
  ar_envelope_process(high_env, number_of_frames, &bd->high_decay_envelope);

  for (int n = 0; n < number_of_frames; ++n) {
    bd->tone_gain = bd->tgk * parameters->tone + bd->tone_gain * bd->tga + bd->denormal;
    double high_gain = 0.5 * bd->tone_gain;
    double low_gain = 0.5 + (0.5 - high_gain);

    out[n] = parameters->gain * (low_gain * (lowTone[n] * low_env[n]) + (high_gain * highTone[n] * high_env[n]));
  }
}

static void bd_on(double frequency,
                  double high_frequency,
                  double sweep_decay,
                  double sweep_threshold,
                  const bd_parameters_t* parameters,
                  bd_t* bd) {
  assert(NULL != parameters);
  assert(NULL != bd);

  ar_envelope_set_decay(parameters->decay, &bd->low_decay_envelope);
  ar_envelope_set_decay(parameters->decay, &bd->high_decay_envelope);
  ar_envelope_start(&bd->low_decay_envelope);
  ar_envelope_start(&bd->high_decay_envelope);

  bd_envelope_set_decay(sweep_decay, &bd->envelope);
  bd_envelope_set_threshold(sweep_threshold, &bd->envelope);
  bd_envelope_start(&bd->envelope);

  bd_tone_set_frequency(frequency, high_frequency, &bd->low_tone);
  bd_tone_set_frequency(frequency * bd_high_tone_frequency_multiplier,
                        high_frequency * bd_high_tone_frequency_multiplier,
                        &bd->high_tone);

  bd->silent = BD_SILENT_FALSE;
}
