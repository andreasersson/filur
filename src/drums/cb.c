/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filur/drums/cb.h>

#include <filur/utils.h>

#include <assert.h>
#include <math.h>
#include <stddef.h>
#include <string.h>

#define FILUR_CB_MAX_BUFFER_SIZE 1024

static const double cb_min_decay = 1e-3;
static const double cb_frequency_multiplier = 1.48148;
static const double cb_gain = 0.0625;

static void cb_sub_process(double* out, int number_of_frames, cb_t* cb);

void cb_init(double sample_rate, cb_t* cb) {
  assert(NULL != cb);

  cb->sample_time = 1.0 / sample_rate;
  cb->silent = CB_SILENT_TRUE;

  cb->osc0 = 0;
  cb->osc1 = 0;

  cb_set_gain(1.0, cb);
  cb_set_decay(50e-3, cb);
  cb_set_frequency(220.0, cb);

  ar_envelope_init(sample_rate, &cb->envelope);
  ar_envelope_set_late_decay_threshold(0.0025, &cb->envelope);

  biquad_state_init(&cb->biquad_state);
  biquad_coeffs_init(sample_rate, &cb->biquad_coeffs);
  biquad_coeffs_low_pass(2000.0, 0.707, &cb->biquad_coeffs);
}

void cb_process(double* out, int number_of_frames, cb_t* cb) {
  assert(NULL != cb);
  assert(NULL != out);

  if (CB_SILENT_TRUE == cb->silent) {
    memset(out, 0, sizeof(double) * number_of_frames);

    return;
  }

  int num_frames_to_process = number_of_frames;
  double* sub_block_out_buffer = out;
  while (num_frames_to_process > 0) {
    int num_sub_frames = num_frames_to_process;
    if (num_sub_frames > FILUR_CB_MAX_BUFFER_SIZE) {
      num_sub_frames = FILUR_CB_MAX_BUFFER_SIZE;
    }

    cb_sub_process(sub_block_out_buffer, num_sub_frames, cb);

    sub_block_out_buffer += num_sub_frames;
    num_frames_to_process -= num_sub_frames;
  }

  cb->silent = cb->envelope.silent;
}

void cb_note_on(cb_t* cb) {
  assert(NULL != cb);

  cb->osc0_delta = 4 * cb->frequency * cb->sample_time;
  cb->osc1_delta = cb_frequency_multiplier * cb->osc0_delta;

  ar_envelope_set_decay(cb->decay, &cb->envelope);
  ar_envelope_start(&cb->envelope);

  if (CB_SILENT_TRUE == cb->silent) {
    biquad_state_reset(&cb->biquad_state);
  }

  cb->silent = CB_SILENT_FALSE;
}

void cb_set_gain(double gain, cb_t* cb) {
  assert(NULL != cb);

  cb->gain = 2.0 * filur_limit(gain, 0.0, 1.0);
}

void cb_set_decay(double decay, cb_t* cb) {
  assert(NULL != cb);

  cb->decay = filur_max(cb_min_decay, decay);
}

void cb_set_frequency(double frequency, cb_t* cb) {
  assert(NULL != cb);

  cb->frequency = frequency;
}

static void cb_sub_process(double* out, int number_of_frames, cb_t* cb) {
  assert(number_of_frames <= FILUR_CB_MAX_BUFFER_SIZE);

  double env[FILUR_CB_MAX_BUFFER_SIZE];

  ar_envelope_process(env, number_of_frames, &cb->envelope);

  for (int n = 0; n < number_of_frames; ++n) {
    double sq0 = -cb_gain;
    double sq1 = sq0;
    cb->osc0 += cb->osc0_delta;
    cb->osc1 += cb->osc1_delta;

    if (cb->osc0 > 1.0f) {
      cb->osc0 -= 2.0f;
    }

    if (cb->osc1 > 1.0f) {
      cb->osc1 -= 2.0f;
    }

    if (cb->osc0 > 0.0f) {
      sq0 = cb_gain;
    }

    if (cb->osc1 > 0.0f) {
      sq1 = cb_gain;
    }

    out[n] = sq0 + sq1;
  }

  for (int n = 0; n < number_of_frames; ++n) {
    double sample = cb->gain * out[n] * env[n];
    out[n] = biquad_process_sample(sample, &cb->biquad_state, &cb->biquad_coeffs);
  }
}
