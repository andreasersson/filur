/*
 * Copyright 2018-2021 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filur/biquad.h>

#include <assert.h>
#include <math.h>
#include <stddef.h>

#ifndef M_PI
  #define M_PI 3.14159265358979323846264338327950288
#endif /* M_PI */

static inline void biquad_set_coefficients(double a0,
                                           double a1,
                                           double a2,
                                           double b0,
                                           double b1,
                                           double b2,
                                           biquad_coeffs_t* biquad_coeffs);

void biquad_coeffs_init(double sample_rate, biquad_coeffs_t* biquad_coeffs) {
  assert(NULL != biquad_coeffs);

  biquad_coeffs->two_pi_ts = 2.0 * M_PI / sample_rate;
  biquad_coeffs->a0 = 0;
  biquad_coeffs->a1 = 0;
  biquad_coeffs->a2 = 0;
  biquad_coeffs->b0 = 0;
  biquad_coeffs->b1 = 0;
  biquad_coeffs->b2 = 0;
}

void biquad_coeffs_low_pass(double frequency, double q, biquad_coeffs_t* biquad_coeffs) {
  assert(NULL != biquad_coeffs);

  double w0 = frequency * biquad_coeffs->two_pi_ts;
  double alpha = sin(w0) / (2.0 * q);

  double a0 = 1.0 + alpha;
  double a1 = -2.0 * cos(w0);
  double a2 = 1.0 - alpha;

  double b1 = 1.0 + a1 * 0.5;
  double b0 = b1 * 0.5;
  double b2 = b0;

  biquad_set_coefficients(a0, a1, a2, b0, b1, b2, biquad_coeffs);
}

void biquad_coeffs_high_pass(double frequency,
                             double q,
                             double gain,
                             biquad_coeffs_t* biquad_coeffs) {
  assert(NULL != biquad_coeffs);

  double w0 = frequency * biquad_coeffs->two_pi_ts;
  double alpha = sin(w0) / (2.0 * q);

  double a0 = 1.0 + alpha;
  double a1 = -2.0 * cos(w0);
  double a2 = 1.0 - alpha;

  double b1 = -(1.0 - a1 * 0.5);
  double b0 = -b1 * 0.5;
  double b2 = b0;

  b0 *= gain;
  b1 *= gain;
  b2 *= gain;

  biquad_set_coefficients(a0, a1, a2, b0, b1, b2, biquad_coeffs);
}

void biquad_coeffs_band_pass(double frequency, double q, biquad_coeffs_t* biquad_coeffs) {
  assert(NULL != biquad_coeffs);

  double w0 = frequency * biquad_coeffs->two_pi_ts;
  double alpha = sin(w0) / (2.0 * q);

  double a0 = 1.0 + alpha;
  double a1 = -2.0 * cos(w0);
  double a2 = 1.0 - alpha;

  double b0 = q * alpha;
  double b1 = 0.0;
  double b2 = -b0;

  biquad_set_coefficients(a0, a1, a2, b0, b1, b2, biquad_coeffs);
}

void biquad_coeffs_low_shelf(double frequency, double q, double gain, biquad_coeffs_t* biquad_coeffs) {
  assert(NULL != biquad_coeffs);

  double a = pow(10.0, gain / 40.0);
  const double sqrta = sqrt(a);
  const double w0 = frequency * biquad_coeffs->two_pi_ts;
  const double cosw0 = cos(w0);
  const double alpha = sin(w0) / (2.0 * q);

  const double a0 = (a + 1.0) + (a - 1.0) * cosw0 + 2.0 * sqrta * alpha;
  const double a1 = -2.0 * ((a - 1.0) + (a + 1.0) * cosw0);
  const double a2 = (a + 1.0) + (a - 1.0) * cosw0 - 2.0 * sqrta * alpha;

  const double b0 = a * ((a + 1.0) - (a - 1.0) * cosw0 + 2.0 * sqrta * alpha);
  const double b1 = 2.0 * a * ((a - 1.0) - (a + 1.0) * cosw0);
  const double b2 = a * ((a + 1.0) - (a - 1.0) * cosw0 - 2.0 * sqrta * alpha);

  biquad_set_coefficients(a0, a1, a2, b0, b1, b2, biquad_coeffs);
}

void biquad_coeffs_high_shelf(double frequency, double q, double gain, biquad_coeffs_t* biquad_coeffs) {
  assert(NULL != biquad_coeffs);

  const double a = pow(10.0, gain / 40.0);
  const double sqrta = sqrt(a);
  const double w0 = frequency * biquad_coeffs->two_pi_ts;
  const double cosw0 = cos(w0);
  const double alpha = sin(w0) / (2.0 * q);

  const double a0 = (a + 1.0) - (a - 1.0) * cosw0 + 2.0 * sqrta * alpha;
  const double a1 = 2.0 * ((a - 1.0) - (a + 1.0) * cosw0);
  const double a2 = (a + 1.0) - (a - 1.0) * cosw0 - 2.0 * sqrta * alpha;

  const double b0 = a * ((a + 1.0) + (a - 1.0) * cosw0 + 2.0 * sqrta * alpha);
  const double b1 = -2.0 * a * ((a - 1.0) + (a + 1.0) * cosw0);
  const double b2 = a * ((a + 1.0) + (a - 1.0) * cosw0 - 2.0 * sqrta * alpha);

  biquad_set_coefficients(a0, a1, a2, b0, b1, b2, biquad_coeffs);
}

void biquad_state_init(biquad_state_t* biquad_state) {
  assert(NULL != biquad_state);

  biquad_state->denormal = 1e-20;

  biquad_state_reset(biquad_state);
}

void biquad_state_reset(biquad_state_t* biquad_state) {
  assert(NULL != biquad_state);

  biquad_state->x1 = 0;
  biquad_state->x2 = 0;
  biquad_state->y1 = 0;
  biquad_state->y2 = 0;
}

void biquad_process(const double* src,
                    double* dst,
                    int number_of_frames,
                    biquad_state_t* biquad_state,
                    const biquad_coeffs_t* biquad_coeffs) {
  assert(NULL != src);
  assert(NULL != dst);
  assert(NULL != biquad_state);
  assert(NULL != biquad_coeffs);

  for (int n = 0; n < number_of_frames; ++n) {
    dst[n] = biquad_process_sample(src[n], biquad_state, biquad_coeffs);
  }

  biquad_state->denormal = -biquad_state->denormal;
}

static inline void biquad_set_coefficients(double a0,
                                           double a1,
                                           double a2,
                                           double b0,
                                           double b1,
                                           double b2,
                                           biquad_coeffs_t* biquad_coeffs) {
  assert(NULL != biquad_coeffs);

  const double c = 1.0 / a0;
  biquad_coeffs->b0 = b0 * c;
  biquad_coeffs->b1 = b1 * c;
  biquad_coeffs->b2 = b2 * c;

  /* biquad_coeffs->a0 = a0 * c = 1 */
  biquad_coeffs->a0 = 1.0;
  biquad_coeffs->a1 = -a1 * c;
  biquad_coeffs->a2 = -a2 * c;
}
