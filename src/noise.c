/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filur/noise.h>

#include <stddef.h>
#include <assert.h>

static const double noise_gain = 0.0000000004656612873077392578125;
static const int noise_constant = 16807;
static const int noise_seed = 4711;

void noise_init(noise_t* noise) {
  assert(noise != NULL);

  noise->noise = noise_seed;
}

void noise_process(double* out, int number_of_frames, noise_t* noise) {
  assert(noise != NULL);

  for (int n = 0; n < number_of_frames; ++n) {
    noise->noise = noise_constant * noise->noise;
    out[n] = noise_gain * noise->noise;
  }
}
