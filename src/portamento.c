/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filur/portamento.h>

#include <filur/utils.h>

#include <assert.h>
#include <math.h>
#include <stddef.h>

static double portamento_min_time = 1e-3;
static double portamento_max_time = 10;

void portamento_init(double sample_rate, portamento_t* portamento) {
  assert(NULL != portamento);

  portamento->sample_rate = sample_rate;
  portamento->target_pitch = 3600;
  portamento->current_pitch = portamento->target_pitch;
  portamento->pitch = portamento->target_pitch;
  portamento->delta_pitch = 0;
  portamento->time = portamento_min_time;
}

void portamento_set_time(double time, portamento_t* portamento) {
  assert(NULL != portamento);

  portamento->time = filur_limit(time, portamento_min_time, portamento_max_time);
  double pitch_diff = portamento->target_pitch - portamento->current_pitch;
  portamento->delta_pitch = pitch_diff / (portamento->sample_rate * portamento->time);
}

void portamento_set_pitch(int pitch, portamento_t* portamento) {
  assert(NULL != portamento);

  portamento->target_pitch = pitch;
  portamento->current_pitch = pitch;
  portamento->pitch = portamento->current_pitch;
  portamento_set_time(portamento->time, portamento);
}

void portamento_set_target_pitch(int target_pitch, portamento_t* portamento) {
  assert(NULL != portamento);

  portamento->target_pitch = target_pitch;
  portamento_set_time(portamento->time, portamento);
}

void portamento_process(portamento_t* portamento) {
  assert(NULL != portamento);

  if (portamento->current_pitch != portamento->target_pitch) {
    portamento->pitch += portamento->delta_pitch;
    portamento->current_pitch = (int) (portamento->pitch);
  }

  if (((portamento->delta_pitch > 0) && (portamento->pitch >= portamento->target_pitch)) ||
      ((portamento->delta_pitch < 0) && (portamento->pitch <= portamento->target_pitch)) ||
      (portamento->delta_pitch == 0)) {
    portamento->pitch = portamento->target_pitch;
    portamento->current_pitch = portamento->target_pitch;
  }
}
