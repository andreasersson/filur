/*
 * Copyright 2018-2024 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filur/ep/ep.h>

#include <assert.h>
#include <stddef.h>

static const double volume_zipper_noise_threshold = 0.05; /* dB */
static const double volume_zipper_noise_factor = 0.005; /* dB */

static void process_volume(const double* in,
                           double* out,
                           int number_of_frames,
                           znf_log_value_t* volume);

static inline voice_t* ep_get_voice(const ep_parameters_t* parameters, ep_t* ep);

void ep_init(double sample_rate, ep_t* ep) {
  assert(NULL != ep);

  ep->sustain_pedal = pedal_off;
  ep->sostenuto_pedal = pedal_off;
  ep->soft_pedal = pedal_off;
  znf_log_value_init(volume_zipper_noise_threshold, volume_zipper_noise_factor, 0.5, &ep->volume);

  for (size_t voice_index = 0; voice_index < ep_num_voices; ++voice_index) {
    voice_init(sample_rate, &ep->voices[voice_index]);
  }
}

void ep_note_on(int pitch, double velocity, const ep_parameters_t* parameters, ep_t* ep) {
  assert(NULL != parameters);
  assert(NULL != ep);
  assert(NULL != parameters->tuning);

  double scaled_velocity = velocity;
  if (pedal_on == ep->soft_pedal) {
    scaled_velocity *= parameters->soft_pedal_damping;
  }
  voice_note_on(pitch, scaled_velocity, &parameters->tuning[pitch], parameters, ep_get_voice(parameters, ep));
}

void ep_note_off(int pitch, const ep_parameters_t* parameters, ep_t* ep) {
  assert(NULL != parameters);
  assert(NULL != ep);

  for (size_t voice_index = 0; voice_index < ep_num_voices; ++voice_index) {
    if (pitch == ep->voices[voice_index].pitch) {
      if (VOICE_STATE_ACTIVE == ep->voices[voice_index].state) {
        if (pedal_off == ep->sustain_pedal) {
          voice_note_off(parameters, &ep->voices[voice_index]);
        } else {
          voice_sustain(&ep->voices[voice_index]);
        }
      }
    } else if (VOICE_STATE_ACTIVE_SOSTENUTO == ep->voices[voice_index].state) {
      if (pedal_off == ep->sostenuto_pedal) {
          voice_note_off(parameters, &ep->voices[voice_index]);
        } else {
          ep->voices[voice_index].state = VOICE_STATE_SOSTENUTO;
        }
    }
  }
}

void ep_all_notes_off(const ep_parameters_t* parameters, ep_t* ep) {
  assert(NULL != parameters);
  assert(NULL != ep);

  for (size_t voice_index = 0; voice_index < ep_num_voices; ++voice_index) {
    if ((VOICE_STATE_ACTIVE == ep->voices[voice_index].state)
        || (VOICE_STATE_SUSTAIN == ep->voices[voice_index].state)
        || (VOICE_STATE_ACTIVE_SOSTENUTO == ep->voices[voice_index].state)
        || (VOICE_STATE_SOSTENUTO == ep->voices[voice_index].state)) {
        voice_note_off(parameters, &ep->voices[voice_index]);
    }
  }
}

void ep_sustain_pedal(pedal_t sustain_pedal, const ep_parameters_t* parameters, ep_t* ep) {
  assert(NULL != parameters);
  assert(NULL != ep);

  if (sustain_pedal != ep->sustain_pedal) {
    ep->sustain_pedal = sustain_pedal;
    if (pedal_off == ep->sustain_pedal) {
      for (size_t voice_index = 0; voice_index < ep_num_voices; ++voice_index) {
        if ((VOICE_STATE_SUSTAIN == ep->voices[voice_index].state)) {
          voice_note_off(parameters, &ep->voices[voice_index]);
        }
      }
    }
  }
}

void ep_sostenuto_pedal(pedal_t sostenuto_pedal, const ep_parameters_t* parameters, ep_t* ep) {
  assert(NULL != parameters);
  assert(NULL != ep);

  if (sostenuto_pedal != ep->sostenuto_pedal) {
    ep->sostenuto_pedal = sostenuto_pedal;
    if (pedal_on == ep->sostenuto_pedal) {
      for (size_t voice_index = 0; voice_index < ep_num_voices; ++voice_index) {
        if ((VOICE_STATE_ACTIVE == ep->voices[voice_index].state)) {
          ep->voices[voice_index].state = VOICE_STATE_ACTIVE_SOSTENUTO;
        }
      }
    } else {
      for (size_t voice_index = 0; voice_index < ep_num_voices; ++voice_index) {
        if ((VOICE_STATE_SOSTENUTO == ep->voices[voice_index].state)) {
          voice_note_off(parameters, &ep->voices[voice_index]);
        } else if ((VOICE_STATE_ACTIVE_SOSTENUTO == ep->voices[voice_index].state)) {
          ep->voices[voice_index].state = VOICE_STATE_ACTIVE;
        }
      }
    }
  }
}

void ep_soft_pedal(pedal_t soft_pedal, const ep_parameters_t* parameters, ep_t* ep) {
  assert(NULL != parameters);
  assert(NULL != ep);

  (void)parameters;

  if (soft_pedal != ep->soft_pedal) {
    ep->soft_pedal = soft_pedal;
  }
}

void ep_process(double *out,
                int number_of_frames,
                const ep_parameters_t* parameters,
                ep_t* ep) {
  assert(NULL != parameters);
  assert(NULL != ep);

  int num_frames_to_process = number_of_frames;
  double* sub_block_out_buffer = out;
  while (num_frames_to_process > 0) {
    int num_sub_frames = num_frames_to_process;
    if (num_sub_frames > FILUR_EP_MAX_BUFFER_SIZE) {
      num_sub_frames = FILUR_EP_MAX_BUFFER_SIZE;
    }

    for (size_t voice = 0; voice < ep_num_voices; ++voice) {
      if (VOICE_STATE_FREE != ep->voices[voice].state) {
        voice_process(sub_block_out_buffer, num_sub_frames, parameters, &ep->voices[voice]);
      } else {
        voice_update_parameters(parameters, &ep->voices[voice]);
      }
    }

    sub_block_out_buffer += num_sub_frames;
    num_frames_to_process -= num_sub_frames;
  }

  znf_log_value_set(parameters->volume, &ep->volume);
  process_volume(out, out, number_of_frames, &ep->volume);
}

size_t ep_num_active_voices(ep_t* ep) {
  assert(NULL != ep);

  size_t active_voices = 0;
  for (size_t index = 0; index < ep_num_voices; ++index) {
    if (VOICE_STATE_FREE != ep->voices[index].state) {
      ++active_voices;
    }
  }

  return active_voices;
}

static inline voice_t* ep_get_voice(const ep_parameters_t* parameters, ep_t* ep) {
  size_t voice_index = ep_num_voices;
  size_t num_free_voices = 0;

  /* find a free voice and calculate number of free voices */
  for (size_t index = 0; index < ep_num_voices; ++index) {
    if (VOICE_STATE_FREE == ep->voices[index].state) {
      voice_index = index;
      ++num_free_voices;
    }
  }

  if (num_free_voices <= ep_voice_headroom) {
    /* free oldest active voice */
    double silent = 1e3;
    size_t voice_to_free = ep_num_voices;
    for (size_t index = 0; index < ep_num_voices; ++index) {
      if ((VOICE_STATE_FREE != ep->voices[index].state) &&
          (VOICE_STATE_RELEASE != ep->voices[index].state)) {
        if (silent > voice_silent(&ep->voices[index])) {
          silent = voice_silent(&ep->voices[index]);
          voice_to_free = index;
        }
      }
    }

    if (voice_to_free != ep_num_voices) {
      voice_note_off(parameters, &ep->voices[voice_to_free]);
    }
  }

  /* if no free voice is available, find the quietest voice */
  if (voice_index == ep_num_voices) {
    double silent = 1e3;
    for (size_t index = 0; index < ep_num_voices; ++index) {
      if (silent > voice_silent(&ep->voices[index])) {
        silent = voice_silent(&ep->voices[index]);
        voice_index = index;
      }
    }
  }

  assert(voice_index != ep_num_voices);

  return &ep->voices[voice_index];
}

static void process_volume(const double* in,
                           double* out,
                           int number_of_frames,
                           znf_log_value_t* volume) {
  if (znf_log_value_equal(volume)) {
    for (int n = 0; n < number_of_frames; ++n) {
      out[n] = volume->value * in[n];
    }
  } else {
    for (int n = 0; n < number_of_frames; ++n) {
      out[n] = volume->value * in[n];
      if (!volume->equal) {
        znf_log_value_update(volume);
      }
    }
  }
}
