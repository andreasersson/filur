/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filur/ep/pickup.h>

#include <filur/biquad.h>
#include <filur/utils.h>

#include <assert.h>
#include <math.h>
#include <stddef.h>

#ifndef M_PI_2
  #define M_PI_2 1.57079632679489661923132169163975144 /* pi/2 */
#endif /* M_PI_2 */

static const double pickup_min_gain = 0.25;
static const double pickup_max_gain = 6.0 - 0.25;

static void pickup_update(pickup_parameters_t *parameters);

void pickup_init_parameters(pickup_parameters_t* parameters) {
  assert(NULL != parameters);

  pickup_set_timbre(0.4, parameters);
  pickup_set_volume(0.4, parameters);
}

void pickup_set_timbre(double timbre, pickup_parameters_t* parameters) {
  assert(NULL != parameters);

  double local_timbre = filur_limit(timbre, 0.0, 1.0);
  parameters->alpha = M_PI_2 * local_timbre;
  pickup_update(parameters);
}

void pickup_set_volume(double volume, pickup_parameters_t* parameters) {
  assert(NULL != parameters);

  double local_volume = filur_limit(volume, 0.0, 1.0);
  parameters->in_gain = pickup_min_gain + local_volume * pickup_max_gain;
  pickup_update(parameters);
}

void pickup_set(double timbre, double volume, pickup_parameters_t* parameters) {
  assert(NULL != parameters);

  double local_timbre = filur_limit(timbre, 0.0, 1.0);
  double local_volume = filur_limit(volume, 0.0, 1.0);
  parameters->in_gain = pickup_min_gain + local_volume * pickup_max_gain;
  parameters->alpha = M_PI_2 * local_timbre;
  pickup_update(parameters);
}

void pickup_init_state(double sample_rate, pickup_state_t* state) {
  assert(NULL != state);

  biquad_coeffs_init(sample_rate, &state->biquad_coeffs);
  biquad_state_init(&state->biquad_state);
}

void pickup_set_cutoff(double cutoff, pickup_state_t* state) {
  assert(NULL != state);

  biquad_state_reset(&state->biquad_state);
  biquad_coeffs_high_pass(cutoff, 0.70711, 1.0, &state->biquad_coeffs);
}

void pickup_process(const double* in,
                    double* out,
                    int number_of_frames,
                    const pickup_parameters_t* parameters,
                    pickup_state_t* state) {
  assert(NULL != in);
  assert(NULL != out);
  assert(NULL != state);
  assert(NULL != parameters);

  for (int n = 0; n < number_of_frames; ++n) {
    out[n] = parameters->out_gain * (sin(parameters->in_gain * in[n] + parameters->alpha) - parameters->offset);
  }
  biquad_process(out, out, number_of_frames, &state->biquad_state, &state->biquad_coeffs);
}

static void pickup_update(pickup_parameters_t* parameters) {
  parameters->offset = sin(parameters->alpha);

  double alpha = parameters->alpha;
  double gain = parameters->in_gain;
  double inv_gain = 1.0 / gain;
  double m1 = sin(gain * sin(M_PI_2) + alpha);
  double m2 = sin(gain * sin(-M_PI_2) + alpha);
  double m3 = sin(gain * sin(asin(filur_min(1.0, inv_gain * (M_PI_2 - alpha)))) + alpha);
  double m4 = sin(gain * sin(asin(filur_max(-1.0, inv_gain * (-M_PI_2 - alpha)))) + alpha);
  double max_value = filur_max(filur_max(m1, m2), filur_max(m3, m4));
  double min_value = filur_min(filur_min(m1, m2), filur_min(m3, m4));
  double peak = (max_value - min_value) * 0.5;
  if (0.0 == peak) {
    peak = 1.0;
  }

  parameters->out_gain = 1.0 / peak;
}
