/*
 * Copyright 2018-2024 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filur/ep/voice.h>

#include <assert.h>
#include <math.h>
#include <stddef.h>
#include <string.h>

static const double s_clang_frequency_1_multiplier = 6.0;
static const double s_clang_frequency_2_multiplier = 18.0;

static const double s_clang_attenuation_1 = 0.125;
static const double s_clang_attenuation_2 = 0.125 * 0.5;

static const double velocity_pos_curve = 3.0;
static const double velocity_neg_curve = 2.0;

static double note_nr_to_frequency_table[256];

static double velocity_curve(double velocity, double velocity_depth, double velocity_curve);
static void note_nr_to_frequency_init(void);
static double note_nr_to_frequency(int note_nr);
static inline double clang_attenuation(double frequency);

void voice_init(double sample_rate, voice_t* voice) {
  assert(0 < sample_rate);
  assert(NULL != voice);

  voice->state = VOICE_STATE_FREE;
  voice->pitch = -1;
  voice->gain = 1.0;

  note_nr_to_frequency_init();

  sine_osc_init(sample_rate, &voice->tonebar);
  sine_osc_init(sample_rate, &voice->clang1);
  sine_osc_init(sample_rate, &voice->clang2);

  adsr_init_state(&voice->tonebar_adsr);
  adsr_init_state(&voice->clang_adsr);

  adsr_init_parameters(sample_rate, &voice->tonebar_adsr_parameters);
  adsr_set_attack(1e-3, &voice->tonebar_adsr_parameters);
  adsr_set_decay(10.0, &voice->tonebar_adsr_parameters);
  adsr_set_sustain(0.0, &voice->tonebar_adsr_parameters);
  adsr_set_release(80e-3, &voice->tonebar_adsr_parameters);

  adsr_init_parameters(sample_rate, &voice->clang_adsr_parameters);
  adsr_set_attack(1e-3, &voice->clang_adsr_parameters);
  adsr_set_decay(200e-3, &voice->clang_adsr_parameters);
  adsr_set_sustain(0.0, &voice->clang_adsr_parameters);
  adsr_set_release(50e-3, &voice->clang_adsr_parameters);

  pickup_init_state(sample_rate, &voice->pickup);
  pickup_init_parameters(&voice->pickup_parameters);
}

void voice_note_on(int pitch,
                   double velocity,
                   const key_tuning_t* tuning,
                   const ep_parameters_t* parameters,
                   voice_t* voice) {
  assert(NULL != tuning);
  assert(NULL != parameters);
  assert(NULL != voice);

  voice->pitch = pitch;

  double tonebar_velocity = velocity_curve(velocity, parameters->tonebar_velocity_depth, parameters->tonebar_velocity_curve);
  double clang_velocity = velocity_curve(velocity, parameters->clang_velocity_depth, parameters->clang_velocity_curve);
  adsr_gate_on(&voice->tonebar_adsr);
  adsr_gate_on(&voice->clang_adsr);

  double frequency = note_nr_to_frequency(voice->pitch);
  pickup_set_cutoff(frequency, &voice->pickup);

  /* set tonebar attack time to a half period */
  double attack = 0.5 / frequency;
  adsr_set_attack(attack, &voice->tonebar_adsr_parameters);
  adsr_set_decay(parameters->tonebar_decay, &voice->tonebar_adsr_parameters);
  adsr_set_release(parameters->tonebar_release, &voice->tonebar_adsr_parameters);

  double clang_decay = parameters->clang_decay;
  adsr_set_attack(0.0, &voice->clang_adsr_parameters);
  adsr_set_decay(clang_decay, &voice->clang_adsr_parameters);
  double clang_release = clang_decay;
  if (clang_release > parameters->tonebar_release) {
    clang_release = parameters->tonebar_release;
  }
  adsr_set_release(clang_release, &voice->clang_adsr_parameters);

  /* tonebar */
  sine_osc_set_frequency(frequency, &voice->tonebar);
  sine_osc_set_amplitude(tonebar_velocity, &voice->tonebar);
  sine_osc_set_phase(0, &voice->tonebar);

  /* clang */
  double clang_amplitude = clang_velocity * parameters->clang_volume * tuning->clang_volume;
  double clang_frequency_1 = s_clang_frequency_1_multiplier * frequency;
  double clang_amplitude_1 = clang_amplitude * s_clang_attenuation_1 * clang_attenuation(clang_frequency_1);
  sine_osc_set_frequency(clang_frequency_1, &voice->clang1);
  sine_osc_set_amplitude(clang_amplitude_1, &voice->clang1);
  sine_osc_set_phase(0, &voice->clang1);

  double clang_frequency_2 = s_clang_frequency_2_multiplier * frequency;
  double clang_amplitude_2 = clang_amplitude * s_clang_attenuation_2 * clang_attenuation(clang_frequency_2);

  sine_osc_set_frequency(clang_frequency_2, &voice->clang2);
  sine_osc_set_amplitude(clang_amplitude_2, &voice->clang2);
  sine_osc_set_phase(0, &voice->clang2);

  pickup_set(parameters->pickup_timbre * tuning->pickup_timbre, parameters->pickup_volume * tuning->pickup_volume, &voice->pickup_parameters);

  voice->gain = tuning->gain;
  voice->state = VOICE_STATE_ACTIVE;
}

void voice_note_off(const ep_parameters_t* parameters, voice_t* voice) {
  (void) parameters;

  assert(NULL != parameters);
  assert(NULL != voice);

  voice->state = VOICE_STATE_RELEASE;
  adsr_gate_off(&voice->tonebar_adsr);
  adsr_gate_off(&voice->clang_adsr);
}

void voice_update_parameters(const ep_parameters_t* parameters, voice_t* voice) {
  (void) parameters;
  (void) voice;

  assert(NULL != voice);
  assert(NULL != parameters);
}

void voice_sustain(voice_t* voice) {
  assert(NULL != voice);

  voice->state = VOICE_STATE_SUSTAIN;
}

void voice_process(double *out,
                   int number_of_frames,
                   const ep_parameters_t* parameters,
                   voice_t* voice) {
  (void) parameters;
  assert(NULL != voice);
  assert(number_of_frames <= FILUR_EP_MAX_BUFFER_SIZE);

  double voice_buffer[FILUR_EP_MAX_BUFFER_SIZE];
  double env[FILUR_EP_MAX_BUFFER_SIZE];

  {
    adsr_process(env, number_of_frames, &voice->tonebar_adsr, &voice->tonebar_adsr_parameters);
    sine_osc_process(voice_buffer, number_of_frames, &voice->tonebar);

    for (int n = 0; n < number_of_frames; ++n) {
      voice_buffer[n] = voice_buffer[n] * env[n];
    }
  }

  pickup_process(voice_buffer, voice_buffer, number_of_frames, &voice->pickup_parameters, &voice->pickup);

  if (0 == voice->clang_adsr.silent) {
    double buffer[FILUR_EP_MAX_BUFFER_SIZE];
    adsr_process(env, number_of_frames, &voice->clang_adsr, &voice->clang_adsr_parameters);
    sine_osc_process(buffer, number_of_frames, &voice->clang1);
    sine_osc_process_add(buffer, number_of_frames, &voice->clang2);

    for (int n = 0; n < number_of_frames; ++n) {
      voice_buffer[n] += buffer[n] * env[n];
    }
  }

  for (int n = 0; n < number_of_frames; ++n) {
    out[n] += voice_buffer[n] * voice->gain;
  }

  if (voice->tonebar_adsr.silent) {
    voice->state = VOICE_STATE_FREE;
    voice->pitch = -1;
  }
}

double voice_silent(voice_t* voice) {
  assert(NULL != voice);

  return voice->tonebar_adsr.value;
}

static double velocity_curve(double velocity, double velocity_depth, double velocity_curve) {

  if (velocity_depth < 0.0) {
    velocity_depth = 0.0;
  } else if (velocity_depth > 1.0) {
    velocity_depth = 1.0;
  }
  double local_velocity = (1.0 - velocity_depth) + velocity * velocity_depth;
  if (local_velocity < 0.0) {
    local_velocity = 0.0;
  } else if (local_velocity > 1.0) {
    local_velocity = 1.0;
  }

  double curve = 1.0;
  if (velocity_curve >= 0.0) {
    curve = 1.0 + velocity_pos_curve * velocity_curve;
  } else {
    curve = 1.0 / (1.0 + velocity_neg_curve * fabs(velocity_curve));
  }
  double pow_velocity = pow(local_velocity, curve);

  return pow_velocity;
}

static void note_nr_to_frequency_init(void) {
  double base_frequency = 8.1757989156437;
  double k = pow(2.0, 1.0 / 12.0);
  double frequency = base_frequency;

  for (unsigned int n = 0; n < sizeof(note_nr_to_frequency_table) / sizeof(double); ++n) {
    note_nr_to_frequency_table[n] = frequency;
    frequency *= k;
  }
}

static double note_nr_to_frequency(int note_nr) {
  const int size = sizeof(note_nr_to_frequency_table) / sizeof(double);
  if (note_nr >= size) {
    note_nr = size - 1;
  }

  return note_nr_to_frequency_table[note_nr];
}

static inline double clang_attenuation(double frequency) {
  double attenuation = 1.0;
  if (frequency > 20000.0) {
    attenuation = 0.0;
  }

  return attenuation;
}
