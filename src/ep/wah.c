/*
 * Copyright 2018-2019 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filur/ep/wah.h>

#include <assert.h>
#include <math.h>
#include <stddef.h>
#include <string.h>

#define FILUR_WAH_MAX_BUFFER_SIZE 1024

static const double wah_default_low_cutoff = 290.0;
static const double wah_default_high_cutoff = 1.5e3;
static const double wah_default_resonance = 2.0;
static const double wah_default_speed = 2.0;

static void wah_sub_process(const double* in,
                            double* out,
                            int number_of_frames,
                            wah_parameters_t* parameters,
                            wah_t* wah);

static void update_lfo_speed(double tempo,
                             tempo_sync_t tempo_sync,
                             double speed,
                             lfo_parameters_t* lfo_parameters);

void wah_parameters_init(double sample_rate, wah_parameters_t* parameters) {
  assert(0 < sample_rate);
  assert(NULL != parameters);

  parameters->enabled = WAH_OFF;
  parameters->lfo_enabled = WAH_LFO_OFF;

  parameters->low_cutoff = wah_default_low_cutoff;
  parameters->high_cutoff = wah_default_high_cutoff;

  parameters->cutoff = 0.5;
  parameters->resonance = wah_default_resonance;
  parameters->speed = wah_default_speed;
  parameters->phase = 0.0;
  parameters->tempo_sync = TEMPO_SYNC_FREE_RUNNING;
  parameters->tempo = 120.0;

  lfo_init_parameters(sample_rate, &parameters->lfo);
}

void wah_set_enabled(wah_enable_t enabled, wah_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->enabled = enabled;
}

void wah_set_low_cutoff(double low_cutoff, wah_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->low_cutoff = low_cutoff;
}

void wah_set_high_cutoff(double high_cutoff, wah_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->high_cutoff = high_cutoff;
}

void wah_set_cutoff(double cutoff, wah_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->cutoff = cutoff;
}

void wah_set_resonance(double resonance, wah_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->resonance = resonance;
}

void wah_set_lfo_enabled(wah_lfo_enable_t enabled, wah_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->lfo_enabled = enabled;
}

void wah_set_speed(double speed, wah_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->speed = speed;
  update_lfo_speed(parameters->tempo, parameters->tempo_sync, parameters->speed, &parameters->lfo);
}

void wah_set_phase(double phase, wah_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->phase = phase;
}

void wah_set_tempo_sync(tempo_sync_t tempo_sync, wah_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->tempo_sync = tempo_sync;
  update_lfo_speed(parameters->tempo, parameters->tempo_sync, parameters->speed, &parameters->lfo);
}

void wah_set_tempo(double tempo, wah_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->tempo = tempo;
  update_lfo_speed(parameters->tempo, parameters->tempo_sync, parameters->speed, &parameters->lfo);
}

void wah_init(double sample_rate, wah_t* wah) {
  assert(0 < sample_rate);
  assert(NULL != wah);

  wah->current_cutoff = 0;

  biquad_state_init(&wah->biquad_state);
  znf_value_init(0.01, 0.005, 1.0, &wah->cutoff);
  znf_value_init(0.01, 0.005, 1.0, &wah->resonance);
  lfo_init_state(&wah->lfo);

  biquad_coeffs_init(sample_rate, &wah->biquad_coeffs);
}

void wah_update_parameters(const wah_parameters_t* parameters, wah_t* wah) {
  assert(NULL != parameters);
  assert(NULL != wah);

  if (parameters->cutoff != wah->cutoff.value) {
    znf_value_set(parameters->cutoff, &wah->cutoff);
    znf_value_reset(&wah->cutoff);
  }

  if (parameters->resonance != wah->resonance.value) {
    znf_value_set(parameters->resonance, &wah->resonance);
    znf_value_reset(&wah->resonance);
  }
}

void wah_restart_lfo(const wah_parameters_t* parameters, wah_t* wah) {
  assert(NULL != parameters);
  assert(NULL != wah);

  lfo_set_phase(parameters->phase, &wah->lfo);
}

void wah_process(const double* in,
                 double* out,
                 int number_of_frames,
                 wah_parameters_t* parameters,
                 wah_t* wah) {
  assert(NULL != in);
  assert(NULL != out);
  assert(NULL != parameters);
  assert(NULL != wah);

  if (WAH_ON == parameters->enabled) {
    int num_frames_to_process = number_of_frames;
    const double* sub_block_in_buffer = in;
    double* sub_block_out_buffer = out;
    while (num_frames_to_process > 0) {
      int num_sub_frames = num_frames_to_process;
      if (num_sub_frames > FILUR_WAH_MAX_BUFFER_SIZE) {
        num_sub_frames = FILUR_WAH_MAX_BUFFER_SIZE;
      }

      wah_sub_process(sub_block_in_buffer, sub_block_out_buffer, num_sub_frames, parameters, wah);

      sub_block_in_buffer += num_sub_frames;
      sub_block_out_buffer += num_sub_frames;
      num_frames_to_process -= num_sub_frames;
    }
  } else {
    if (out != in) {
      memcpy(out, in, number_of_frames * sizeof(double));
    }
    wah_update_parameters(parameters, wah);
  }
}

static void wah_sub_process(const double* in,
                            double* out,
                            int number_of_frames,
                            wah_parameters_t* parameters,
                            wah_t* wah) {
  assert(number_of_frames <= FILUR_WAH_MAX_BUFFER_SIZE);

  double cutoff[FILUR_WAH_MAX_BUFFER_SIZE];
  bool update_biquad = false;

  znf_value_set(parameters->resonance, &wah->resonance);
  znf_value_set(parameters->cutoff, &wah->cutoff);
  if (WAH_LFO_ON == parameters->lfo_enabled) {
    znf_value_reset(&wah->cutoff);
  }

  if (WAH_LFO_ON == parameters->lfo_enabled) {
    lfo_process_triangle_sqr(cutoff, number_of_frames, &wah->lfo, &parameters->lfo);
  } else {
    for (int n = 0; n < number_of_frames; ++n) {
      znf_value_update(&wah->cutoff);
      cutoff[n] = wah->cutoff.value;
    }
  }

  for (int n = 0; n < number_of_frames; ++n) {
    if (!wah->resonance.equal) {
      znf_value_update(&wah->resonance);
      update_biquad = true;
    }

    double new_cutoff = parameters->low_cutoff + cutoff[n] * (parameters->high_cutoff - parameters->low_cutoff);
    if (fabs(new_cutoff - wah->current_cutoff) > 1) {
      update_biquad = true;
      wah->current_cutoff = new_cutoff;
    }

    if (update_biquad) {
      update_biquad = false;
      biquad_coeffs_band_pass(wah->current_cutoff, wah->resonance.value, &wah->biquad_coeffs);
    }

    out[n] = biquad_process_sample(in[n], &wah->biquad_state, &wah->biquad_coeffs);
  }
}

static void update_lfo_speed(double tempo,
                             tempo_sync_t tempo_sync,
                             double speed,
                             lfo_parameters_t* lfo_parameters) {
  double local_frequency = speed;
  if (TEMPO_SYNC_FREE_RUNNING != tempo_sync) {
    local_frequency = lfo_tempo_to_frequency(tempo, tempo_sync_to_rate(tempo_sync));
  }

  lfo_set_frequency(local_frequency, lfo_parameters);
}

