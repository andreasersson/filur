/*
 * Copyright 2021 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filur/ep/eq.h>

#include "filur/biquad.h"

#include <assert.h>
#include <stddef.h>
#include <string.h>

static const double eq_low_shelf_default_cutoff = 100.0;
static const double eq_low_shelf_default_resonance = 0.707;
static const double eq_low_shelf_default_gain = 0.0;

static const double eq_high_shelf_default_cutoff = 2000.0;
static const double eq_high_shelf_default_resonance = 0.707;
static const double eq_high_shelf_default_gain = 0.0;

void eq_parameters_init(eq_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->low_shelf.cutoff = eq_low_shelf_default_cutoff;
  parameters->low_shelf.resonance = eq_low_shelf_default_resonance;
  parameters->low_shelf.gain = eq_low_shelf_default_gain;
  parameters->low_shelf.changed = true;

  parameters->high_shelf.cutoff = eq_high_shelf_default_cutoff;
  parameters->high_shelf.resonance = eq_high_shelf_default_resonance;
  parameters->high_shelf.gain = eq_high_shelf_default_gain;
  parameters->high_shelf.changed = true;
}

void eq_low_shelf_set_cutoff(double cutoff, eq_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->low_shelf.cutoff = cutoff;
  parameters->low_shelf.changed = true;
}

void eq_low_shelf_set_resonance(double resonance, eq_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->low_shelf.resonance = resonance;
  parameters->low_shelf.changed = true;
}

void eq_low_shelf_set_gain(double gain, eq_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->low_shelf.gain = gain;
  parameters->low_shelf.changed = true;
}

void eq_high_shelf_set_cutoff(double cutoff, eq_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->high_shelf.cutoff = cutoff;
  parameters->high_shelf.changed = true;
}

void eq_high_shelf_set_resonance(double resonance, eq_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->high_shelf.resonance = resonance;
  parameters->high_shelf.changed = true;
}

void eq_high_shelf_set_gain(double gain, eq_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->high_shelf.gain = gain;
  parameters->high_shelf.changed = true;
}

void eq_init(double sample_rate, eq_t* eq) {
  assert(0 < sample_rate);
  assert(NULL != eq);

  biquad_state_init(&eq->low_shelf_state);
  biquad_state_init(&eq->high_shelf_state);
  biquad_coeffs_init(sample_rate, &eq->low_shelf_coeffs);
  biquad_coeffs_init(sample_rate, &eq->high_shelf_coeffs);
}

void eq_process(const double* in,
                double* out,
                int number_of_frames,
                eq_parameters_t* parameters,
                eq_t* eq) {
  assert(NULL != in);
  assert(NULL != out);
  assert(NULL != parameters);
  assert(NULL != eq);

  if (true == parameters->low_shelf.changed) {
    parameters->low_shelf.changed = false;
    biquad_coeffs_low_shelf(parameters->low_shelf.cutoff,
                            parameters->low_shelf.resonance,
                            parameters->low_shelf.gain,
                            &eq->low_shelf_coeffs);
  }

  if (true == parameters->high_shelf.changed) {
    parameters->high_shelf.changed = false;
    biquad_coeffs_high_shelf(parameters->high_shelf.cutoff,
                             parameters->high_shelf.resonance,
                             parameters->high_shelf.gain,
                             &eq->high_shelf_coeffs);
  }

  if (in != out) {
    memmove(out, in, number_of_frames * sizeof(double));
  }

  if (parameters->low_shelf.gain != 0.0) {
    biquad_process(out, out, number_of_frames, &eq->low_shelf_state, &eq->low_shelf_coeffs);
  }

  if (parameters->high_shelf.gain != 0.0) {
    biquad_process(out, out, number_of_frames, &eq->high_shelf_state, &eq->high_shelf_coeffs);
  }
}
