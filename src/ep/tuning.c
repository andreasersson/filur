/*
 * Copyright 2024 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filur/ep/tuning.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void key_tuning_init(key_tuning_t* key_tuning) {
  key_tuning->pickup_timbre = 1.0;
  key_tuning->pickup_volume = 1.0;
  key_tuning->clang_volume = 1.0;
  key_tuning->gain = 1.0;
}

void tuning_init(tuning_t tuning) {
  for (unsigned int i = 0u; i < TUNING_MAX_NUMBER_OF_KEYS; ++i) {
    key_tuning_init(&tuning[i]);
  }
}

#ifdef __cplusplus
}
#endif /* __cplusplus */
