/*
 * Copyright 2018-2019 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filur/ep/tremolo.h>

#include <assert.h>
#include <stddef.h>
#include <string.h>

#define FILUR_TREMOLO_MAX_BUFFER_SIZE 1024

static const double tremolo_default_depth = 0.5;
static const double tremolo_default_speed = 4.0;

static void tremolo_sub_process(const double* in,
                                double* out,
                                int number_of_frames,
                                const tremolo_parameters_t* parameters,
                                tremolo_t* tremolo);

static void update_lfo_speed(double tempo,
                             tempo_sync_t tempo_sync,
                             double speed,
                             lfo_parameters_t* lfo_parameters);

void tremolo_parameters_init(double sample_rate, tremolo_parameters_t* parameters) {
  assert(sample_rate > 0);
  assert(NULL != parameters);

  parameters->enabled = TREMOLO_OFF;
  parameters->depth = tremolo_default_depth;
  parameters->speed = tremolo_default_speed;
  parameters->tempo_sync = TEMPO_SYNC_FREE_RUNNING;
  parameters->tempo = 120.0;

  lfo_init_parameters(sample_rate, &parameters->lfo);
}

void tremolo_set_enabled(tremolo_enable_t enabled, tremolo_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->enabled = enabled;
}

void tremolo_set_depth(double depth, tremolo_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->depth = depth;
}

void tremolo_set_speed(double speed, tremolo_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->speed = speed;
  update_lfo_speed(parameters->tempo,
                   parameters->tempo_sync,
                   parameters->speed,
                   &parameters->lfo);
}

void tremolo_set_tempo_sync(tempo_sync_t tempo_sync, tremolo_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->tempo_sync = tempo_sync;
  update_lfo_speed(parameters->tempo,
                   parameters->tempo_sync,
                   parameters->speed,
                   &parameters->lfo);
}

void tremolo_set_tempo(double tempo, tremolo_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->tempo = tempo;
  update_lfo_speed(parameters->tempo,
                   parameters->tempo_sync,
                   parameters->speed,
                   &parameters->lfo);
}

void tremolo_init(tremolo_t* tremolo) {
  assert(NULL != tremolo);

  lfo_init_state(&tremolo->lfo);
  znf_value_init(0.001, 0.0001, 0.0, &tremolo->depth);
}

void tremolo_update_parameters(const tremolo_parameters_t* parameters, tremolo_t* tremolo) {
  assert(NULL != parameters);
  assert(NULL != tremolo);

  if (parameters->depth != tremolo->depth.value) {
    znf_value_set(parameters->depth, &tremolo->depth);
    znf_value_reset(&tremolo->depth);
  }
}

void tremolo_restart_lfo(tremolo_t* tremolo) {
  assert(NULL != tremolo);

  lfo_init_state(&tremolo->lfo);
}

void tremolo_process(const double* in,
                     double* out,
                     int number_of_frames,
                     const tremolo_parameters_t* parameters,
                     tremolo_t* tremolo) {
  assert(NULL != in);
  assert(NULL != out);
  assert(NULL != parameters);
  assert(NULL != tremolo);

  if (TREMOLO_ON == parameters->enabled) {
    int num_frames_to_process = number_of_frames;
    const double* sub_block_in_buffer = in;
    double* sub_block_out_buffer = out;
    while (num_frames_to_process > 0) {
      int num_sub_frames = num_frames_to_process;
      if (num_sub_frames > FILUR_TREMOLO_MAX_BUFFER_SIZE) {
        num_sub_frames = FILUR_TREMOLO_MAX_BUFFER_SIZE;
      }

      tremolo_sub_process(sub_block_in_buffer, sub_block_out_buffer, num_sub_frames, parameters, tremolo);

      sub_block_in_buffer += num_sub_frames;
      sub_block_out_buffer += num_sub_frames;
      num_frames_to_process -= num_sub_frames;
    }
  } else {
    if (out != in) {
      memcpy(out, in, number_of_frames * sizeof(double));
    }
    tremolo_update_parameters(parameters, tremolo);
  }
}

static void tremolo_sub_process(const double* in,
                                double* out,
                                int number_of_frames,
                                const tremolo_parameters_t* parameters,
                                tremolo_t* tremolo) {
  assert(number_of_frames <= FILUR_TREMOLO_MAX_BUFFER_SIZE);

  double lfo[FILUR_TREMOLO_MAX_BUFFER_SIZE];

  znf_value_set(parameters->depth, &tremolo->depth);

  double lfo_depth_b = tremolo->depth.value;
  double lfo_depth_a = (1.0 - lfo_depth_b);

  lfo_process_triangle_normalized(lfo, number_of_frames,
                                  &tremolo->lfo,
                                  &parameters->lfo);

  for (int n = 0; n < number_of_frames; ++n) {
    out[n] = in[n] * (lfo_depth_a + lfo_depth_b * lfo[n]);

    if (!tremolo->depth.equal) {
      znf_value_update(&tremolo->depth);
      lfo_depth_b = tremolo->depth.value;
      lfo_depth_a = (1.0 - lfo_depth_b);
    }
  }
}

static void update_lfo_speed(double tempo,
                             tempo_sync_t tempo_sync,
                             double speed,
                             lfo_parameters_t* lfo_parameters) {
  double local_frequency = speed;
  if (tempo_sync > 0) {
    local_frequency = lfo_tempo_to_frequency(tempo, tempo_sync_to_rate(tempo_sync));
  }

  lfo_set_frequency(local_frequency, lfo_parameters);
}
