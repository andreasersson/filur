/*
 * Copyright 2018-2024 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filur/ep/ep_parameters.h>
#include <filur/ep/tuning.h>

#include <math.h>
#include <stddef.h>
#include <assert.h>

static const double ep_tonebar_default_velocity_curve = 0.75;
static const double ep_clang_default_velocity_curve = 0.0;
static const double ep_tonebar_default_decay = 10000e-3;
static const double ep_tonebar_min_release = 70e-3;
static const double ep_clang_default_decay = 400e-3;

static key_tuning_t default_tuning[TUNING_MAX_NUMBER_OF_KEYS];

static inline double from_db(double db_value);
static void init_default_tuning(key_tuning_t* tuning);

static inline double from_db(double db_value) {
  return pow(10.0, db_value / 20.0);
}

static void init_default_tuning(key_tuning_t* tuning) {
  assert(NULL != tuning);
  static const size_t key_threshold = 60;

  for (size_t key = 0; key < TUNING_MAX_NUMBER_OF_KEYS; ++key) {
    tuning[key].pickup_timbre = 1.0;
    tuning[key].pickup_volume = 1.0;
    tuning[key].clang_volume = 1.0;
    tuning[key].gain = 1.0;
    if (key > key_threshold) {
      const double max = 128 - key_threshold;
      double f = (key - key_threshold) / max;
      tuning[key].pickup_timbre = 1.0 - f;
      tuning[key].pickup_volume = 1.0 - f;

      tuning[key].pickup_timbre = tuning[key].pickup_timbre * tuning[key].pickup_timbre;
      tuning[key].pickup_volume = tuning[key].pickup_volume * tuning[key].pickup_volume;
    }
  }
}

void ep_parameters_init(ep_parameters_t* parameters) {
  assert(NULL != parameters);

  init_default_tuning(default_tuning);
  parameters->tuning = default_tuning;

  parameters->volume = from_db(-6.0);

  parameters->tonebar_velocity_curve = ep_tonebar_default_velocity_curve;
  parameters->tonebar_velocity_depth = 1.0;
  parameters->tonebar_decay = ep_tonebar_default_decay;
  parameters->tonebar_release = ep_tonebar_min_release;

  parameters->clang_volume = from_db(-24.0);
  parameters->clang_velocity_curve = ep_clang_default_velocity_curve;
  parameters->clang_velocity_depth = 1.0;
  parameters->clang_decay = ep_clang_default_decay;

  parameters->pickup_timbre = 0.4;
  parameters->pickup_volume = 0.4;

  parameters->soft_pedal_damping = from_db(-6.0);
}

void ep_set_volume(double volume, ep_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->volume = from_db(volume);
}

void ep_set_velocity_curve(double velocity_curve, ep_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->tonebar_velocity_curve = velocity_curve;
}

void ep_set_velocity_depth(double velocity_depth, ep_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->tonebar_velocity_depth = velocity_depth;
}

void ep_set_decay(double decay, ep_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->tonebar_decay = decay;
}

void ep_set_release(double release, ep_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->tonebar_release = release;
}

void ep_set_clang_volume(double volume, ep_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->clang_volume = from_db(volume);
}

void ep_set_clang_velocity_curve(double velocity_curve, ep_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->clang_velocity_curve = velocity_curve;
}

void ep_set_clang_velocity_depth(double velocity_depth, ep_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->clang_velocity_depth = velocity_depth;
}

void ep_set_clang_decay(double decay, ep_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->clang_decay = decay;
}

void ep_set_pickup_timbre(double timbre, ep_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->pickup_timbre = timbre;
}

void ep_set_pickup_volume(double volume, ep_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->pickup_volume = volume;
}

void ep_set_soft_pedal_damping(double soft_pedal_damping, ep_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->soft_pedal_damping = soft_pedal_damping;
  if (parameters->soft_pedal_damping > 1.0) {
    parameters->soft_pedal_damping = 1.0;
  } else if (parameters->soft_pedal_damping < 0.0) {
    parameters->soft_pedal_damping = 0.0;
  }
}

void ep_set_key_tuning(const key_tuning_t* tuning, ep_parameters_t* parameters) {
  assert(NULL != parameters);

  if (NULL != tuning) {
    parameters->tuning = tuning;
  } else {
    parameters->tuning = default_tuning;
  }
}
