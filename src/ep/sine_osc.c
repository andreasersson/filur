/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filur/ep/sine_osc.h>

#include <stddef.h>
#include <math.h>
#include <assert.h>

#ifndef M_PI
  #define M_PI 3.14159265358979323846264338327950288
#endif /* M_PI */

//#define SINE_OSC_APPROXIMATION 1

#ifdef SINE_OSC_APPROXIMATION
#include "sine_approximation.h"
#endif

#ifdef SINE_OSC_APPROXIMATION
#define SINE_OSC(x) sine_11(x)
#else
#define SINE_OSC(x) sin(x)
#endif /* SINE_OSC_APPROXIMATION */

static inline double sine_osc_process_sample(sine_osc_t* sine_osc) {
  double sample = sine_osc->amplitude * SINE_OSC(sine_osc->alpha);
  sine_osc->alpha += sine_osc->delta_alpha;
  if (sine_osc->alpha > M_PI) {
    sine_osc->alpha -= 2 * M_PI;
  }

  return sample;
}

void sine_osc_init(double sample_rate, sine_osc_t* sine_osc) {
  assert(NULL != sine_osc);
  assert(0 != sample_rate);

  sine_osc->sample_time = 1.0 / sample_rate;
  sine_osc->alpha = 0.0;
  sine_osc->delta_alpha = 0.0;
  sine_osc->amplitude = 0.0;
}

void sine_osc_set_amplitude(double amplitude, sine_osc_t* sine_osc) {
  sine_osc->amplitude = amplitude;
}

void sine_osc_set_frequency(double frequency, sine_osc_t* sine_osc) {
  sine_osc->delta_alpha = 2 * M_PI * sine_osc->sample_time * frequency;
}

void sine_osc_set_phase(double phase, sine_osc_t* sine_osc) {
  sine_osc->alpha = phase;
  while (sine_osc->alpha > M_PI) {
    sine_osc->alpha -= 2 * M_PI;
  }
}

void sine_osc_process(double* out, int number_of_frames, sine_osc_t* sine_osc) {
  for (int n = 0; n < number_of_frames; ++n) {
    out[n] = sine_osc_process_sample(sine_osc);
  }
}

void sine_osc_process_add(double* out, int number_of_frames, sine_osc_t* sine_osc) {
  for (int n = 0; n < number_of_frames; ++n) {
    out[n] += sine_osc_process_sample(sine_osc);
  }
}
