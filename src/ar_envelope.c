/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filur/ar_envelope.h>

#include <filur/utils.h>

#include <assert.h>
#include <math.h>
#include <stddef.h>
#include <string.h>

static const double ar_envelope_late_decay_threshold = 0.01;
static const double ar_envelope_silent_threshold = 1e-4;
static const double ar_envelope_min_attack = 1e-3;
static const double ar_envelope_max = 1.0;

void ar_envelope_init(double sample_rate, ar_envelope_t* envelope) {
  assert(NULL != envelope);

  envelope->sample_rate = sample_rate;
  envelope->state = AR_ENVELOPE_STATE_SILENT;
  envelope->silent = AR_SILENT_TRUE;
  envelope->denormal = 1e-10;
  envelope->value = 0;
  envelope->late_decay_threshold = ar_envelope_late_decay_threshold;

  ar_envelope_set_attack(ar_envelope_min_attack, envelope);
  ar_envelope_set_decay(0.05f, envelope);
}

void ar_envelope_process(double* out, int number_of_frames, ar_envelope_t* envelope) {
  assert(NULL != envelope);

  if ((AR_ENVELOPE_STATE_ATTACK != envelope->state) &&
      (envelope->value < ar_envelope_silent_threshold)) {
    envelope->silent = AR_SILENT_TRUE;
    envelope->state = AR_ENVELOPE_STATE_SILENT;
    envelope->value = 0;
  }

  if (AR_ENVELOPE_STATE_SILENT == envelope->state) {
    memset(out, 0, sizeof(double) * number_of_frames);

    return;
  }

  envelope->denormal = -envelope->denormal;

  for (int n = 0; n < number_of_frames; ++n) {
    switch (envelope->state) {
      case AR_ENVELOPE_STATE_ATTACK:
        envelope->value += envelope->attack;
        if (envelope->value > ar_envelope_max) {
          envelope->value = ar_envelope_max;
          envelope->state = AR_ENVELOPE_STATE_EARLY_DECAY;
        }
        break;
      case AR_ENVELOPE_STATE_EARLY_DECAY:
        envelope->value = envelope->value * envelope->early_decay + envelope->denormal;
        if (envelope->value <= envelope->late_decay_threshold) {
          envelope->state = AR_ENVELOPE_STATE_LATE_DECAY;
        }
        break;

      case AR_ENVELOPE_STATE_LATE_DECAY:
        envelope->value = envelope->value * envelope->late_decay + envelope->denormal;
        break;

      case AR_ENVELOPE_STATE_SILENT:
        envelope->value = 0;
        break;

      default:
        assert(0);
        break;
    }

    out[n] = envelope->value;
  }
}

void ar_envelope_start(ar_envelope_t* envelope) {
  assert(NULL != envelope);

  envelope->state = AR_ENVELOPE_STATE_ATTACK;
  envelope->silent = AR_SILENT_FALSE;
}

void ar_envelope_set_decay(double decay, ar_envelope_t* envelope) {
  assert(NULL != envelope);

  envelope->decay = decay;
  static const double c = -6.90775527898214;
  double d = c / (envelope->decay * envelope->sample_rate);
  envelope->early_decay = exp(d);
  envelope->late_decay = exp(d * 0.25);
}

void ar_envelope_set_attack(double attack, ar_envelope_t* envelope) {
  assert(NULL != envelope);

  double local_attack = filur_max(attack, ar_envelope_min_attack);

  envelope->attack = 1.0f / (envelope->sample_rate * local_attack);
}

void ar_envelope_set_late_decay_threshold(double threshold, ar_envelope_t* envelope) {
  envelope->late_decay_threshold = filur_limit(threshold, 0.0, 1.0);
}
