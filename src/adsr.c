/*
 * Copyright 2018-2024 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filur/adsr.h>

#include <filur/utils.h>

#include <assert.h>
#include <math.h>
#include <stddef.h>
#include <string.h>

enum {
  ADSR_ATTACK_STATE = 0,
  ADSR_DECAY_STATE,
  ADSR_SUSTAIN_STATE,
  ADSR_RELEASE_STATE,
  ADSR_SILENT_STATE
};

static const double adsr_silent_threshold = 0.001f;
static const double adsr_min_attack = 1e-4f;
static const double adsr_min_decay = 1e-3f;
static const double adsr_min_release = 1e-3f;

static int adsr_attack(double* out,
                       int number_of_frames,
                       adsr_state_t* state,
                       const adsr_parameters_t* parameters);
static int adsr_decay(double* out,
                      int number_of_frames,
                      adsr_state_t* state,
                      const adsr_parameters_t* parameters);
static int adsr_sustain(double* out,
                        int number_of_frames,
                        adsr_state_t* state,
                        const adsr_parameters_t* parameters);
static int adsr_release(double* out,
                        int number_of_frames,
                        adsr_state_t* state,
                        const adsr_parameters_t* parameters);
static inline void adsr_is_silent(adsr_state_t* state);

void adsr_init_parameters(double sample_rate, adsr_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->sample_rate = sample_rate;

  adsr_set_attack(1e-3, parameters);
  adsr_set_decay(200e-3, parameters);
  adsr_set_sustain(1.0, parameters);
  adsr_set_release(50e-3, parameters);
}

void adsr_set_attack(double attack, adsr_parameters_t* parameters) {
  assert(NULL != parameters);

  if (attack < adsr_min_attack) {
    attack = adsr_min_attack;
  }

  parameters->attack = 1.0 / (attack * parameters->sample_rate);
}

void adsr_set_decay(double decay, adsr_parameters_t* parameters) {
  assert(NULL != parameters);

  if (decay < adsr_min_decay) {
    decay = adsr_min_decay;
  }

  static const double c = -6.90775527898214;
  parameters->decay = exp(c / (decay * parameters->sample_rate));
}

void adsr_set_sustain(double sustain, adsr_parameters_t* parameters) {
  assert(NULL != parameters);

  parameters->sustain = filur_limit(sustain, 0.0, 1.0);
}

void adsr_set_release(double release, adsr_parameters_t* parameters) {
  assert(NULL != parameters);

  if (adsr_min_release > release) {
    release = adsr_min_release;
  }
  static const double c = -6.90775527898214;
  parameters->release = exp(c / (release * parameters->sample_rate));
}

void adsr_init_state(adsr_state_t* state) {
  assert(NULL != state);

  state->silent = ADSR_SILENT_TRUE;
  state->state = ADSR_SILENT_STATE;
  state->value = 0;
  state->denormal = 1e-10;
}

void adsr_process(double* out,
                  int number_of_frames,
                  adsr_state_t* state,
                  const adsr_parameters_t* parameters) {
  assert(NULL != out);
  assert(NULL != state);
  assert(NULL != parameters);

  int number_of_frames_to_process = number_of_frames;
  int number_of_processed_frames = 0;
  double* env = out;

  if ((ADSR_ATTACK_STATE != state->state) && (state->value < adsr_silent_threshold)) {
    state->silent = ADSR_SILENT_TRUE;
    state->state = ADSR_SILENT_STATE;
  }

  while (0 < number_of_frames_to_process) {
    switch (state->state) {
      case ADSR_ATTACK_STATE:
        number_of_processed_frames = adsr_attack(env, number_of_frames_to_process, state, parameters);
        break;

      case ADSR_DECAY_STATE:
        number_of_processed_frames = adsr_decay(env, number_of_frames_to_process, state, parameters);
        break;

      case ADSR_SUSTAIN_STATE:
        number_of_processed_frames = adsr_sustain(env, number_of_frames_to_process, state, parameters);
        break;

      case ADSR_RELEASE_STATE:
        number_of_processed_frames = adsr_release(env, number_of_frames_to_process, state, parameters);
        break;

      case ADSR_SILENT_STATE:
        state->value = 0;
        for (int n = 0; n < number_of_frames_to_process; ++n) {
          env[n] = 0;
        }
        number_of_processed_frames = number_of_frames_to_process;
        break;

      default:
        assert(0);
        break;
    }

    number_of_frames_to_process -= number_of_processed_frames;
    env += number_of_processed_frames;
  }
}

void adsr_gate_on(adsr_state_t* state) {
  assert(NULL != state);

  state->state = ADSR_ATTACK_STATE;
  state->silent = ADSR_SILENT_FALSE;
}

void adsr_gate_off(adsr_state_t* state) {
  assert(NULL != state);

  if (ADSR_SILENT_STATE != state->state) {
    state->state = ADSR_RELEASE_STATE;
  }

  if (ADSR_SILENT_STATE == state->state) {
    state->silent = ADSR_SILENT_TRUE;
  }
}

int adsr_attack(double* out,
                int number_of_frames,
                adsr_state_t* state,
                const adsr_parameters_t* parameters) {
  assert(NULL != out);
  assert(NULL != state);
  assert(NULL != parameters);

  int number_of_frames_to_process = number_of_frames;
  int n = 0;
  for (; n < number_of_frames_to_process; ++n) {
    out[n] = state->value;
    state->value += parameters->attack;
    if (state->value >= 1.0) {
      state->state = ADSR_DECAY_STATE;
      state->value = 1.0;
      number_of_frames_to_process = 0;
    }
  }

  return n;
}

int adsr_decay(double* out,
               int number_of_frames,
               adsr_state_t* state,
               const adsr_parameters_t* parameters) {
  assert(NULL != out);
  assert(NULL != state);
  assert(NULL != parameters);

  int n = 0;
  int number_of_frames_to_process = number_of_frames;

  state->denormal = -state->denormal;

  for (; n < number_of_frames_to_process; ++n) {
    out[n] = state->value;
    state->value = state->value * parameters->decay + state->denormal;
    if (state->value <= parameters->sustain) {
      state->state = ADSR_SUSTAIN_STATE;
      number_of_frames_to_process = 0;
    }
  }

  adsr_is_silent(state);

  return n;
}

int adsr_sustain(double* out,
                 int number_of_frames,
                 adsr_state_t* state,
                 const adsr_parameters_t* parameters) {
  assert(NULL != out);
  assert(NULL != state);
  assert(NULL != parameters);

  state->value = parameters->sustain;
  for (int n = 0; n < number_of_frames; ++n) {
    out[n] = state->value;
  }

  adsr_is_silent(state);

  return number_of_frames;
}

int adsr_release(double* out,
                 int number_of_frames,
                 adsr_state_t* state,
                 const adsr_parameters_t* parameters) {
  assert(NULL != out);
  assert(NULL != state);
  assert(NULL != parameters);

  state->denormal = -state->denormal;
  for (int n = 0; n < number_of_frames; ++n) {
    out[n] = state->value;
    state->value = state->value * parameters->release + state->denormal;
  }

  adsr_is_silent(state);

  return number_of_frames;
}

static inline void adsr_is_silent(adsr_state_t* state) {
  if (state->value < adsr_silent_threshold) {
    state->value = 0;
    state->state = ADSR_SILENT_STATE;
  }
}
