/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filur/int_list.h>

#include <assert.h>
#include <stddef.h>

static void int_list_pool_init(size_t size, int_list_pool_element_t* pool);
static int_list_element_t* int_list_pool_find_free(
    size_t size, int_list_pool_element_t* pool);
static void int_list_pool_free(int_list_element_t* element,
                               size_t size,
                               int_list_pool_element_t* pool);
static void int_list_element_init(int value,
                                  int_list_element_t* previous,
                                  int_list_element_t* next,
                                  int_list_element_t* element);

void int_list_init(int_list_t* list) {
  assert(NULL != list);

  int_list_pool_init(INT_LIST_POOL_SIZE, list->pool);

  list->back = NULL;
  list->front = NULL;
}

void int_list_push(int value, int_list_t* list) {
  assert(NULL != list);

  /* check that both back and front are NULL or not NULL */
  assert(((NULL == list->back) && (NULL == list->front)) ||
         ((NULL != list->back) && (NULL != list->front)));

  int_list_element_t* element = int_list_pool_find_free(INT_LIST_POOL_SIZE, list->pool);
  /* if pool is full, remove oldest value from list */
  if (NULL == element) {
    int_list_remove(list->back->value, list);
    element = int_list_pool_find_free(INT_LIST_POOL_SIZE, list->pool);
  }

  if (NULL != element) {
    int_list_element_init(value, list->front, NULL, element);

    if (NULL == list->back) {
      list->back = element;
      list->front = element;
    } else {
      assert(NULL == list->front->next);
      list->front->next = element;
      list->front = element;
    }
  }
}

void int_list_pop(int_list_t* list) {
  assert(NULL != list);

  /* check that both back and front are NULL or not NULL */
  assert(((NULL == list->back) && (NULL == list->front)) ||
         ((NULL != list->back) && (NULL != list->front)));

  if (NULL != list->front) {
    assert(NULL == list->front->next);
    int_list_remove(list->front->value, list);
  }
}

int int_list_top(const int_list_t* list) {
  assert(NULL != list);

  /* check that both back and front are NULL or not NULL */
  assert(((NULL == list->back) && (NULL == list->front)) ||
         ((NULL != list->back) && (NULL != list->front)));

  int ret_val = 0;

  if (NULL != list->front) {
    assert(NULL == list->front->next);
    ret_val = list->front->value;
  }

  return ret_val;
}

void int_list_remove(int value, int_list_t* list) {
  assert(NULL != list);

  /* check that both back and front are NULL or not NULL */
  assert(((NULL == list->back) && (NULL == list->front)) ||
         ((NULL != list->back) && (NULL != list->front)));

  int_list_element_t* element = list->front;
  while (NULL != element) {
    int_list_element_t* element_to_remove = NULL;
    if (value == element->value) {
      element_to_remove = element;
    }
    element = element->previous;

    if (NULL != element_to_remove) {
      if (NULL != element_to_remove->next) {
        element_to_remove->next->previous = element_to_remove->previous;
      }

      if (NULL != element_to_remove->previous) {
        element_to_remove->previous->next = element_to_remove->next;
      }

      if (NULL == element_to_remove->next) {
        list->front = element_to_remove->previous;
      }

      if (NULL == element_to_remove->previous) {
        list->back = element_to_remove->next;
      }

      int_list_pool_free(element_to_remove, INT_LIST_POOL_SIZE, list->pool);
    }
  }
}

size_t int_list_size(const int_list_t* list) {
  assert(NULL != list);

  size_t size = 0;
  int_list_element_t* element = list->back;
  while (NULL != element) {
    ++size;
    element = element->next;
  }

  return size;
}

void int_list_clear(int_list_t* list) {
  assert(NULL != list);

  int_list_init(list);
}

static void int_list_pool_init(size_t size, int_list_pool_element_t* pool) {
  assert(NULL != pool);

  for (size_t index = 0; index < size; ++index) {
    pool[index].free = 1;
    int_list_element_init(0, NULL, NULL, &pool[index].element);
  }
}

static int_list_element_t* int_list_pool_find_free(size_t size,
                                                   int_list_pool_element_t* pool) {
  assert(NULL != pool);

  int_list_element_t* element = NULL;

  for (size_t index = 0; (index < size) && (NULL == element); ++index) {
    if (1 == pool[index].free) {
      pool[index].free = 0;
      element = &pool[index].element;
    }
  }

  return element;
}

static void int_list_pool_free(int_list_element_t* element,
                               size_t size,
                               int_list_pool_element_t* pool) {
  assert(NULL != element);
  assert(NULL != pool);

  for (size_t index = 0; (index < size) && (NULL != element); ++index) {
    if (element == &pool[index].element) {
      pool[index].free = 1;
      int_list_element_init(0, NULL, NULL, &pool[index].element);
      element = NULL;
    }
  }
}

static void int_list_element_init(int value,
                                  int_list_element_t* previous,
                                  int_list_element_t* next,
                                  int_list_element_t* element) {
  assert(NULL != element);

  element->value = value;
  element->previous = previous;
  element->next = next;
}
