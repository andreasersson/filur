/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filur/metal_noise.h>

#include <stdlib.h>
#include <stddef.h>
#include <assert.h>

typedef struct {
  double f1;
  double f2;
  double f3;
  double f4;
  double f5;
  double f6;

} metal_noise_coeffs_t;

static const double metal_noise_default_frequency = 100.0;

static const metal_noise_coeffs_t metal_noise_coeffs[METAL_NOISE_NUM_TYPES] = {
    { 1.0, 1.484848484848485, 1.803030303030303, 2.545454545454545, 5.454545454545454, 8.080808080808081 },
    { 1.0, 1.937500000000000, 3.41666666666666, 5.083333333333333, 8.625000000000000, 11.29166666666667 },
    { 1.0, 2.40483, 3.83171, 5.52008, 8.41724, 10.1735 },
    { 1.0, 1.414784394250513, 1.829568788501027, 2.33264887063655, 5.250513347022587, 8.080808080808081 } };

static inline double metal_noise_ramp(double x, double dx) {
  x += dx;
  if (x >= 1.0) {
    x -= 2.0f;
  }

  return x;
}

static inline double metal_noise_sign(double x) {
  return (x < 0.0) ? -1.0 : 1.0;
}

void metal_noise_init(double sample_rate, metal_noise_t* noise) {
  assert(NULL != noise);

  noise->sample_time = 1.0 / sample_rate;

  metal_noise_set_type(METAL_NOISE_TYPE_1, noise);
  metal_noise_set_frequency(metal_noise_default_frequency, noise);

  srand(4711);
  noise->y1 = (double) rand() / RAND_MAX;
  noise->y2 = (double) rand() / RAND_MAX;
  noise->y3 = (double) rand() / RAND_MAX;
  noise->y4 = (double) rand() / RAND_MAX;
  noise->y5 = (double) rand() / RAND_MAX;
  noise->y6 = (double) rand() / RAND_MAX;
}

void metal_noise_set_frequency(double frequency, metal_noise_t* noise) {
  assert(NULL != noise);

  noise->frequency = frequency;

  assert(noise->type < METAL_NOISE_NUM_TYPES);
  double dy = 2.0 * noise->frequency * noise->sample_time;
  noise->dy1 = dy * metal_noise_coeffs[noise->type].f1;
  noise->dy2 = dy * metal_noise_coeffs[noise->type].f2;
  noise->dy3 = dy * metal_noise_coeffs[noise->type].f3;
  noise->dy4 = dy * metal_noise_coeffs[noise->type].f4;
  noise->dy5 = dy * metal_noise_coeffs[noise->type].f5;
  noise->dy6 = dy * metal_noise_coeffs[noise->type].f6;
}

void metal_noise_set_type(metal_noise_type_t type, metal_noise_t* noise) {
  assert(NULL != noise);
  assert(type < METAL_NOISE_NUM_TYPES);

  noise->type = type;
}

void metal_noise_process(double* out, int number_of_frames, metal_noise_t* noise) {
  assert(NULL != noise);
  assert(NULL != out);

  const double gain = 0.16;

  for (int n = 0; n < number_of_frames; ++n) {
    noise->y1 = metal_noise_ramp(noise->y1, noise->dy1);
    double sample = metal_noise_sign(noise->y1);

    noise->y2 = metal_noise_ramp(noise->y2, noise->dy2);
    sample += metal_noise_sign(noise->y2);

    noise->y3 = metal_noise_ramp(noise->y3, noise->dy3);
    sample += metal_noise_sign(noise->y3);

    noise->y4 = metal_noise_ramp(noise->y4, noise->dy4);
    sample += metal_noise_sign(noise->y4);

    noise->y5 = metal_noise_ramp(noise->y5, noise->dy5);
    sample += metal_noise_sign(noise->y5);

    noise->y6 = metal_noise_ramp(noise->y6, noise->dy6);
    sample += metal_noise_sign(noise->y6);

    out[n] = gain * sample;
  }
}

metal_noise_type_t metal_noise_type(double value) {
  int int_value = (int)(value);
  metal_noise_type_t type = METAL_NOISE_TYPE_1;
  switch (int_value) {
    case METAL_NOISE_TYPE_1:
      type = METAL_NOISE_TYPE_1;
      break;

    case METAL_NOISE_TYPE_2:
      type = METAL_NOISE_TYPE_2;
      break;

    case METAL_NOISE_TYPE_3:
      type = METAL_NOISE_TYPE_3;
      break;

    case METAL_NOISE_TYPE_4:
      type = METAL_NOISE_TYPE_4;
      break;

    default:
      assert(0);
      break;
  }

  return type;
}

