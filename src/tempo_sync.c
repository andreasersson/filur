/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filur/tempo_sync.h>

#include <assert.h>

double tempo_sync_to_rate(const tempo_sync_t tempo_sync) {
  double rate = 1.0;

  switch (tempo_sync) {
    case TEMPO_SYNC_FREE_RUNNING:
      rate = 1.0;
      break;

    case TEMPO_SYNC_16TH:
      rate = 4.0;
      break;

    case TEMPO_SYNC_8TH:
      rate = 2.0;
      break;

    case TEMPO_SYNC_4TH:
      rate = 1.0;
      break;

    case TEMPO_SYNC_2TH:
      rate = 1.0 / 2.0;
      break;

    case TEMPO_SYNC_1:
      rate = 1.0 / 4.0;
      break;

    case TEMPO_SYNC_2:
      rate = 1.0 / 8.0;
      break;

    case TEMPO_SYNC_4:
      rate = 1.0 / 16.0;
      break;

    default:
      assert(0);
      break;
  }

  return rate;
}

tempo_sync_t value_to_tempo_sync(double value) {
  int int_value = (int)(value);
  tempo_sync_t tempo_sync = TEMPO_SYNC_FREE_RUNNING;

  switch (int_value) {
    case TEMPO_SYNC_FREE_RUNNING:
      tempo_sync = TEMPO_SYNC_FREE_RUNNING;
      break;

    case TEMPO_SYNC_16TH:
      tempo_sync = TEMPO_SYNC_16TH;
      break;

    case TEMPO_SYNC_8TH:
      tempo_sync = TEMPO_SYNC_8TH;
      break;

    case TEMPO_SYNC_4TH:
      tempo_sync = TEMPO_SYNC_4TH;
      break;

    case TEMPO_SYNC_2TH:
      tempo_sync = TEMPO_SYNC_2TH;
      break;

    case TEMPO_SYNC_1:
      tempo_sync = TEMPO_SYNC_1;
      break;

    case TEMPO_SYNC_2:
      tempo_sync = TEMPO_SYNC_2;
      break;

    case TEMPO_SYNC_4:
      tempo_sync = TEMPO_SYNC_4;
      break;

    default:
      break;
  }

  return tempo_sync;
}
