/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filur/bilin.h>

#include <math.h>
#include <stddef.h>
#include <assert.h>

#ifndef M_PI
  #define M_PI 3.14159265358979323846264338327950288
#endif /* M_PI */

static inline void bilin_lp_recalc(bilin_lp_t* lp) {
  assert(NULL != lp);

  double w0 = 2.0 * lp->sample_rate * tan(lp->cutoff * lp->sample_time * 2 * M_PI * 0.5);
  double k = w0;
  lp->b0 = k * lp->sample_time / (w0 * lp->sample_time + 2);
  lp->a0 = (w0 * lp->sample_time - 2) / (w0 * lp->sample_time + 2);
}

static inline void bilin_hp2_recalc(bilin_hp2_t* hp) {
  assert(NULL != hp);

  /*
   * b0=4KQ
   * b1=-8KQ
   * b2=4KQ
   * a0=w0^2QT^2 + 2w0T + 4Q
   * a1=2w0^2QT^2 - 8Q
   * a2=w0^2QT^2 - 2w0T + 4Q
   */
  double w0 = 2.0 * hp->sample_rate * tan(hp->cutoff * hp->sample_time * M_PI * 0.5);
  hp->k = 1.0;
  hp->b0 = 4.0 * hp->k * hp->q;
  hp->b1 = -8.0 * hp->k * hp->q;
  hp->b2 = 4.0 * hp->k * hp->q;
  hp->a0 = w0 * w0 * hp->q * hp->sample_time * hp->sample_time + 2.0 * w0 * hp->sample_time + 4.0 * hp->q;
  hp->a1 = 2.0 * w0 * w0 * hp->q * hp->sample_time * hp->sample_time - 8.0 * hp->q;
  hp->a2 = w0 * w0 * hp->q * hp->sample_time * hp->sample_time - 2.0 * w0 * hp->sample_time + 4.0 * hp->q;
  hp->b0 = hp->b0 / hp->a0;
  hp->b1 = hp->b1 / hp->a0;
  hp->b2 = hp->b2 / hp->a0;
  hp->a1 = hp->a1 / hp->a0;
  hp->a2 = hp->a2 / hp->a0;
}

void bilin_lp_init(double sample_rate, bilin_lp_t* lp) {
  assert(NULL != lp);

  lp->sample_rate = sample_rate;
  lp->sample_time = 1.0 / sample_rate;
  lp->denormal = 1e-10;
  lp->cutoff = 50.0;

  bilin_lp_recalc(lp);
  bilin_lp_reset(lp);
}

void bilin_lp_process(const double* in, double* out, int number_of_frames, bilin_lp_t* lp) {
  assert(NULL != lp);

  lp->denormal = -lp->denormal;

  for (int n = 0; n < number_of_frames; ++n) {
    lp->y = lp->b0 * (in[n] + lp->x1) - lp->a0 * lp->y + lp->denormal;
    lp->x1 = in[n];
    out[n] = lp->y;
  }
}

void bilin_lp_reset(bilin_lp_t* lp) {
  assert(NULL != lp);

  lp->x1 = 0;
  lp->y = 0;
}

void bilin_lp_set_cutoff(double cutoff, bilin_lp_t* lp) {
  assert(NULL != lp);

  lp->cutoff = cutoff;
  bilin_lp_recalc(lp);
}

void bilin_hp2_init(double sample_rate, bilin_hp2_t* hp) {
  assert(NULL != hp);

  hp->sample_rate = sample_rate;
  hp->sample_time = 1.0 / sample_rate;
  hp->denormal = 1e-10;
  hp->cutoff = 3000;
  hp->q = 0.707;

  bilin_hp2_reset(hp);
  bilin_hp2_recalc(hp);
}

void bilin_hp2_process(const double* in, double* out, int number_of_frames, bilin_hp2_t* hp) {
  assert(NULL != hp);

  hp->denormal = -hp->denormal;

  for (int n = 0; n < number_of_frames; ++n) {
    hp->y0 = hp->b0 * in[n] + hp->b1 * hp->x1 + hp->b2 * hp->x2 - hp->a1 * hp->y1 - hp->a2 * hp->y2 + hp->denormal;
    hp->x2 = hp->x1;
    hp->x1 = in[n];
    hp->y2 = hp->y1;
    hp->y1 = hp->y0;
    out[n] = hp->y0;
  }
}

void bilin_hp2_reset(bilin_hp2_t* hp) {
  assert(NULL != hp);

  hp->y0 = 0;
  hp->y1 = 0;
  hp->y2 = 0;
  hp->x1 = 0;
  hp->x2 = 0;
}

void bilin_hp2_set_cutoff(double cutoff, bilin_hp2_t* hp) {
  assert(NULL != hp);

  hp->cutoff = cutoff;
  bilin_hp2_recalc(hp);
}

void bilin_hp2_set_q(double q, bilin_hp2_t* hp) {
  assert(NULL != hp);

  hp->q = q;
  bilin_hp2_recalc(hp);
}
