/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef METAL_NOISE_H
#define METAL_NOISE_H

#include <stdint.h>

typedef enum {
  METAL_NOISE_TYPE_1 = 0,
  METAL_NOISE_TYPE_2,
  METAL_NOISE_TYPE_3,
  METAL_NOISE_TYPE_4,
  /* NOTE: add new types before METAL_NOISE_NUM_TYPES */
  METAL_NOISE_NUM_TYPES,
} metal_noise_type_t;

typedef struct {
  double sample_time;
  double frequency;
  metal_noise_type_t type;

  double dy1;
  double dy2;
  double dy3;
  double dy4;
  double dy5;
  double dy6;
  double y1;
  double y2;
  double y3;
  double y4;
  double y5;
  double y6;
} metal_noise_t;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void metal_noise_init(double sample_rate, metal_noise_t* noise);
void metal_noise_set_frequency(double frequency, metal_noise_t* noise);
void metal_noise_set_type(metal_noise_type_t type, metal_noise_t* noise);
void metal_noise_process(double* out, int number_of_frames, metal_noise_t* noise);
metal_noise_type_t metal_noise_type(double value);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* METAL_NOISE_H */
