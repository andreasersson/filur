/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LFO_H
#define LFO_H

typedef struct {
  double sample_time;
  double delta_alpha;
} lfo_parameters_t;

typedef struct {
  double alpha;
} lfo_state_t;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void lfo_init_parameters(double sample_rate, lfo_parameters_t* parameters);
void lfo_set_frequency(double frequency, lfo_parameters_t* parameters);

void lfo_init_state(lfo_state_t* state);
void lfo_set_phase(double phase, lfo_state_t* state);
void lfo_process_sine(double* out,
                      int number_of_frames,
                      lfo_state_t* state,
                      const lfo_parameters_t* parameters);
void lfo_process_triangle(double* out,
                          int number_of_frames,
                          lfo_state_t* state,
                          const lfo_parameters_t* parameters);
void lfo_process_triangle_normalized(double* out,
                                     int number_of_frames,
                                     lfo_state_t* state,
                                     const lfo_parameters_t* parameters);
void lfo_process_triangle_sqr(double* out,
                              int number_of_frames,
                              lfo_state_t* state,
                              const lfo_parameters_t* parameters);
void lfo_process_saw(double* out,
                     int number_of_frames,
                     lfo_state_t* state,
                     const lfo_parameters_t* parameters);
void lfo_process_square(double* out,
                        int number_of_frames,
                        lfo_state_t* state,
                        const lfo_parameters_t* parameters);
void lfo_process_rnd(double* out,
                     int number_of_frames,
                     lfo_state_t* state,
                     const lfo_parameters_t* parameters);

double lfo_tempo_to_frequency(double tempo, double rate);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* LFO_H */
