/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef INT_LIST_H
#define INT_LIST_H

#include <stddef.h>

typedef struct int_list_element {
  int value;
  struct int_list_element* previous;
  struct int_list_element* next;
} int_list_element_t;

enum {
  INT_LIST_POOL_SIZE = 32
};

typedef struct {
  int free;
  int_list_element_t element;
} int_list_pool_element_t;

typedef struct {
  int_list_element_t* back;
  int_list_element_t* front;

  int_list_pool_element_t pool[INT_LIST_POOL_SIZE];
} int_list_t;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void int_list_init(int_list_t* list);
void int_list_push(int value, int_list_t* list);
void int_list_pop(int_list_t* list);
int int_list_top(const int_list_t* list);
void int_list_remove(int value, int_list_t* list);
size_t int_list_size(const int_list_t* list);
void int_list_clear(int_list_t* list);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* INT_LIST_H */

