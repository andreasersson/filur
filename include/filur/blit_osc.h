/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef BLIT_OSC_H
#define BLIT_OSC_H

typedef struct {
  double alpha;
  double delta_alpha;
  double m;
  double band_limit_factor;
  double attenuation;
  double integrator;
  double sample_rate;
  double sample_time;
  double band_limit_frequency;
} blit_t;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void blit_init(double sample_rate, blit_t* blit);
void blit_clear(blit_t* blit);
void blit_set_frequency(double frequency, blit_t* blit);

double blit_process_sine(blit_t* blit);
double blit_process_saw(blit_t* blit);
double blit_process_pulse(const double pwm, blit_t* blit);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* BLIT_OSC_H */
