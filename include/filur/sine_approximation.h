/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SINE_APPROXIMATION_H
#define SINE_APPROXIMATION_H

#define sine_approx_a1  ( 1.0)
#define sine_approx_a3  (-1.0/(1.0*2.0*3.0))
#define sine_approx_a5  ( 1.0/(1.0*2.0*3.0*4.0*5.0))
#define sine_approx_a7  (-1.0/(1.0*2.0*3.0*4.0*5.0*6.0*7.0))
#define sine_approx_a9  ( 1.0/(1.0*2.0*3.0*4.0*5.0*6.0*7.0*8.0*9.0))
#define sine_approx_a11 (-1.0/(1.0*2.0*3.0*4.0*5.0*6.0*7.0*8.0*9.0*10.0*11.0))
#define sine_approx_a13 ( 1.0/(1.0*2.0*3.0*4.0*5.0*6.0*7.0*8.0*9.0*10.0*11.0*12.0*13.0))
#define sine_approx_a15 (-1.0/(1.0*2.0*3.0*4.0*5.0*6.0*7.0*8.0*9.0*10.0*11.0*12.0*13.0*14.0*15.0))
#define sine_approx_a17 ( 1.0/(1.0*2.0*3.0*4.0*5.0*6.0*7.0*8.0*9.0*10.0*11.0*12.0*13.0*14.0*15.0*16.0*17.0))
#define sine_approx_a19 (-1.0/(1.0*2.0*3.0*4.0*5.0*6.0*7.0*8.0*9.0*10.0*11.0*12.0*13.0*14.0*15.0*16.0*17.0*18.0*19.0))
#define sine_approx_a21 ( 1.0/(1.0*2.0*3.0*4.0*5.0*6.0*7.0*8.0*9.0*10.0*11.0*12.0*13.0*14.0*15.0*16.0*17.0*18.0*19.0*20.0*21.0))

#define sine_approx_9(x,  x2) (x*(sine_approx_a1 + x2*(sine_approx_a3 + x2*(sine_approx_a5 + x2*(sine_approx_a7 + x2*(sine_approx_a9))))))
#define sine_approx_11(x, x2) (x*(sine_approx_a1 + x2*(sine_approx_a3 + x2*(sine_approx_a5 + x2*(sine_approx_a7 + x2*(sine_approx_a9 + x2*(sine_approx_a11)))))))
#define sine_approx_13(x, x2) (x*(sine_approx_a1 + x2*(sine_approx_a3 + x2*(sine_approx_a5 + x2*(sine_approx_a7 + x2*(sine_approx_a9 + x2*(sine_approx_a11 + x2*(sine_approx_a13))))))))
#define sine_approx_15(x, x2) (x*(sine_approx_a1 + x2*(sine_approx_a3 + x2*(sine_approx_a5 + x2*(sine_approx_a7 + x2*(sine_approx_a9 + x2*(sine_approx_a11 + x2*(sine_approx_a13 + x2*(sine_approx_a15)))))))))
#define sine_approx_17(x, x2) (x*(sine_approx_a1 + x2*(sine_approx_a3 + x2*(sine_approx_a5 + x2*(sine_approx_a7 + x2*(sine_approx_a9 + x2*(sine_approx_a11 + x2*(sine_approx_a13 + x2*(sine_approx_a15 + x2*(sine_approx_a17))))))))))
#define sine_approx_19(x, x2) (x*(sine_approx_a1 + x2*(sine_approx_a3 + x2*(sine_approx_a5 + x2*(sine_approx_a7 + x2*(sine_approx_a9 + x2*(sine_approx_a11 + x2*(sine_approx_a13 + x2*(sine_approx_a15 + x2*(sine_approx_a17 + x2*(sine_approx_a19)))))))))))
#define sine_approx_21(x, x2) (x*(sine_approx_a1 + x2*(sine_approx_a3 + x2*(sine_approx_a5 + x2*(sine_approx_a7 + x2*(sine_approx_a9 + x2*(sine_approx_a11 + x2*(sine_approx_a13 + x2*(sine_approx_a15 + x2*(sine_approx_a17 + x2*(sine_approx_a19 + x2*(sine_approx_a21))))))))))))

#define sine_approx_a2  (-1.0/(1.0*2.0))
#define sine_approx_a4  ( 1.0/(1.0*2.0*3.0*4.0))
#define sine_approx_a6  (-1.0/(1.0*2.0*3.0*4.0*5.0*6.0))
#define sine_approx_a8  ( 1.0/(1.0*2.0*3.0*4.0*5.0*6.0*7.0*8.0))
#define sine_approx_a10 (-1.0/(1.0*2.0*3.0*4.0*5.0*6.0*7.0*8.0*9.0*10.0))
#define sine_approx_a12 ( 1.0/(1.0*2.0*3.0*4.0*5.0*6.0*7.0*8.0*9.0*10.0*11.0*12.0))
#define sine_approx_a14 (-1.0/(1.0*2.0*3.0*4.0*5.0*6.0*7.0*8.0*9.0*10.0*11.0*12.0*13.0*14.0))
#define sine_approx_a16 ( 1.0/(1.0*2.0*3.0*4.0*5.0*6.0*7.0*8.0*9.0*10.0*11.0*12.0*13.0*14.0*15.0*16.0))
#define sine_approx_a18 (-1.0/(1.0*2.0*3.0*4.0*5.0*6.0*7.0*8.0*9.0*10.0*11.0*12.0*13.0*14.0*15.0*16.0*17.0*18.0))
#define sine_approx_a20 ( 1.0/(1.0*2.0*3.0*4.0*5.0*6.0*7.0*8.0*9.0*10.0*11.0*12.0*13.0*14.0*15.0*16.0*17.0*18.0*19.0*20.0))

#define cos_approx_8(x2)  (1.0 + x2*(sine_approx_a2 + x2*(sine_approx_a4 + x2*(sine_approx_a6 + x2*(sine_approx_a8)))))
#define cos_approx_10(x2) (1.0 + x2*(sine_approx_a2 + x2*(sine_approx_a4 + x2*(sine_approx_a6 + x2*(sine_approx_a8 + x2*(sine_approx_a10))))))
#define cos_approx_12(x2) (1.0 + x2*(sine_approx_a2 + x2*(sine_approx_a4 + x2*(sine_approx_a6 + x2*(sine_approx_a8 + x2*(sine_approx_a10 + x2*(sine_approx_a12)))))))
#define cos_approx_14(x2) (1.0 + x2*(sine_approx_a2 + x2*(sine_approx_a4 + x2*(sine_approx_a6 + x2*(sine_approx_a8 + x2*(sine_approx_a10 + x2*(sine_approx_a12 + x2*(sine_approx_a14))))))))
#define cos_approx_16(x2) (1.0 + x2*(sine_approx_a2 + x2*(sine_approx_a4 + x2*(sine_approx_a6 + x2*(sine_approx_a8 + x2*(sine_approx_a10 + x2*(sine_approx_a12 + x2*(sine_approx_a14 + x2*(sine_approx_a16)))))))))
#define cos_approx_18(x2) (1.0 + x2*(sine_approx_a2 + x2*(sine_approx_a4 + x2*(sine_approx_a6 + x2*(sine_approx_a8 + x2*(sine_approx_a10 + x2*(sine_approx_a12 + x2*(sine_approx_a14 + x2*(sine_approx_a16 + x2*(sine_approx_a18))))))))))
#define cos_approx_20(x2) (1.0 + x2*(sine_approx_a2 + x2*(sine_approx_a4 + x2*(sine_approx_a6 + x2*(sine_approx_a8 + x2*(sine_approx_a10 + x2*(sine_approx_a12 + x2*(sine_approx_a14 + x2*(sine_approx_a16 + x2*(sine_approx_a18 + x2*(sine_approx_a20)))))))))))

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

static inline double sine_9(double x) {
  return sine_approx_9(x, x * x);
}

static inline double sine_11(double x) {
  return sine_approx_11(x, x * x);
}

static inline double sine_13(double x) {
  return sine_approx_13(x, x * x);
}

static inline double sine_15(double x) {
  return sine_approx_15(x, x * x);
}

static inline double sine_21(double x) {
  return sine_approx_21(x, x * x);
}

static inline double cos_8(double x) {
  return cos_approx_8(x * x);
}

static inline double cos_10(double x) {
  return cos_approx_10(x * x);
}

static inline double cos_12(double x) {
  return cos_approx_12(x * x);
}

static inline double cos_14(double x) {
  return cos_approx_14(x * x);
}

static inline double cos_16(double x) {
  return cos_approx_16(x * x);
}

static inline double cos_18(double x) {
  return cos_approx_18(x * x);
}

static inline double cos_20(double x) {
  return cos_approx_20(x * x);
}

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* SINE_APPROXIMATION_H */
