/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef BILIN_H
#define BILIN_H

typedef struct {
  double sample_rate;
  double sample_time;
  double y;
  double x1;
  double a0;
  double b0;
  double denormal;
  double cutoff;
} bilin_lp_t;

typedef struct {
  double sample_rate;
  double sample_time;
  double y0;
  double y1;
  double y2;
  double x1;
  double x2;
  double a0;
  double a1;
  double a2;
  double b0;
  double b1;
  double b2;
  double k;
  double denormal;
  double cutoff;
  double q;
} bilin_hp2_t;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void bilin_lp_init(double sample_rate, bilin_lp_t* lp);
void bilin_lp_process(const double* in, double* out, int number_of_frames, bilin_lp_t* lp);
void bilin_lp_reset(bilin_lp_t* lp);
void bilin_lp_set_cutoff(double cutoff, bilin_lp_t* lp);

void bilin_hp2_init(double sample_rate, bilin_hp2_t* hp);
void bilin_hp2_process(const double* in, double* out, int number_of_frames, bilin_hp2_t* hp);
void bilin_hp2_reset(bilin_hp2_t* hp);
void bilin_hp2_set_cutoff(double cutoff, bilin_hp2_t* hp);
void bilin_hp2_set_q(double q, bilin_hp2_t* hp);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* BILIN_H */
