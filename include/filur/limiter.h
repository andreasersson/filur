/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LIMITER_H
#define LIMITER_H

#include "znf_value.h"

typedef struct {
  znf_value_t gain;
} limiter_t;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void limiter_init(limiter_t* limiter);
void limiter_process(const double* in,
                     double* out,
                     int numberOfFrames,
                     limiter_t* limiter);
void limiter_set_gain(double gain, limiter_t* limiter);
void limiter_reset(limiter_t* limiter);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* LIMITER_H */
