/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef FILUR_UTILS_H
#define FILUR_UTILS_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

static double filur_min(double value_1, double value_2) {
  return (value_1 < value_2) ? value_1 : value_2;
}

static double filur_max(double value_1, double value_2) {
  return (value_1 > value_2) ? value_1 : value_2;
}

static double filur_limit(double value, double min, double max) {
  return filur_max(filur_min(value, max), min);
}

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* FILUR_UTILS_H */
