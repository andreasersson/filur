/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ADSR_H
#define ADSR_H

#include <stdint.h>

typedef enum {
  ADSR_SILENT_FALSE = 0,
  ADSR_SILENT_TRUE,
} adsr_silent_t;

typedef struct {
  double attack;
  double decay;
  double sustain;
  double release;

  double sample_rate;
} adsr_parameters_t;

typedef struct {
  adsr_silent_t silent;
  int state;
  double value;
  double denormal;
} adsr_state_t;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void adsr_init_parameters(double sample_rate, adsr_parameters_t* parameters);
void adsr_set_attack(double attack, adsr_parameters_t* parameters);
void adsr_set_decay(double decay, adsr_parameters_t* parameters);
void adsr_set_sustain(double sustain, adsr_parameters_t* parameters);
void adsr_set_release(double release, adsr_parameters_t* parameters);

void adsr_init_state(adsr_state_t* state);
void adsr_process(double* out,
                  int number_of_frames,
                  adsr_state_t* state,
                  const adsr_parameters_t* parameters);
void adsr_gate_on(adsr_state_t* state);
void adsr_gate_off(adsr_state_t* state);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* ADSR_H */
