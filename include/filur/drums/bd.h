/*
 * Copyright 2018-2024 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef BD_H
#define BD_H

#include <filur/ar_envelope.h>

typedef enum {
  BD_SILENT_FALSE = 0,
  BD_SILENT_TRUE,
} bd_silent_t;

typedef struct {
  double sample_time;
  double x;
  double dx;

  double frequency;
  double high_frequency;
} bd_tone_t;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void bd_tone_init(double sample_rate, bd_tone_t* tone);
void bd_tone_process(const double* envelope, double* out, int number_of_frames, bd_tone_t* tone);
void bd_tone_set_frequency(double frequency, double high_frequency, bd_tone_t* tone);

#ifdef __cplusplus
}
#endif /* __cplusplus */

typedef struct {
  double sample_rate;
  double y;
  double attack;
  double decay;
  double denormal;
  double threshold;
  bd_silent_t silent;
} bd_envelope_t;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void bd_envelope_init(double sample_rate, bd_envelope_t* envelope);
void bd_envelope_set_decay(double decay, bd_envelope_t* envelope);
void bd_envelope_set_threshold(double threshold, bd_envelope_t* envelope);
void bd_envelope_start(bd_envelope_t* envelope);
void bd_envelope_process(double* out, int number_of_frames, bd_envelope_t* envelope);

#ifdef __cplusplus
}
#endif /* __cplusplus */

typedef struct {
  double frequency;
  double tone;
  double decay;
  double sweep_decay;
  double sweep;
  double sweep_threshold;
  double gain;
} bd_parameters_t;

typedef struct {
  bd_envelope_t envelope;

  bd_tone_t low_tone;
  bd_tone_t high_tone;
  ar_envelope_t low_decay_envelope;
  ar_envelope_t high_decay_envelope;

  double tone_gain;
  double tga;
  double tgk;

  double denormal;

  bd_silent_t silent;
} bd_t;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void bd_init_parameters(bd_parameters_t* parameters);
void bd_set_decay(double decay, bd_parameters_t* parameters);
void bd_set_sweep_decay(double sweep_decay, bd_parameters_t* parameters);
void bd_set_frequency(double frequency, bd_parameters_t* parameters);
void bd_set_tone(double tone, bd_parameters_t* parameters);
void bd_set_gain(double gain, bd_parameters_t* parameters);
void bd_set_sweep(double sweep, bd_parameters_t* parameters);
void bd_set_sweep_threshold(double sweep_threshold, bd_parameters_t* parameters);

void bd_init(double sample_rate, bd_t* bd);
void bd_process(double* out, int number_of_frames, const bd_parameters_t* parameters, bd_t* bd);
void bd_choke(const bd_parameters_t* parameters, bd_t* bd);
void bd_note_on(const bd_parameters_t* parameters, bd_t* bd);
void bd_note_on_classic(const bd_parameters_t* parameters, bd_t* bd);
void bd_note_on_kick(const bd_parameters_t* parameters, bd_t* bd);
void bd_note_on_tom(const bd_parameters_t* parameters, bd_t* bd);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* BD_H */
