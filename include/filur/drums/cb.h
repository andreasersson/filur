/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CB_H
#define CB_H

#include <filur/ar_envelope.h>
#include <filur/biquad.h>

#include <stdint.h>

typedef enum {
  CB_SILENT_FALSE = 0,
  CB_SILENT_TRUE,
} cb_silent_t;

typedef struct {
  double gain;
  double frequency;
  double decay;
  double cutoff;
  double resonance;

  double sample_time;

  double osc0;
  double osc1;
  double osc0_delta;
  double osc1_delta;

  uint8_t silent;

  ar_envelope_t envelope;
  biquad_coeffs_t biquad_coeffs;
  biquad_state_t biquad_state;
} cb_t;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void cb_init(double sample_rate, cb_t* cb);
void cb_process(double* out, int number_of_frames, cb_t* cb);
void cb_note_on(cb_t* cb);
void cb_set_gain(double gain, cb_t* cb);
void cb_set_decay(double decay, cb_t* cb);
void cb_set_frequency(double frequency, cb_t* cb);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* CB_H */
