/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef HIHAT_H
#define HIHAT_H

#include <filur/biquad.h>
#include <filur/metal_noise.h>

#include <stdint.h>

typedef enum {
  HIHAT_ENV_STATE_OPEN,
  HIHAT_ENV_STATE_PEDAL,
  HIHAT_ENV_STATE_CLOSED,
  HIHAT_ENV_STATE_CHOKED
} hihat_envelope_state_t;

typedef enum {
  HIHAT_SILENT_FALSE,
  HIHAT_SILENT_TRUE
} hihat_silent_t;

typedef struct {
  double gain;
  double cutoff;
  double resonance;
  double closed_decay;
  double pedal_decay;
  double open_decay;
  double pedal_threshold;
  metal_noise_type_t noise;
  double frequency;
} hihat_parameters_t;

typedef struct {
  double sample_rate;
  double sample_time;
  double denormal;
  double envelope;
  double envelope_open_a;
  double envelope_pedal_a;
  double envelope_closed_a;
  double envelope_choked_a;
  double pedal_threshold;

  double cutoff_gain;
  double cutoff;
  double resonance;

  uint8_t silent;

  hihat_envelope_state_t state;

  metal_noise_t noise;
  biquad_coeffs_t biquad_coeffs;
  biquad_state_t biquad_state;

} hihat_t;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void hihat_init_parameters(hihat_parameters_t* parameters);
void hihat_set_gain(double gain, hihat_parameters_t* parameters);
void hihat_set_closed_decay(double decay, hihat_parameters_t* parameters);
void hihat_set_pedal_decay(double decay, hihat_parameters_t* parameters);
void hihat_set_open_decay(double decay, hihat_parameters_t* parameters);
void hihat_set_cutoff(double cutoff, hihat_parameters_t* parameters);
void hihat_set_resonance(double resonance, hihat_parameters_t* parameters);
void hihat_set_pedal_threshold(double threshold, hihat_parameters_t* parameters);
void hihat_set_noise(metal_noise_type_t noise, hihat_parameters_t* parameters);
void hihat_set_frequency(double frequency, hihat_parameters_t* parameters);

void hihat_init(double sample_rate, hihat_t* hihat);
void hihat_process(double* out, int number_of_frames,
                   const hihat_parameters_t* parameters,
                   hihat_t* hihat);
void hihat_closed_on(const hihat_parameters_t* parameters, hihat_t* hihat);
void hihat_pedal_on(const hihat_parameters_t* parameters, hihat_t* hihat);
void hihat_open_on(const hihat_parameters_t* parameters, hihat_t* hihat);
void hihat_choke(const hihat_parameters_t* parameters, hihat_t* hihat);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* HIHAT_H */
