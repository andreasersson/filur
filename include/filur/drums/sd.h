/*
 * Copyright 2018-2024 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SD_H
#define SD_H

#include <filur/noise.h>
#include <filur/bilin.h>

#include <stdint.h>

typedef enum {
  SD_ENVELOPE_STATE_ATTACK,
  SD_ENVELOPE_STATE_DECAY,
  SD_ENVELOPE_STATE_LATE_DECAY,
  SD_ENVELOPE_STATE_SILENT,
} sd_envelope_state_t;

typedef enum {
  SD_SILENT_FALSE = 0,
  SD_SILENT_TRUE,
} SD_silent_t;

typedef struct {
  double sample_rate;
  uint8_t silent;
  double decay;
  double snappy;
  double short_attack_time;
  double long_attack_time;
  double attack;
  double early_decay;
  double late_decy;
  double y;
  double denormal;
  double gate;
  sd_envelope_state_t state;
} sd_envelope_t;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void sd_envelope_init(double sample_rate, sd_envelope_t* envelope);
void sd_envelope_process(double* out, int number_of_frames, sd_envelope_t* envelope);
void sd_envelope_start(double accent, sd_envelope_t* envelope);
void sd_envelope_set_decay(double decay, sd_envelope_t* envelope);
void sd_envelope_set_snappy(double snappy, sd_envelope_t* envelope);

#ifdef __cplusplus
}
#endif /* __cplusplus */

typedef struct {
  double sample_rate;
  double sample_time;
  double x;
  double dx;

  double frequency;
} sd_tone_t;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void sd_tone_init(double sample_rate, sd_tone_t* tone);
void sd_tone_process(double* out, int number_of_frames, sd_tone_t* tone);
void sd_tone_set_frequency(double frequency, sd_tone_t* tone);

#ifdef __cplusplus
}
#endif /* __cplusplus */

typedef struct {
  double gain;
  double tone;
  double snappy;
  double decay;
  double frequency;

  double env;
  double envd;
  double enva;
  int env_state;
  double low_gain;
  double high_gain;

  sd_tone_t low_tone;
  sd_tone_t high_tone;

  sd_envelope_t envelope;
  noise_t noise;
  bilin_hp2_t hp;
  bilin_lp_t lp;

  uint8_t silent;

} sd_t;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void sd_init(double sample_rate, sd_t* sd);
void sd_set_gain(double gain, sd_t* sd);
void sd_set_tone(double tone, sd_t* sd);
void sd_set_snappy(double snappy, sd_t* sd);
void sd_set_decay(double decay, sd_t* sd);
void sd_set_frequency(double frequency, sd_t* sd);
void sd_process(double* out, int number_of_frames, sd_t* sd);
void sd_choke(sd_t* sd);
void sd_note_on(sd_t* sd);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* SD_H */
