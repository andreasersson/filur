/*
 * Copyright 2018-2024 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CLAP_H
#define CLAP_H

#include <filur/bilin.h>
#include <filur/biquad.h>
#include <filur/noise.h>

#include <stdint.h>

typedef struct {
  double gain;
  double decay;
  double early_decay;
  double early_delay;
  double cutoff;
  double resonance;
} clap_parameters_t;

typedef struct {
  double sample_rate;
  double level;
  double early_env;
  double release_env;
  double early_a;
  double release_a;
  double denormal;

  int delay;
  int early_delay_in_samples;
  int early_delay_counter;

  uint8_t silent;

  noise_t _noise;
  bilin_hp2_t hp;
  biquad_coeffs_t biquad_coeffs;
  biquad_state_t biquad_state;
} clap_t;

typedef enum {
  CLAP_SILENT_FALSE = 0,
  CLAP_SILENT_TRUE,
} clap_silent_t;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void clap_init_parameters(clap_parameters_t* parameters);
void clap_set_gain(double gain, clap_parameters_t* parameters);
void clap_set_decay(double decay, clap_parameters_t* parameters);
void clap_set_early_decay(double decay, clap_parameters_t* parameters);
void clap_set_early_delay(double delay, clap_parameters_t* parameters);
void clap_set_cutoff(double cutoff, clap_parameters_t* parameters);
void clap_set_resonance(double resonance, clap_parameters_t* parameters);

void clap_init(double sample_rate, clap_t* clap);
void clap_process(double* out, int number_of_frames, const clap_parameters_t* parameters, clap_t* clap);
void clap_choke(const clap_parameters_t* parameters, clap_t* clap);
void clap_note_on(const clap_parameters_t* parameters, clap_t* clap);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* CLAP_H */
