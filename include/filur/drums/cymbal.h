/*
 * Copyright 2018-2024 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CYMBAL_H
#define CYMBAL_H


#include <filur/biquad.h>
#include <filur/metal_noise.h>

#include <stdint.h>

typedef enum {
  CYMBAL_ENV_STATE_EARLY,
  CYMBAL_ENV_STATE_LATE,
  CYMBAL_ENV_STATE_CHOKED
} cymbal_envelope_state_t;

typedef enum {
  CYMBAL_SILENT_FALSE,
  CYMBAL_SILENT_TRUE
} cymbal_silent_t;

typedef struct {
  double gain;
  double frequency;
  double cutoff;
  double resonance;
  double early_decay;
  double late_decay;
  double decay_threshold;
  double swoosh;
  metal_noise_type_t noise;
} cymbal_parameters_t;

typedef struct {
  double sample_rate;
  double sample_time;
  double denormal;
  double envelope;
  double envelope_early_a;
  double envelope_late_a;
  double envelope_choked_a;
  double decay_threshold;
  double filter_envelope;
  double cutoff_gain;
  double cutoff;
  double resonance;

  cymbal_envelope_state_t state;

  uint8_t silent;

  metal_noise_t noise;
  biquad_coeffs_t biquad_coeffs;
  biquad_state_t biquad_state;

} cymbal_t;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void cymbal_init_parameters(cymbal_parameters_t* parameters);
void cymbal_set_gain(double gain, cymbal_parameters_t* parameters);
void cymbal_set_frequency(double frequency, cymbal_parameters_t* parameters);
void cymbal_set_early_decay(double decay, cymbal_parameters_t* parameters);
void cymbal_set_late_decay(double decay, cymbal_parameters_t* parameters);
void cymbal_set_cutoff(double cutoff, cymbal_parameters_t* parameters);
void cymbal_set_resonance(double resonance, cymbal_parameters_t* parameters);
void cymbal_set_decay_threshold(double threshold, cymbal_parameters_t* parameters);
void cymbal_set_noise(metal_noise_type_t noise, cymbal_parameters_t* parameters);
void cymbal_set_swoosh(double swoosh, cymbal_parameters_t* parameters);

void cymbal_init(double sample_rate, cymbal_t* cymbal);
void cymbal_process(double* out, int number_of_frames, const cymbal_parameters_t* parameters, cymbal_t* cymbal);
void cymbal_note_on(const cymbal_parameters_t* parameters, cymbal_t* cymbal);
void cymbal_choke(const cymbal_parameters_t* parameters, cymbal_t* hihat);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* CYMBAL_H */
