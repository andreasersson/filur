/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NOISE_H
#define NOISE_H

typedef struct {
  int noise;

} noise_t;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void noise_init(noise_t* noise);
void noise_process(double* out, int number_of_frames, noise_t* noise);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* NOISE_H */
