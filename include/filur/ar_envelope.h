/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef AR_ENVELOPE_H
#define AR_ENVELOPE_H

#include <stdint.h>

typedef enum {
  AR_SILENT_FALSE = 0,
  AR_SILENT_TRUE,
} ar_silent_t;

typedef enum AR_ENVELOPE_STATE {
  AR_ENVELOPE_STATE_ATTACK,
  AR_ENVELOPE_STATE_EARLY_DECAY,
  AR_ENVELOPE_STATE_LATE_DECAY,
  AR_ENVELOPE_STATE_SILENT
} ar_envelope_state_t;

typedef struct {
  double decay;
  double attack;

  double sample_rate;
  ar_silent_t silent;
  ar_envelope_state_t state;
  double early_decay;
  double late_decay;
  double value;
  double denormal;
  double late_decay_threshold;
} ar_envelope_t;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void ar_envelope_init(double sample_rate, ar_envelope_t* envelope);
void ar_envelope_set_decay(double decay, ar_envelope_t* envelope);
void ar_envelope_set_attack(double attack, ar_envelope_t* envelope);
void ar_envelope_set_late_decay_threshold(double threshold, ar_envelope_t* envelope);
void ar_envelope_start(ar_envelope_t* envelope);
void ar_envelope_process(double* out, int number_of_frames, ar_envelope_t* envelope);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* AR_ENVELOPE_H */
