/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MIXER_H
#define MIXER_H

#include "znf_value.h"

typedef struct {
  double volume;
  double pan;
} mixer_parameters_t;

typedef struct {
  double volume;
  double pan;

  znf_value_t gain;
  znf_value_t left_gain;
  znf_value_t right_gain;
} mixer_t;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void mixer_init_parameters(mixer_parameters_t* parameters);
void mixer_set_volume(double volume, mixer_parameters_t* parameters);
void mixer_set_pan(double pan, mixer_parameters_t* parameters);

void mixer_init(mixer_t* mixer);
void mixer_process(const double* in,
                   double* out_left,
                   double* out_right,
                   int number_of_frames,
                   const mixer_parameters_t* parameters,
                   mixer_t* mixer);
void mixer_process_stereo(const double* in_left,
                          const double* in_right,
                          double* out_left,
                          double* out_right,
                          int number_of_frames,
                          const mixer_parameters_t* parameters,
                          mixer_t* mixer);
void mixer_update_parameters(const mixer_parameters_t* parameters, mixer_t* mixer);
void mixer_reset(mixer_t* mixer);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* MIXER_H */
