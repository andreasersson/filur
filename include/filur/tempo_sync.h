/*
 * Copyright 2018-2024 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TEMPO_SYNC_H
#define TEMPO_SYNC_H

typedef enum TEMPO_SYNC {
  TEMPO_SYNC_FREE_RUNNING = 0,
  TEMPO_SYNC_16TH,
  TEMPO_SYNC_8TH,
  TEMPO_SYNC_4TH,
  TEMPO_SYNC_2TH,
  TEMPO_SYNC_1,
  TEMPO_SYNC_2,
  TEMPO_SYNC_4,
  // NOTE: ADD NEW TEMPO SYNC BEFORE TEMPO_SYNC_NUM_TEMPO_SYNCS
  TEMPO_SYNC_NUM_TEMPO_SYNCS
} tempo_sync_t;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

double tempo_sync_to_rate(const tempo_sync_t tempo_sync);
tempo_sync_t value_to_tempo_sync(double value);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* TEMPO_SYNC_H */
