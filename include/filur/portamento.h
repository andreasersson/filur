/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PORTAMENTO_H
#define PORTAMENTO_H

typedef struct {
  int target_pitch;
  int current_pitch;

  double pitch;
  double delta_pitch;
  double time;
  double sample_rate;
} portamento_t;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void portamento_init(double sample_rate, portamento_t* portamento);
void portamento_set_time(double time, portamento_t* portamento);
void portamento_set_pitch(int pitch, portamento_t* portamento);
void portamento_set_target_pitch(int target_pitch, portamento_t* portamento);
void portamento_process(portamento_t* portamento);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* PORTAMENTO_H */
