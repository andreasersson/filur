/*
 * Copyright 2021 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef EQ_PARAMETERS_H
#define EQ_PARAMETERS_H

#include <stdbool.h>

typedef struct {
  double cutoff;
  double resonance;
  double gain;
  bool changed;
} eq_shelf_parameters_t;

typedef struct {
  eq_shelf_parameters_t low_shelf;
  eq_shelf_parameters_t high_shelf;
} eq_parameters_t;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void eq_parameters_init(eq_parameters_t* parameters);
void eq_low_shelf_set_cutoff(double cutoff, eq_parameters_t* parameters);
void eq_low_shelf_set_resonance(double resonance, eq_parameters_t* parameters);
void eq_low_shelf_set_gain(double gain, eq_parameters_t* parameters);
void eq_high_shelf_set_cutoff(double cutoff, eq_parameters_t* parameters);
void eq_high_shelf_set_resonance(double resonance, eq_parameters_t* parameters);
void eq_high_shelf_set_gain(double gain, eq_parameters_t* parameters);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* EQ_PARAMETERS_H */
