/*
 * Copyright 2018-2024 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef EP_PARAMETERS_H
#define EP_PARAMETERS_H

#include <filur/ep/pickup.h>
#include <filur/ep/tuning.h>

#include <stdint.h>

typedef struct {
  double volume;

  double tonebar_velocity_curve;
  double tonebar_velocity_depth;
  double tonebar_decay;
  double tonebar_release;

  double clang_velocity_curve;
  double clang_velocity_depth;
  double clang_volume;
  double clang_decay;

  double pickup_timbre;
  double pickup_volume;

  double soft_pedal_damping;
  const key_tuning_t* tuning;
} ep_parameters_t;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void ep_parameters_init(ep_parameters_t* parameters);

void ep_set_volume(double volume, ep_parameters_t* parameters);
void ep_set_velocity_curve(double velocity_curve, ep_parameters_t* parameters);
void ep_set_velocity_depth(double velocity_depth, ep_parameters_t* parameters);
void ep_set_decay(double decay, ep_parameters_t* parameters);
void ep_set_release(double release, ep_parameters_t* parameters);
void ep_set_clang_volume(double volume, ep_parameters_t* parameters);
void ep_set_clang_velocity_curve(double velocity_curve, ep_parameters_t* parameters);
void ep_set_clang_velocity_depth(double velocity_depth, ep_parameters_t* parameters);
void ep_set_clang_decay(double decay, ep_parameters_t* parameters);
void ep_set_pickup_timbre(double timbre, ep_parameters_t* parameters);
void ep_set_pickup_volume(double volume, ep_parameters_t* parameters);
void ep_set_soft_pedal_damping(double soft_pedal_damping, ep_parameters_t* parameters);
void ep_set_key_tuning(const key_tuning_t* tuning, ep_parameters_t* parameters);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* EP_PARAMETERS_H */
