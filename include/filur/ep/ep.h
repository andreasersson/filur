/*
 * Copyright 2018-2024 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef EP_H
#define EP_H

#include "ep_parameters.h"
#include "voice.h"

#include <filur/znf_value.h>

#include <stddef.h>

enum {
  ep_voice_headroom = 8,
  ep_num_voices = 32 + ep_voice_headroom
};

typedef enum {
  pedal_off = 0,
  pedal_on
} pedal_t;

typedef struct {
  pedal_t sustain_pedal;
  pedal_t sostenuto_pedal;
  pedal_t soft_pedal;

  voice_t voices[ep_num_voices];
  znf_log_value_t volume;
} ep_t;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void ep_init(double sample_rate, ep_t* ep);
void ep_note_on(int pitch, double velocity, const ep_parameters_t* parameters, ep_t* ep);
void ep_note_off(int pitch, const ep_parameters_t* parameters, ep_t* ep);
void ep_all_notes_off(const ep_parameters_t* parameters, ep_t* ep);
void ep_sustain_pedal(pedal_t sustain_pedal, const ep_parameters_t* parameters, ep_t* ep);
void ep_sostenuto_pedal(pedal_t sostenuto_pedal, const ep_parameters_t* parameters, ep_t* ep);
void ep_soft_pedal(pedal_t soft_pedal, const ep_parameters_t* parameters, ep_t* ep);

void ep_process(double *out,
                int number_of_frames,
                const ep_parameters_t* parameters,
                ep_t* ep);
size_t ep_num_active_voices(ep_t* ep);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* EP_H */
