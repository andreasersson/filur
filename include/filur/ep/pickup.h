/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PICKUP_H
#define PICKUP_H

#include <filur/biquad.h>

typedef struct {
  double alpha;
  double in_gain;
  double out_gain;
  double offset;
} pickup_parameters_t;

typedef struct {
  biquad_coeffs_t biquad_coeffs;
  biquad_state_t biquad_state;
} pickup_state_t;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void pickup_init_parameters(pickup_parameters_t* parameters);
void pickup_set_timbre(double value, pickup_parameters_t* parameters);
void pickup_set_volume(double value, pickup_parameters_t* parameters);
void pickup_set(double timbre, double volume, pickup_parameters_t* parameters);

void pickup_init_state(double sample_rate, pickup_state_t* state);
void pickup_set_cutoff(double cutoff, pickup_state_t* state);
void pickup_process(const double* in,
                    double* out,
                    int number_of_frames,
                    const pickup_parameters_t* parameters,
                    pickup_state_t* state);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* PICKUP_H */
