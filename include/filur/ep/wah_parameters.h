/*
 * Copyright 2018-2019 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WAH_PARAMETERS_H
#define WAH_PARAMETERS_H

#include <filur/lfo.h>
#include <filur/tempo_sync.h>

#include <stdint.h>

typedef enum WAH_ENABLED {
  WAH_OFF = 0,
  WAH_ON,
} wah_enable_t;

typedef enum WAH_LFO_ENABLED {
  WAH_LFO_OFF = 0,
  WAH_LFO_ON,
} wah_lfo_enable_t;

typedef struct {
  wah_enable_t enabled;
  wah_lfo_enable_t lfo_enabled;
  double low_cutoff;
  double high_cutoff;
  double cutoff;
  double resonance;
  double speed;
  double phase;
  tempo_sync_t tempo_sync;
  double tempo;

  lfo_parameters_t lfo;
} wah_parameters_t;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void wah_parameters_init(double sample_rate, wah_parameters_t* parameters);
void wah_set_enabled(wah_enable_t enabled, wah_parameters_t* parameters);
void wah_set_low_cutoff(double low_cutoff, wah_parameters_t* parameters);
void wah_set_high_cutoff(double high_cutoff, wah_parameters_t* parameters);
void wah_set_cutoff(double cutoff, wah_parameters_t* parameters);
void wah_set_resonance(double resonance, wah_parameters_t* parameters);
void wah_set_lfo_enabled(wah_lfo_enable_t enabled,
                         wah_parameters_t* parameters);
void wah_set_speed(double speed, wah_parameters_t* parameters);
void wah_set_phase(double phase, wah_parameters_t* parameters);
void wah_set_tempo_sync(tempo_sync_t tempo_sync, wah_parameters_t* parameters);
void wah_set_tempo(double tempo, wah_parameters_t* parameters);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* WAH_PARAMETERS_H */
