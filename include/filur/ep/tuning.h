/*
 * Copyright 2024 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TUNING_H
#define TUNING_H

#define TUNING_MAX_NUMBER_OF_KEYS 128u

typedef struct {
  double pickup_timbre;
  double pickup_volume;
  double clang_volume;
  double gain;
} key_tuning_t;

typedef key_tuning_t tuning_t[TUNING_MAX_NUMBER_OF_KEYS];

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void key_tuning_init(key_tuning_t* key_tuning);
void tuning_init(tuning_t tunings);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* TUNING_H */
