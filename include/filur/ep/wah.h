/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WAH_H
#define WAH_H

#include "wah_parameters.h"

#include "filur/biquad.h"
#include "filur/lfo.h"
#include "filur/znf_value.h"

typedef struct {
  double current_cutoff;

  znf_value_t cutoff;
  znf_value_t resonance;

  biquad_coeffs_t biquad_coeffs;
  biquad_state_t biquad_state;
  lfo_state_t lfo;
} wah_t;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void wah_init(double sample_rate, wah_t* wah);
void wah_update_parameters(const wah_parameters_t* parameters, wah_t* wah);
void wah_restart_lfo(const wah_parameters_t* parameters, wah_t* wah);
void wah_process(const double* in,
                 double* out,
                 int number_of_frames,
                 wah_parameters_t* parameters,
                 wah_t* wah);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* WAH_H */
