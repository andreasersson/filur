/*
 * Copyright 2018-2019 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TREMOLO_PARAMETERS_H
#define TREMOLO_PARAMETERS_H

#include <filur/lfo.h>
#include <filur/tempo_sync.h>

#include <stdint.h>

typedef enum TREMOLO_ENABLED {
  TREMOLO_OFF = 0,
  TREMOLO_ON,
} tremolo_enable_t;

typedef struct {
  tremolo_enable_t enabled;
  double depth;
  double speed;
  tempo_sync_t tempo_sync;
  double tempo;
  lfo_parameters_t lfo;
} tremolo_parameters_t;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void tremolo_parameters_init(double sample_rate, tremolo_parameters_t* parameters);
void tremolo_set_enabled(tremolo_enable_t enabled, tremolo_parameters_t* parameters);
void tremolo_set_depth(double depth, tremolo_parameters_t* parameters);
void tremolo_set_speed(double speed, tremolo_parameters_t* parameters);
void tremolo_set_tempo_sync(tempo_sync_t tempo_sync, tremolo_parameters_t* parameters);
void tremolo_set_tempo(double tempo, tremolo_parameters_t* parameters);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* TREMOLO_PARAMETERS_H */
