/*
 * Copyright 2021 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef EQ_H
#define EQ_H

#include "eq_parameters.h"

#include "filur/biquad.h"

typedef struct {
  biquad_coeffs_t low_shelf_coeffs;
  biquad_coeffs_t high_shelf_coeffs;
  biquad_state_t low_shelf_state;
  biquad_state_t high_shelf_state;
} eq_t;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void eq_init(double sample_rate, eq_t* eq);
void eq_process(const double* in,
                double* out,
                int number_of_frames,
                eq_parameters_t* parameters,
                eq_t* eq);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* EQ_H */
