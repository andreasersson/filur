/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TREMOLO_H
#define TREMOLO_H

#include "tremolo_parameters.h"

#include <filur/lfo.h>
#include <filur/znf_value.h>

typedef struct {
  lfo_state_t lfo;
  znf_value_t depth;
} tremolo_t;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void tremolo_init(tremolo_t* tremolo);
void tremolo_update_parameters(const tremolo_parameters_t* parameters,
                               tremolo_t* tremolo);
void tremolo_restart_lfo(tremolo_t* tremolo);

void tremolo_process(const double* in,
                     double* out,
                     int number_of_frames,
                     const tremolo_parameters_t* parameters,
                     tremolo_t* tremolo);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* TREMOLO_H */
