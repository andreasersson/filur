/*
 * Copyright 2018-2024 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VOICE_H
#define VOICE_H

#define FILUR_EP_MAX_BUFFER_SIZE 1024

#include "ep_parameters.h"
#include "pickup.h"
#include "sine_osc.h"
#include "tuning.h"

#include <filur/adsr.h>

typedef enum VOICE_STATE {
  VOICE_STATE_FREE,
  VOICE_STATE_ACTIVE,
  VOICE_STATE_ACTIVE_SOSTENUTO,
  VOICE_STATE_SUSTAIN,
  VOICE_STATE_SOSTENUTO,
  VOICE_STATE_RELEASE
} voice_state_t;

typedef struct {
  voice_state_t state;
  int pitch;
  double gain;

  sine_osc_t tonebar;
  sine_osc_t clang1;
  sine_osc_t clang2;

  adsr_state_t tonebar_adsr;
  adsr_parameters_t tonebar_adsr_parameters;
  adsr_state_t clang_adsr;
  adsr_parameters_t clang_adsr_parameters;

  pickup_state_t pickup;
  pickup_parameters_t pickup_parameters;

} voice_t;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void voice_init(double sample_rate, voice_t* voice);
void voice_note_on(int pitch,
                   double velocity,
                   const key_tuning_t* tuning,
                   const ep_parameters_t* parameters,
                   voice_t* voice);
void voice_note_off(const ep_parameters_t* parameters, voice_t* voice);
void voice_update_parameters(const ep_parameters_t* parameters, voice_t* voice);
void voice_sustain(voice_t* voice);
void voice_process(double *out,
                   int number_of_frames,
                   const ep_parameters_t* parameters,
                   voice_t* voice);

double voice_silent(voice_t* voice);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* VOICE_H */
