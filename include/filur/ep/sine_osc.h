/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SINE_OSC_H
#define SINE_OSC_H

typedef struct {
  double sample_time;
  double alpha;
  double delta_alpha;
  double amplitude;

} sine_osc_t;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void sine_osc_init(double sample_rate, sine_osc_t* sine_osc);
void sine_osc_set_amplitude(double amplitude, sine_osc_t* sine_osc);
void sine_osc_set_frequency(double frequency, sine_osc_t* sine_osc);
void sine_osc_set_phase(double phase, sine_osc_t* sine_osc);
void sine_osc_process(double* out, int number_of_frames, sine_osc_t* sine_osc);
void sine_osc_process_add(double* out, int number_of_frames, sine_osc_t* sine_osc);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* SINE_OSC_H */
