/*
 * Copyright 2018-2024 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PF_PARAMETERS_H
#define PF_PARAMETERS_H

#include <filur/adsr.h>
#include <filur/biquad.h>
#include <filur/lfo.h>
#include <filur/tempo_sync.h>

#include <stdint.h>

typedef enum {
  PF_OSC_WAVEFORM_SINE = 0,
  PF_OSC_WAVEFORM_SAW,
  PF_OSC_WAVEFORM_PULSE
} pf_osc_waveform_t;

typedef struct {
  pf_osc_waveform_t waveform;
  double pulse_width;
  double pwm_depth;
  double vibrato_depth;
  int coarse;
  int fine_tune;
} pf_osc_parameters_t;

typedef enum {
  PF_FILTER_TYPE_LOWPASS = 0,
  PF_FILTER_TYPE_HIGHPASS,
  PF_FILTER_TYPE_BANDPASS
} pf_filter_type_t;

typedef enum {
  PF_KBF_DISABLED = 0,
  PF_KBF_ENABLED
} pf_kbf_enabled_t;

typedef struct {
  pf_filter_type_t type;
  pf_kbf_enabled_t keyboard_follow_enabled;
  double keyboard_follow_factor;
  double cutoff;
  double resonance;
  double env_depth;
  double lfo_depth;
  double velocity_depth;
  double mod_wheel_depth;
} pf_filter_parameters_t;

typedef enum Waveform {
  PF_LFO_WAVEFORM_SINE = 0,
  PF_LFO_WAVEFORM_TRIANGLE,
  PF_LFO_WAVEFORM_SAW,
} pf_lfo_waveform_t;

typedef enum {
  PF_LFO_RESTART_FALSE = 0,
  PF_LFO_RESTART_TRUE
} pf_lfo_restart_t;
typedef struct {
  pf_lfo_waveform_t waveform;
  double speed;
  double phase;
  tempo_sync_t tempo_sync;
  lfo_parameters_t lfo;
  pf_lfo_restart_t restart;
} pf_lfo_parameters_t;

typedef enum {
  PF_PORTAMENTO_DISABLED = 0,
  PF_PORTAMENTO_ENABLED
} pf_portamento_enabled_t;

typedef struct {
  pf_portamento_enabled_t enabled;
  double time;
} pf_portamento_parameters_t;

typedef enum {
  PF_MIXER_CH_OFF = 0,
  PF_MIXER_CH_ON,
  PF_MIXER_CH_BYPASS,
} pf_mixer_ch_state_t;

typedef struct {
  pf_mixer_ch_state_t enabled;
  double volume;
} pf_mixer_ch_parameters_t;

typedef struct {
  pf_mixer_ch_parameters_t ch1;
  pf_mixer_ch_parameters_t ch2;
  pf_mixer_ch_parameters_t ch3;
} pf_mixer_parameters_t;

typedef enum {
  PF_VOICE_MODE_POLY = 0,
  PF_VOICE_MODE_MONO,
  PF_VOICE_MODE_MONOLEGATO,
} pf_voice_mode_t;

typedef struct {

  pf_osc_parameters_t osc1;
  pf_osc_parameters_t osc2;
  pf_filter_parameters_t filter;
  adsr_parameters_t adsr1;
  adsr_parameters_t adsr2;
  pf_lfo_parameters_t osc_lfo;
  pf_lfo_parameters_t filter_lfo;
  pf_portamento_parameters_t portamento;
  pf_mixer_parameters_t mixer;

  double volume;
  double velocity_depth;
  double velocity_curve;
  pf_voice_mode_t voice_mode;
  double pitch_bend;
  double pitch_bend_range;
  double mod_wheel;
  double tempo;
} pf_parameters_t;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void pf_osc_parameters_init(pf_osc_parameters_t* parameters);
void pf_osc_parameters_set_waveform(pf_osc_waveform_t waveform, pf_osc_parameters_t* parameters);
void pf_osc_parameters_set_coarse(int coarse, pf_osc_parameters_t* parameters);
void pf_osc_parameters_set_fine_tune(int fine_tune, pf_osc_parameters_t* parameters);
void pf_osc_parameters_set_pulse_width(double pulse_width, pf_osc_parameters_t* parameters);
void pf_osc_parameters_set_pwm_depth(double depth, pf_osc_parameters_t* parameters);
void pf_osc_parameters_set_vibrato_depth(double depth, pf_osc_parameters_t* parameters);

void pf_filter_parameters_init(double keyboard_follow_factor, pf_filter_parameters_t* parameters);
void pf_filter_parameters_set_type(pf_filter_type_t type, pf_filter_parameters_t* parameters);
void pf_filter_parameters_set_kbf_enabled(pf_kbf_enabled_t enabled, pf_filter_parameters_t* parameters);
void pf_filter_parameters_set_cutoff(double cutoff, pf_filter_parameters_t* parameters);
void pf_filter_parameters_set_resonance(double resonance, pf_filter_parameters_t* parameters);
void pf_filter_parameters_set_env_depth(double depth, pf_filter_parameters_t* parameters);
void pf_filter_parameters_set_lfo_depth(double depth, pf_filter_parameters_t* parameters);
void pf_filter_parameters_set_velocity_depth(double depth, pf_filter_parameters_t* parameters);
void pf_filter_parameters_set_mod_wheel_depth(double depth, pf_filter_parameters_t* parameters);

void pf_lfo_parameters_init(double sample_rate, pf_lfo_parameters_t* parameters);
void pf_lfo_parameters_set_waveform(pf_lfo_waveform_t waveform, pf_lfo_parameters_t* parameters);
void pf_lfo_parameters_set_speed(double speed, pf_lfo_parameters_t* parameters);
void pf_lfo_parameters_set_phase(double phase, pf_lfo_parameters_t* parameters);
void pf_lfo_parameters_set_tempo_sync(tempo_sync_t tempo_sync, pf_lfo_parameters_t* parameters);
void pf_lfo_parameters_set_restart(pf_lfo_restart_t restart, pf_lfo_parameters_t* parameters);

void pf_portamento_parameters_init(pf_portamento_parameters_t* parameters);

void pf_mixer_ch_parameters_init(pf_mixer_ch_parameters_t* parameters);
void pf_mixer_ch_parameters_set_volume(double volume, pf_mixer_ch_parameters_t* parameters);
void pf_mixer_ch_parameters_set_enabled(pf_mixer_ch_state_t state, pf_mixer_ch_parameters_t* parameters);

void pf_mixer_parameters_init(pf_mixer_parameters_t* parameters);

void pf_parameters_init(double sample_rate, double keyboard_follow_factor, pf_parameters_t* parameters);
void pf_parameters_set_tempo(double tempo, pf_parameters_t* parameters);
void pf_parameters_set_volume(double volume, pf_parameters_t* parameters);

void pf_parameters_set_velocity_depth(double depth, pf_parameters_t* parameters);
void pf_parameters_set_velocity_curve(double curve, pf_parameters_t* parameters);
void pf_parameters_set_voice_mode(pf_voice_mode_t voice_mode, pf_parameters_t* parameters);
void pf_parameters_set_pitch_bend(double bend, pf_parameters_t* parameters);
void pf_parameters_set_pitch_bend_range(double range, pf_parameters_t* parameters);
void pf_parameters_set_mod_wheel(double mod_wheel, pf_parameters_t* parameters);

void pf_parameters_update_lfo_speed(double tempo, pf_lfo_parameters_t* lfo_parameters);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* PF_PARAMETERS_H */
