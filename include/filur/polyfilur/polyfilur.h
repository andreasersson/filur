/*
 * Copyright 2018-2020 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef POLYFILUR_H
#define POLYFILUR_H

#include "pf_voice.h"

#include <filur/bilin.h>
#include <filur/int_list.h>
#include <filur/lfo.h>
#include <filur/noise.h>
#include <filur/znf_value.h>

enum {
  POLYFILUR_NUM_VOICES = 8
};

typedef struct {
  znf_log_value_t volume;
  pf_voice_t voices[POLYFILUR_NUM_VOICES];
  int_list_t note_list;

  pf_voice_mode_t voice_mode;

  lfo_state_t osc_lfo;
  lfo_state_t filter_lfo;

  noise_t noise;
  bilin_hp2_t noise_hp;
  bilin_lp_t noise_lp;
} polyfilur_t;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void polyfilur_init(double sample_rate, polyfilur_t* polyfilur);

void polyfilur_note_on(int pitch, double velocity,
                       const pf_parameters_t* parameters,
                       polyfilur_t* polyfilur);
void polyfilur_note_off(int pitch, const pf_parameters_t* parameters,
                        polyfilur_t* polyfilur);
void polyfilur_all_notes_off(const pf_parameters_t* parameters,
                             polyfilur_t* polyfilur);
void polyfilur_process(double* left, double* right,
                       int number_of_frames,
                       const pf_parameters_t* parameters,
                       polyfilur_t* polyfilur);
void polyfilur_update_parameters(const pf_parameters_t* parameters,
                                 polyfilur_t* polyfilur);
size_t polyfilur_num_active_voices(const polyfilur_t* polyfilur);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* POLYFILUR_H */

