/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PF_VOICE_H
#define PF_VOICE_H

#define POLYFILUR_MAX_BUFFER_SIZE 1024

#include "pf_parameters.h"

#include <filur/adsr.h>
#include <filur/biquad.h>
#include <filur/blit_osc.h>
#include <filur/lfo.h>
#include <filur/portamento.h>
#include <filur/znf_value.h>

typedef struct {
  znf_value_t cutoff;
  znf_value_t resonance;

  biquad_coeffs_t biquad_coeffs;
  biquad_state_t biquad_state0;
  biquad_state_t biquad_state1;

  pf_filter_type_t type;
  double current_cutoff;
  double velocity;
} pf_filter_t;

typedef struct {
  blit_t blit;
  int pitch;
} pf_osc_t;

typedef struct {
  znf_log_value_t volume;
} pf_mixer_channel_t;

typedef struct {
  pf_mixer_channel_t ch1;
  pf_mixer_channel_t ch2;
  pf_mixer_channel_t ch3;
} pf_mixer_t;

typedef enum {
  PF_VOICE_STATE_FREE,
  PF_VOICE_STATE_ACTIVE,
  PF_VOICE_STATE_RELEASE
} pf_voice_state_t;

typedef enum {
  PF_VOICE_SILENT_FALSE = 0,
  PF_VOICE_SILENT_TRUE
} pf_voice_silent_t;

typedef struct {
  uint64_t voice_id;
  pf_voice_state_t state;
  pf_voice_silent_t silent;
  int pitch;
  double velocity_gain;

  pf_osc_t osc1;
  pf_osc_t osc2;
  adsr_state_t adsr1;
  adsr_state_t adsr2;
  pf_filter_t filter;
  portamento_t portamento;
  pf_mixer_t mixer;
} pf_voice_t;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void pf_filter_init(double sample_rate, pf_filter_t* filter);
void pf_filter_set_velocity(double velocity,
                            const pf_filter_parameters_t* parameters,
                            pf_filter_t* filter);
void pf_filter_update_parameters(const pf_filter_parameters_t* parameters,
                                 pf_filter_t* filter);
void pf_filter_process_modulation(double* out, const int* master_pitch,
                                  const double* env, const double* lfo,
                                  int number_of_frames, double mod_wheel,
                                  const pf_filter_parameters_t* parameters,
                                  pf_filter_t* filter);
void pf_filter_process(const double* in,
                       const double* mod,
                       double* out,
                       int number_of_frames,
                       const pf_filter_parameters_t* parameters,
                       pf_filter_t* filter);

void pf_osc_init(double sample_rate, pf_osc_t* osc);
void pf_osc_restart(pf_osc_t* osc);
void pf_osc_process(double *out,
                    const int *master_pitch,
                    const double *lfo,
                    int number_of_frames,
                    const pf_osc_parameters_t* parameters,
                    pf_osc_t* osc);

void pf_mixer_channle_init(pf_mixer_channel_t* mixer_ch);
void pf_mixer_channle_update_parameters(
    const pf_mixer_ch_parameters_t* parameters, pf_mixer_channel_t* mixer_ch);
void pf_mixer_channle_process(const double* in,
                              double* master,
                              double* bypass,
                              int number_of_frames,
                              const pf_mixer_ch_parameters_t* parameters,
                              pf_mixer_channel_t* mixer_ch);

void pf_mixer_init(pf_mixer_t* mixer);
void pf_mixer_update_parameters(const pf_mixer_parameters_t* parameters,
                                pf_mixer_t* mixer);
void pf_mixer_process(const double* ch1,
                      const double* ch2,
                      const double* ch3,
                      double* master,
                      double* bypass,
                      int number_of_frames,
                      const pf_mixer_parameters_t* parameters,
                      pf_mixer_t* mixer);

void pf_voice_init(double sample_rate, pf_voice_t* voice);
void pf_voice_note_on(int pitch, double velocity,
                      const pf_parameters_t* parameters,
                      pf_voice_t* voice);
void pf_voice_slide_note(int start_pitch, int target_pitch,
                         const pf_parameters_t* parameters,
                         pf_voice_t* voice);
void pf_voice_note_off(const pf_parameters_t* parameters, pf_voice_t* voice);
void pf_voice_process(const double* noise, const double* osc_lfo,
                      const double* filter_lfo, double* out,
                      int number_of_frames,
                      const pf_parameters_t* parameters,
                      pf_voice_t* voice);
void pf_voice_update_parameters(const pf_parameters_t* parameters,
                                pf_voice_t* voice);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* PF_VOICE_H */
