/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ZNF_VALUE_H
#define ZNF_VALUE_H

#include <stdbool.h>

typedef struct {
  double value;
  double target;
  double step;
  double threshold;
  double delta;
  bool equal;
} znf_value_t;

typedef struct {
  double value;
  double target;
  double step;
  double threshold;
  double multiplier;
  double threshold_max;
  double threshold_min;
  bool equal;
} znf_log_value_t;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void znf_value_init(double threshold, double step, double value, znf_value_t* znf_value);
void znf_value_set(double value, znf_value_t* znf_value);
void znf_value_reset(znf_value_t* znf_value);
void znf_value_update(znf_value_t* znf_value);
bool znf_value_equal(znf_value_t* znf_value);

void znf_log_value_init(double threshold, double step, double value, znf_log_value_t* znf_value);
void znf_log_value_set(double value, znf_log_value_t* znf_value);
void znf_log_value_reset(znf_log_value_t* znf_value);
void znf_log_value_update(znf_log_value_t* znf_value);
bool znf_log_value_equal(znf_log_value_t* znf_value);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* ZNF_VALUE_H */
