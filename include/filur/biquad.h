/*
 * Copyright 2018-2021 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef BIQUAD_H
#define BIQUAD_H

typedef struct {
  double a0;
  double a1;
  double a2;
  double b0;
  double b1;
  double b2;
  double two_pi_ts; /* 2*PI/sample_rate */
} biquad_coeffs_t;

typedef struct {
  double x1;
  double x2;
  double y1;
  double y2;
  double denormal;
} biquad_state_t;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void biquad_coeffs_init(double sample_rate, biquad_coeffs_t* biquad_coeffs);
void biquad_coeffs_low_pass(double frequency, double q, biquad_coeffs_t* biquad_coeffs);
void biquad_coeffs_high_pass(double frequency, double q, double gain, biquad_coeffs_t* biquad_coeffs);
void biquad_coeffs_band_pass(double frequency, double q, biquad_coeffs_t* biquad_coeffs);
void biquad_coeffs_low_shelf(double frequency, double q, double gain, biquad_coeffs_t* biquad_coeffs);
void biquad_coeffs_high_shelf(double frequency, double q, double gain, biquad_coeffs_t* biquad_coeffs);

void biquad_state_init(biquad_state_t* biquad_state);
void biquad_state_reset(biquad_state_t* biquad_state);

void biquad_process(const double* src,
                    double* dst,
                    int number_of_frames,
                    biquad_state_t* biquad_state,
                    const biquad_coeffs_t* biquad_coeffs);

static inline double biquad_process_sample(double src,
                                           biquad_state_t* biquad_state,
                                           const biquad_coeffs_t* biquad_coeffs) {
  double dst = biquad_coeffs->b0 * src + biquad_coeffs->b1 * biquad_state->x1
      + biquad_coeffs->b2 * biquad_state->x2
      + biquad_coeffs->a1 * biquad_state->y1
      + biquad_coeffs->a2 * biquad_state->y2
      + biquad_state->denormal;

  biquad_state->x2 = biquad_state->x1;
  biquad_state->x1 = src;
  biquad_state->y2 = biquad_state->y1;
  biquad_state->y1 = dst;

  biquad_state->denormal = -biquad_state->denormal;

  return dst;
}

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* BIQUAD_H */
