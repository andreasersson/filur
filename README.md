C libraries used by 
[filurep][filurep],
[polyfilur][polyfilur] and 
[drumelidrum][drumelidrum]
VST3 plug-ins.

## How to build.
### Requirements
- [CMake][CMake] 3.13 or later.

### Dependencies
- [googletest][googletest]. Only used if BUILD_TESTS is enabled.

*The dependencies will be automatically cloned and built using the [CMake FetchContent module][FetchContent]*  

Download the source or clone the git repository.

    git clone https://gitlab.com/andreasersson/filur.git

### Linux/macOS with Makefiles
    cmake -DCMAKE_BUILD_TYPE=Release -S filur -B build-filur
    cmake --build build-filur

### macOS with Xcode
    cmake -GXcode -S filur -B build-filur
    cmake --build build-filur --config Release

## License
[![GPLv3](https://www.gnu.org/graphics/gplv3-with-text-136x68.png "GNU General Public License")](https://www.gnu.org/licenses/gpl.html)

    filur is free software: you can redistribute it and/or modify   
    it under the terms of the GNU General Public License as published by   
    the Free Software Foundation, either version 3 of the License, or   
    (at your option) any later version.   

    filur is distributed in the hope that it will be useful,   
    but WITHOUT ANY WARRANTY; without even the implied warranty of   
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the   
    GNU General Public License for more details.

[CMake]: https://cmake.org/
[FetchContent]: https://cmake.org/cmake/help/latest/module/FetchContent.html
[googletest]: https://github.com/abseil/googletest
[filurep]: https://gitlab.com/andreasersson/filurep-vst
[polyfilur]: https://gitlab.com/andreasersson/polyfilur-vst
[drumelidrum]: https://gitlab.com/andreasersson/drumelidrum-vst
