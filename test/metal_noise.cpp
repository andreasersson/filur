/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <gtest/gtest.h>

#include "../src/metal_noise.c"

TEST(metal_noise, value_to_type) {
  EXPECT_EQ(METAL_NOISE_TYPE_1, metal_noise_type(METAL_NOISE_TYPE_1));
  EXPECT_EQ(METAL_NOISE_TYPE_2, metal_noise_type(METAL_NOISE_TYPE_2));
  EXPECT_EQ(METAL_NOISE_TYPE_3, metal_noise_type(METAL_NOISE_TYPE_3));
  EXPECT_EQ(METAL_NOISE_TYPE_4, metal_noise_type(METAL_NOISE_TYPE_4));
}

TEST(metal_noise, loop_all) {
  for (int noise_type = METAL_NOISE_TYPE_1; noise_type < METAL_NOISE_NUM_TYPES; ++noise_type) {
    metal_noise_type(noise_type);
  }
}
