/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <gtest/gtest.h>

#include <filur/int_list.h>
#include "../src/int_list.c"

#include <list>

static int_list_pool_element_t* get_pool_element(int_list_element_t* list_element, size_t size,
                                                 int_list_pool_element_t* pool) {
  int_list_pool_element_t* pool_element = NULL;
  for (size_t index = 0; (index < size) && (NULL == pool_element); ++index) {
    if (list_element == &pool[index].element) {
      pool_element = &pool[index];
    }
  }

  EXPECT_NE((int_list_pool_element_t*)NULL, pool_element);

  return pool_element;
}

TEST(int_list_test, pool_init) {
  const size_t size = 16;
  int_list_pool_element_t pool[size];
  int_list_pool_init(size, pool);

  for (size_t index = 0; index < size; ++index) {
    EXPECT_EQ(1, pool[index].free);
    EXPECT_EQ(0, pool[index].element.value);
    EXPECT_EQ(NULL, pool[index].element.previous);
    EXPECT_EQ(NULL, pool[index].element.next);
  }
}

TEST(int_list_test, pool_find_free) {
  const size_t size = 16;
  int_list_pool_element_t pool[size];
  int_list_pool_init(size, pool);

  for (size_t index = 0; index < size; ++index) {
    int_list_element_t* element = int_list_pool_find_free(size, pool);
    ASSERT_NE((int_list_element_t*)NULL, element);

    int_list_pool_element_t* pool_element = get_pool_element(element, size,
                                                             pool);
    ASSERT_NE((int_list_pool_element_t*)NULL, pool_element);
    EXPECT_EQ(0, pool_element->free);
  }

  int_list_element_t* element = int_list_pool_find_free(size, pool);
  EXPECT_EQ((int_list_element_t*)NULL, element);
}

TEST(int_list_test, pool_free) {
  const size_t size = 16;
  int_list_pool_element_t pool[size];
  int_list_pool_init(size, pool);

  int_list_element_t* element = int_list_pool_find_free(size, pool);
  EXPECT_NE((int_list_element_t*)NULL, element);

  int_list_pool_element_t* pool_element = get_pool_element(element, size,
                                                           pool);
  ASSERT_NE((int_list_pool_element_t*)NULL, pool_element);
  EXPECT_EQ(0, pool_element->free);

  element->value = 4711;
  element->previous = element;
  element->next = element;

  int_list_pool_free(element, size, pool);

  pool_element = get_pool_element(element, size, pool);
  EXPECT_EQ(1, pool_element->free);

  EXPECT_EQ(0, element->value);
  EXPECT_EQ(NULL, element->previous);
  EXPECT_EQ(NULL, element->next);
}

TEST(int_list_test, element_init) {
  int_list_element_t element;
  element.value = 4711;
  element.previous = &element;
  element.next = &element;

  int expected_value = 17;
  int_list_element_t* expected_previous = (int_list_element_t*) 1;
  int_list_element_t* expected_next = (int_list_element_t*) 2;
  int_list_element_init(expected_value, expected_previous, expected_next,
                        &element);

  EXPECT_EQ(expected_value, element.value);
  EXPECT_EQ(expected_previous, element.previous);
  EXPECT_EQ(expected_next, element.next);
}

TEST(int_list_test, init) {
  int_list_t list;
  int_list_init(&list);
  EXPECT_EQ((int_list_element_t*)NULL, list.back);
  EXPECT_EQ((int_list_element_t*)NULL, list.front);

  for (size_t index = 0; index < INT_LIST_POOL_SIZE; ++index) {
    EXPECT_EQ(1, list.pool[index].free);
    EXPECT_EQ(0, list.pool[index].element.value);
    EXPECT_EQ((int_list_element_t*)NULL, list.pool[index].element.previous);
    EXPECT_EQ((int_list_element_t*)NULL, list.pool[index].element.next);
  }
}

TEST(int_list_test, push_pop) {
  int_list_t list;

  int_list_init(&list);
  size_t size = int_list_size(&list);
  EXPECT_EQ(0, size);
  std::list<int> ref_list;
  for (int index = 0; index < INT_LIST_POOL_SIZE; ++index) {
    int value = index + 17;
    int_list_push(value, &list);
    ref_list.push_front(value);
    EXPECT_EQ(ref_list.size(), int_list_size(&list));
    EXPECT_EQ(ref_list.front(), int_list_top(&list));
  }

  for (size_t index = 0; index < INT_LIST_POOL_SIZE; ++index) {
    int_list_pop(&list);
    ref_list.pop_front();
    EXPECT_EQ(ref_list.size(), int_list_size(&list));
    if (!ref_list.empty()) {
      EXPECT_EQ(ref_list.front(), int_list_top(&list));
    }
  }
}

TEST(int_list_test, push_max_size) {
  int_list_t list;

  int_list_init(&list);
  size_t size = int_list_size(&list);
  EXPECT_EQ(0, size);

  std::list<int> ref_list;
  int value = -1;

  /* fill list*/
  for (int index = 0; index < INT_LIST_POOL_SIZE; ++index) {
    value = index;
    int_list_push(value, &list);
    ref_list.push_front(value);
    EXPECT_EQ(ref_list.size(), int_list_size(&list));
    EXPECT_EQ(ref_list.front(), int_list_top(&list));
  }

  EXPECT_EQ(INT_LIST_POOL_SIZE, int_list_size(&list));
  value += 1;
  int_list_push(value, &list);
  EXPECT_EQ(INT_LIST_POOL_SIZE, int_list_size(&list));
  EXPECT_EQ(value, int_list_top(&list));
  EXPECT_EQ(1, list.back->value);
}

TEST(int_list_test, remove) {
  int_list_t list;

  int_list_init(&list);
  size_t size = int_list_size(&list);
  EXPECT_EQ(0, size);

  std::list<int> ref_list;

  std::vector<int> values_to_remove = {4, 3, 2, 1, 5, 6, 7, 0};

  for (size_t index = 0; index < values_to_remove.size(); ++index) {
    int value = static_cast<int>(index);
    int_list_push(value, &list);
    ref_list.push_front(value);
    EXPECT_EQ(ref_list.size(), int_list_size(&list));
    EXPECT_EQ(ref_list.front(), int_list_top(&list));
  }

  for (auto &value_to_remove : values_to_remove) {
    int_list_remove(value_to_remove, &list);
    ref_list.remove(value_to_remove);
    EXPECT_EQ(ref_list.size(), int_list_size(&list));
    if (!ref_list.empty()) {
      EXPECT_EQ(ref_list.front(), int_list_top(&list));
    }
  }
}

TEST(int_list_test, clear) {
  int_list_t list;

  int_list_init(&list);
  size_t size = int_list_size(&list);
  EXPECT_EQ(0, size);
  size_t num_elements_to_push = 8;

  for (size_t index = 0; index < num_elements_to_push; ++index) {
    int value = static_cast<int>(index) + 17;
    int_list_push(value, &list);
  }
  EXPECT_EQ(num_elements_to_push, int_list_size(&list));

  int_list_clear(&list);

  EXPECT_EQ(0, int_list_size(&list));
}
