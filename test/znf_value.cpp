/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <gtest/gtest.h>

#include <filur/znf_value.h>
#include "../src/znf_value.c"

void test_znf_value_init(znf_value_t& znf_value,
                         double threshold = 0.001,
                         double step = 0.001,
                         double target = 0.0) {
  znf_value_init(threshold, step, target, &znf_value);
}

TEST(znf_value, init) {
  double threshold = 0.001;
  double step = 0.001;
  double target = 0.0;

  znf_value_t znf_value = {123456789, 123456789, 123456789, 123456789, 123456789, false};

  znf_value_init(threshold, step, target, &znf_value);

  EXPECT_EQ(0.0, znf_value.delta);
  EXPECT_EQ(true, znf_value.equal);
  EXPECT_EQ(step, znf_value.step);
  EXPECT_EQ(threshold, znf_value.threshold);
  EXPECT_EQ(target, znf_value.target);
  EXPECT_EQ(target, znf_value.value);
}

TEST(znf_value, set_target) {
  znf_value_t znf_value;
  test_znf_value_init(znf_value);
  double expected_target = 3.74;
  double expected_value = 0.0;

  znf_value_set(expected_target, &znf_value);
  EXPECT_EQ(expected_target, znf_value.target);
  EXPECT_EQ(expected_value, znf_value.value);
  EXPECT_EQ(false, znf_value.equal);

  expected_target = expected_value;
  znf_value_set(expected_target, &znf_value);
  EXPECT_EQ(expected_target, znf_value.target);
  EXPECT_EQ(expected_value, znf_value.value);
  EXPECT_EQ(true, znf_value.equal);
}

TEST(znf_value, reset) {
  znf_value_t znf_value;
  test_znf_value_init(znf_value);
  double expected_target = 3.74;

  znf_value_set(expected_target, &znf_value);

  znf_value_reset(&znf_value);

  EXPECT_EQ(expected_target, znf_value.target);
  EXPECT_EQ(expected_target, znf_value.value);
}

TEST(znf_value, update) {
  znf_value_t znf_value;
  test_znf_value_init(znf_value);
  double expected_target = 1.0;
  double init_target = 0.0;
  znf_value_init(0.1, 0.5, init_target, &znf_value);

  znf_value_set(expected_target, &znf_value);
  EXPECT_EQ(expected_target, znf_value.target);
  EXPECT_NE(expected_target, znf_value.value);
  EXPECT_NE(true, znf_value.equal);

  znf_value_update(&znf_value);
  EXPECT_EQ(expected_target, znf_value.target);
  EXPECT_NE(expected_target, znf_value.value);
  EXPECT_NE(true, znf_value.equal);

  znf_value_update(&znf_value);
  EXPECT_EQ(expected_target, znf_value.target);
  EXPECT_EQ(expected_target, znf_value.value);
  EXPECT_EQ(true, znf_value.equal);
}

TEST(znf_value, equal) {
  znf_value_t znf_value;
  test_znf_value_init(znf_value);
  double init_target = 0.0;
  double threshold = 0.1;

  znf_value_init(threshold, 0.01, init_target, &znf_value);
  EXPECT_EQ(true, znf_value_equal(&znf_value));

  znf_value_set(init_target + 1.1 * threshold, &znf_value);
  EXPECT_NE(true, znf_value_equal(&znf_value));

  znf_value_set(init_target + 0.99 * threshold, &znf_value);
  EXPECT_EQ(true, znf_value_equal(&znf_value));
}
