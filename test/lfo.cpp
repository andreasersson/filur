/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <gtest/gtest.h>

#include <filur/lfo.h>
#include "../src/lfo.c"

#include <cstring>
#include <cmath>
#include <functional>

typedef std::function<void(double*, int, lfo_state_t*, const lfo_parameters_t*)> process_func_t;

static bool verify_process(int number_of_frames,
                           int number_of_block,
                           double sample_rate,
                           double frequency,
                           double accepted_min,
                           double accepted_max,
                           double tolerance,
                           process_func_t func)
{
  bool ret_val = true;

  std::vector<double> buffer(number_of_frames);
  double min = 1e10;
  double max = -1e10;
  lfo_state_t lfo_state;
  lfo_parameters_t lfo_parameters;
  lfo_init_state(&lfo_state);
  lfo_init_parameters(sample_rate, &lfo_parameters);
  lfo_set_frequency(frequency, &lfo_parameters);

  for (int block = 0; block < number_of_block; ++block) {
    func(buffer.data(), number_of_frames, &lfo_state, &lfo_parameters);

    for (int n = 0; n < number_of_frames; ++n) {
      double value = buffer.at(n);
      if (min > value) {
        min = value;
      }
      if (max < value) {
        max = value;
      }
    }
  }

  EXPECT_TRUE((max >= accepted_max - tolerance) && (max <= accepted_max + tolerance));
  EXPECT_TRUE((min >= accepted_min - tolerance) && (min <= accepted_min + tolerance));

  return ret_val;
}

TEST(lfo, init) {
  lfo_state_t lfo_state = { 123456789 };

  lfo_init_state(&lfo_state);

  EXPECT_EQ(0.0, lfo_state.alpha);
}

TEST(lfo, process_alpha) {
  EXPECT_TRUE(verify_process(1024, 50, 44100.0, 10.0, -1.0, 1.0, 0.01, lfo_process_alpha));
}

TEST(lfo, process_sine) {
  EXPECT_TRUE(verify_process(1024, 50, 44100.0, 10.0, -1.0, 1.0, 0.01, lfo_process_sine));
}

TEST(lfo, process_triangle) {
  EXPECT_TRUE(verify_process(1024, 50, 44100.0, 10.0, -1.0, 1.0, 0.01, lfo_process_triangle));
}

TEST(lfo, process_triangle_normalized) {
  EXPECT_TRUE(verify_process(1024, 50, 44100.0, 10.0, 0.0, 1.0, 0.01, lfo_process_triangle_normalized));
}

TEST(lfo, process_triangle_sqr) {
  EXPECT_TRUE(verify_process(1024, 50, 44100.0, 10.0, 0.0, 1.0, 0.01, lfo_process_triangle_sqr));
}

TEST(lfo, process_saw) {
  EXPECT_TRUE(verify_process(1024, 50, 44100.0, 10.0, -1.0, 1.0, 0.01, lfo_process_saw));
}

TEST(lfo, process_square) {
  EXPECT_TRUE(verify_process(1024, 50, 44100.0, 10.0, -1.0, 1.0, 0.01, lfo_process_square));
}
