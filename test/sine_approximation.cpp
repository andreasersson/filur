/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <gtest/gtest.h>
#include <cmath>
#include <functional>

#include <filur/sine_approximation.h>

#ifndef M_PI
  #define M_PI 3.14159265358979323846264338327950288
#endif /* M_PI */

static bool verify(size_t length,
                   double error,
                   std::function<double (double value)> ref_func,
                   std::function<double (double value)> test_func)
{
  bool ret_val = true;
  double alpha = -M_PI;
  double delta = 2.0 * M_PI / length;
  for (size_t n = 0; (n < length) && (true == ret_val); ++n) {
    double expected_value = ref_func(alpha);
    double value = test_func(alpha);
    double e = fabs(expected_value - value);
    EXPECT_GE(error, e);
    if (e >= error) {
      ret_val = false;
    }
    alpha += delta;
  }

  return ret_val;
}

TEST(sine_approximation, sine_9) {
  size_t length = 1000;
  double error = 0.00692528;
  EXPECT_TRUE(verify(length, error, (double(*)(double))&std::sin, sine_9));
}

TEST(sine_approximation, sine_11) {
  size_t length = 1000;
  double error = 0.00044517;
  EXPECT_TRUE(verify(length, error, (double(*)(double))&std::sin, sine_11));
}

TEST(sine_approximation, sine_13) {
  size_t length = 1000;
  double error = 2.11427e-05;
  EXPECT_TRUE(verify(length, error, (double(*)(double))&std::sin, sine_13));
}

TEST(sine_approximation, sine_15) {
  size_t length = 1000;
  double error = 7.72787e-07;
  EXPECT_TRUE(verify(length, error, (double(*)(double))&std::sin, sine_15));
}

TEST(sine_approximation, sine_21) {
  size_t length = 1000;
  double error = 1.0349e-11;
  EXPECT_TRUE(verify(length, error, (double(*)(double))&std::sin, sine_21));
}

TEST(sine_approximation, cos_8) {
  size_t length = 1000;
  double error = 0.0239779;
  EXPECT_TRUE(verify(length, error, (double(*)(double))&std::cos, cos_8));
}

TEST(sine_approximation, cos_10) {
  size_t length = 1000;
  double error = 0.0018292;
  EXPECT_TRUE(verify(length, error, (double(*)(double))&std::cos, cos_10));
}

TEST(sine_approximation, cos_12) {
  size_t length = 1000;
  double error = 0.00010048;
  EXPECT_TRUE(verify(length, error, (double(*)(double))&std::cos, cos_12));
}

TEST(sine_approximation, cos_14) {
  size_t length = 1000;
  double error = 4.16782e-06;
  EXPECT_TRUE(verify(length, error, (double(*)(double))&std::cos, cos_14));
}

TEST(sine_approximation, cos_16) {
  size_t length = 1000;
  double error = 1.3527e-07;
  EXPECT_TRUE(verify(length, error, (double(*)(double))&std::cos, cos_16));
}

TEST(sine_approximation, cos_18) {
  size_t length = 1000;
  double error = 3.52909e-09;
  EXPECT_TRUE(verify(length, error, (double(*)(double))&std::cos, cos_18));
}

TEST(sine_approximation, cos_20) {
  size_t length = 1000;
  double error = 7.56509e-11;
  EXPECT_TRUE(verify(length, error, (double(*)(double))&std::cos, cos_20));
}
