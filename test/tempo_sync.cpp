/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filur.
 *
 * filur is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filur is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filur.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <gtest/gtest.h>

#include "../src/tempo_sync.c"

TEST(tempo_sync, value_to_rate) {
  EXPECT_EQ(1.0, tempo_sync_to_rate(TEMPO_SYNC_FREE_RUNNING));
  EXPECT_EQ(4.0, tempo_sync_to_rate(TEMPO_SYNC_16TH));
  EXPECT_EQ(2.0, tempo_sync_to_rate(TEMPO_SYNC_8TH));
  EXPECT_EQ(1.0, tempo_sync_to_rate(TEMPO_SYNC_4TH));
  EXPECT_EQ(1.0/2.0, tempo_sync_to_rate(TEMPO_SYNC_2TH));
  EXPECT_EQ(1.0/4.0, tempo_sync_to_rate(TEMPO_SYNC_1));
  EXPECT_EQ(1.0/8.0, tempo_sync_to_rate(TEMPO_SYNC_2));
  EXPECT_EQ(1.0/16.0, tempo_sync_to_rate(TEMPO_SYNC_4));
}

TEST(tempo_sync, loop_all) {
  for (int tempo_sync = TEMPO_SYNC_FREE_RUNNING; tempo_sync < TEMPO_SYNC_NUM_TEMPO_SYNCS; ++tempo_sync) {
    tempo_sync_to_rate(static_cast<tempo_sync_t>(tempo_sync));
  }
}

TEST(tempo_sync, value_to_tempo_sync) {
  EXPECT_EQ(TEMPO_SYNC_FREE_RUNNING, value_to_tempo_sync(TEMPO_SYNC_FREE_RUNNING));
  EXPECT_EQ(TEMPO_SYNC_16TH, value_to_tempo_sync(TEMPO_SYNC_16TH));
  EXPECT_EQ(TEMPO_SYNC_8TH, value_to_tempo_sync(TEMPO_SYNC_8TH));
  EXPECT_EQ(TEMPO_SYNC_4TH, value_to_tempo_sync(TEMPO_SYNC_4TH));
  EXPECT_EQ(TEMPO_SYNC_2TH, value_to_tempo_sync(TEMPO_SYNC_2TH));
  EXPECT_EQ(TEMPO_SYNC_1, value_to_tempo_sync(TEMPO_SYNC_1));
  EXPECT_EQ(TEMPO_SYNC_2, value_to_tempo_sync(TEMPO_SYNC_2));
  EXPECT_EQ(TEMPO_SYNC_4, value_to_tempo_sync(TEMPO_SYNC_4));

  EXPECT_EQ(TEMPO_SYNC_FREE_RUNNING, value_to_tempo_sync(-1.0));
  EXPECT_EQ(TEMPO_SYNC_FREE_RUNNING, value_to_tempo_sync(TEMPO_SYNC_NUM_TEMPO_SYNCS));
}
